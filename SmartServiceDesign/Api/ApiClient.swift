//
//  ApiClient.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/11/23.
//

import Foundation
import Alamofire

//api 호출 클라이언트
final class ApiClient {
    static let shared = ApiClient()
    
    static let BASE_URL = "https://smart-service-api.team-everywhere.com"
    
    let interceptors = Interceptor(interceptors: [
        
        BaseInterceptor() //application/json
        
    ])
    
    let monitors = [ApiLogger()] as [EventMonitor]
    
    var session: Session  // session 자체가 핵심
    
    init(){
        print("ApiClient - init() called")
        
        session = Session(interceptor: interceptors, eventMonitors: monitors)
    }
    
    
    
}
