//
//  SignIn.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/2/24.
//

import Foundation

//로그인 유저정보 받기

struct SignIn: Codable {
    let userEmail: String
    let userPwd: String
}
