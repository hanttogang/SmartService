//
//  DetailEmployeeView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/26/23.
//

import SwiftUI

struct DetailEmployeeView: View {
    
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    
    
    @State private var navigate: Bool = false
    @State private var navigateContractDetailForDetailCustomerBrandView: Bool = false
    
    @State private var showToast: Bool = false
    @State private var toastText: String = ""
    
    @State private var modificationMode: Bool = false
    
    @State private var showingImagePickerFor_DetailEmployeeInfo = false
    @State private var selectedDetailEmployeeInfo: UIImage? = nil
    
    
    var getEmployeeNameFor_DetailEmployeeInfo: String = "김영희"
    var getEmployeePositionFor_DetailEmployeeInfo: String = "사원"
    @State private var employeeNameFor_DetailEmployeeInfo: String = ""
    @State private var employeePositionFor_DetailEmployeeInfo: String = ""
    
    @State private var classificationFor_DetailEmployeeInfo: String = "소속직원"
    
    @State private var departmentFor_DetailEmployeeInfo: String = "ㅁㅁㅁ"
    @State private var employeePhoneNumberFor_DetailEmployeeInfo: String = "031-123-1234"
    @State private var employeeEmailFor_DetailEmployeeInfo: String = "bca321@mail.com"
    
    @State private var contractImageForDetailEmployeeInfoContractList: UIImage? = nil
    
    
    @State private var showAllContractList: Bool = false
    @State private var showShowAllButton: Bool = false
    
    
    
    
    var body: some View {
        
        ZStack {
            ScrollView(.vertical){
                VStack(spacing: 0){
                    
                    
                    HStack{
                        
                        
                        VStack(alignment: .leading){// 브랜드 이미지
                            ZStack{
                                if modificationMode{
                                    
                                    Button(action: {
                                        showingImagePickerFor_DetailEmployeeInfo = true
                                    }, label: {
                                        
                                        ZStack {
                                            if let image = selectedDetailEmployeeInfo {
                                                Image(uiImage: image)
                                                    .resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                    .cornerRadius(12.0)
                                                
                                            } else {
                                                Rectangle()
                                                    .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                    .cornerRadius(12.0)
                                                    .foregroundColor(.colorE5E5E5)
                                                
                                            }
                                        }
                                    })
                                    
                                }else{
                                    
                                    ZStack {
                                        if let image = selectedDetailEmployeeInfo {
                                            Image(uiImage: image)
                                                .resizable()
                                                .aspectRatio(contentMode: .fill)
                                                .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                .cornerRadius(12.0)
                                            
                                        } else {
                                            Rectangle()
                                                .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                .cornerRadius(12.0)
                                                .foregroundColor(.colorE5E5E5)
                                            
                                        }
                                    }
                                    
                                }
                            } //ZStack BrandImage
                            .sheet(isPresented: $showingImagePickerFor_DetailEmployeeInfo, onDismiss: loadDetailEmployeeInfoImage) {
                                ImagePicker(selectedImage: $selectedDetailEmployeeInfo)
                            }
                            
                        }//VStack 브랜드 이미지
                        
                        HStack(){
                            
                            HStack{
                                
                                //직원이름
                                if modificationMode{
                                    
                                    TextField(".", text: $employeeNameFor_DetailEmployeeInfo, prompt: Text("\(employeeNameFor_DetailEmployeeInfo)")
                                        .foregroundColor(Color("hintTextColor")))
                                    .keyboardType(.default)
                                    .font(.title2)
                                    .frame(width: screenWidth/6.3)
    //                                .padding(.leading, -screenWidth/150)
                                    
                                    
                                }else {
                                    
                                    Text("\(employeeNameFor_DetailEmployeeInfo)")
                                        .font(.title2)
                                    
//                                        .frame(width: screenWidth/6.3)
                                }
                            }
                            HStack{
                                
                                
                                //직원 직함
                                if modificationMode{
                                    
                                    TextField(".", text: $employeePositionFor_DetailEmployeeInfo, prompt: Text("\(employeePositionFor_DetailEmployeeInfo)")
                                        .foregroundColor(Color("hintTextColor")))
                                    .keyboardType(.default)
                                    .font(.title2)
                                    .frame(width: screenWidth/7.8125)//48
//                                    .padding(.leading, -1)//4
                                    
                                    
                                    
                                    
                                }else {
                                    
                                    Text("\(employeePositionFor_DetailEmployeeInfo)")
                                        .font(.title2)
                                        .frame(width: screenWidth/7.8125)//48
                                }
                            }
                            
                        }
                        
                        
                        
                        
                        
                        
                        
                        Spacer()
                        
                        if(modificationMode){
                            Button(action: {
                                
                                
                                if (selectedDetailEmployeeInfo == nil || employeeNameFor_DetailEmployeeInfo == "" ||  employeePositionFor_DetailEmployeeInfo == "" || classificationFor_DetailEmployeeInfo == "" || departmentFor_DetailEmployeeInfo == "" || employeePhoneNumberFor_DetailEmployeeInfo == "" || employeeEmailFor_DetailEmployeeInfo == ""){
                                    
                                    toastText = "칸을 비울 수 없습니다."
                                    showToast = true
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                        self.showToast = false
                                        toastText = ""
                                    }
                                    
                                } else {
                                    
                                    modificationMode = false
                                }
                                
                                
                                
                            }, label: {
                                Text("저장")
                                
                            })
                        } else {
                            Button(action: {
                                
                                modificationMode = true
                                
                                
                            }, label: {
                                Text("수정")
                                
                            })
                        }
                        
                        
                    }
                    .padding(.horizontal)
                    .padding(.vertical, screenHeight / 81.5)
                    .padding(.top, screenHeight / 81.5)
                    
                    Divider()
                        .padding(.leading)
                   

                    HStack{
                        Text("분류")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        Text("소속직원")
                            .foregroundColor(.white)
                            .bold()
                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                            .background(Color("mainColor"))
                            .cornerRadius(16.0)
                        
//                        if modificationMode{
//                            RightAlignedTextField(text: $departmentFor_DetailEmployeeInfo)
//                                .keyboardType(.default)
//                                .autocapitalization(.none)
//                            
//                        }else {
//                            
//                            Text("\(departmentFor_DetailEmployeeInfo)")
//                                .foregroundColor(Color("color00000040"))
//                        }
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    
                    HStack{
                        Text("담당부서")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            RightAlignedTextField(text: $departmentFor_DetailEmployeeInfo)
                                .keyboardType(.default)
                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(departmentFor_DetailEmployeeInfo)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("전화번호")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            RightAlignedTextField(text: $employeePhoneNumberFor_DetailEmployeeInfo)
                                .keyboardType(.numberPad)
                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(employeePhoneNumberFor_DetailEmployeeInfo)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{
                        Text("이메일")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            RightAlignedTextField(text: $employeeEmailFor_DetailEmployeeInfo)
                                .keyboardType(.emailAddress)
                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(employeeEmailFor_DetailEmployeeInfo)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    
                    VStack(spacing: 0) {
                        HStack{
                            
                            Text("계약 리스트")
                                .font(.title2)
                                .bold()
                                .padding()
                            //                                .padding(.top, -screenHeight/13.5)
                            Spacer()
                        }
                        .padding(0)
                        
                        Button(action: {
                            print("계약 추가 버튼 클릭됨")
                            
                            self.navigate = true
                            
                        }, label: {
                            
                            NavigationLink(destination: AddContractOfDetailEmployeeView(selectedDetailEmployeeName: "\(employeeNameFor_DetailEmployeeInfo) \(employeePositionFor_DetailEmployeeInfo)"), isActive: $navigate) {
                                EmptyView()
                            }
                            
                            Text("+  계약 추가")
                                .foregroundColor(.white)
                                .font(.system(size: 16))
                                .bold()
                                .frame(width: screenWidth/1.25, height: screenHeight/20.375)
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        
                        
                        VStack{
                            
                            ForEach(contractDummyData.sorted(by: { $0.endDate > $1.endDate }).prefix(showAllContractList ? contractDummyData.count : 3)) { contract in
                                
                                NavigationLink(destination: ContractDetailOfEmployeeView(selectedDetailContractNameFor_DetailEmployee: contract.repName, selectedDetailEmployeeName: "\(employeeNameFor_DetailEmployeeInfo) \(employeePositionFor_DetailEmployeeInfo)")) {
                                    
                                    VStack{
                                        HStack{ //리스트항목의 모델
                                            
                                            if let image = contract.image {
                                                Image(uiImage: image)
                                                    .resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                    .cornerRadius(12.0)
                                            } else {
                                                Rectangle()
                                                    .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                    .cornerRadius(12.0)
                                                    .foregroundColor(.colorE5E5E5)
                                            }
                                            
                                            VStack(alignment: .leading){
                                                Text(contract.repName)
                                                    .foregroundColor(.black)
                                                
                                                Text("\(contract.formattedStartDate) ~ \(contract.formattedEndDate)")
                                                    .foregroundColor(Color("color00000040"))
                                                    .font(.caption)
                                            }
                                            
                                            Spacer()
                                            
                                            Text(contract.status)
                                                .foregroundColor(.white)
                                                .bold()
                                                .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                                .background(Color("mainColor"))
                                                .cornerRadius(16.0)
                                            
                                            
                                        }
                                        
                                        Divider()
                                            .padding(.leading, screenWidth/8)
                                    }
                                    
                                }//NavigationLink
                            }
                            
                            
                            
                            
                        } //VStack 계약 리스트
                        .listStyle(PlainListStyle())
                        .padding(.horizontal)
                        .padding(.top, screenHeight/50.9375)
                        
                        
                        if showShowAllButton{
                            
                            Button(action: {
                                
                                showShowAllButton = false
                                showAllContractList = true
                                
                            }, label: {
                                
                                Text("더 보기")
                                    .foregroundColor(.black)
                                    .font(.system(size: 16))
                                    .bold()
                                    .frame(width: screenWidth/1.25, height: screenHeight/20.375)
                            })
                            .background(.white)
                            .cornerRadius(4)
                            .padding()
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color(.black), lineWidth: 1)
                                .padding()
                            )
                            
                        }
                        
                        Spacer()
                        
                        
                        
                        
                    }//VStack
                    .frame(width: screenWidth/1.0932)
                    .background(Color.white)
                    .cornerRadius(20)
                    .shadow(color: Color("color00000040"), radius: 20, x: 0, y: 30)
                    .padding(.top, screenHeight/31.346)
                    
                    //                    .sheet(isPresented: $showingBankRecordCopyImagePickerForDetailBrand, onDismiss: loadContractImageForDetailBrandContractList) {
                    //                        ImagePicker(selectedImage: $selectedBankRecordCopyImageForDetailBrand)
                    //                    }
                    
                    
                    
                    
                    
                } //VStack
                
                
            }//ScrollView
            .toast(isShowing: $showToast, text: Text(toastText))
            
            
            
            
            
        }//ZStack
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: {
            
            self.presentationMode.wrappedValue.dismiss()
            
            
        }) {
            
            HStack(spacing: 0){
                
                Image(systemName: "chevron.backward")
                
            }
            
            
            
        })
        .onAppear {
            if contractDummyData.count > 3 {
                showShowAllButton = true
            }
            
            employeeNameFor_DetailEmployeeInfo = getEmployeeNameFor_DetailEmployeeInfo
            employeePositionFor_DetailEmployeeInfo = getEmployeePositionFor_DetailEmployeeInfo
        }
        .navigationBarTitle("\(employeeNameFor_DetailEmployeeInfo) \(employeePositionFor_DetailEmployeeInfo)", displayMode: .inline)
        
        
        
        
    }//body
    
    private func loadDetailEmployeeInfoImage() {
        guard let selectedDetailEmployeeInfo = selectedDetailEmployeeInfo else { return }
        // You can do something with the selected brand image here
    }
    
}

#Preview {
    DetailEmployeeView()
}
