//
//  User.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/11/23.
//

import Foundation

//로그인 유저정보 받기

struct User: Codable {
    let id: String
    let password: String
}
