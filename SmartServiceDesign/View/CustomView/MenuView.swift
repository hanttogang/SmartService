//
//  MenuView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/13/23.
//

import SwiftUI

enum ActiveView {
    case marketingProjectMain, customerManagement, marketingChannelManagement, employeeAndCreaterManagement
}

struct MenuView: View {
    @Environment(\.presentationMode) var presentationMode
    
    @State var isMarketingProjectMenu = false
    @State var isPersonnelManagementMenu = false
    @State var isCostPriceManagementMenu = false
    @State var isSalesManagementMenu = false
    @State var isMessageManagementMenu = false
    @State var isAIAnalysisMenu = false
    @State var isMyPageMenu = false
    
    @State var navigateToLoginView = false
    
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    let defaults = UserDefaults.standard
    
    
//    @State var marketingProjectMainViewFromMenu: Bool = false
//    @State var customerManagementViewFromMenu: Bool = false
    
    @State private var activeView: ActiveView? = nil
    
    var body: some View {
        
        ZStack{
            switch activeView {
            case .marketingProjectMain:
                MainMarketingProjectManagementView()
            case .customerManagement:
                MainCustomerManagementView()
            case .marketingChannelManagement:
                MainMarketingChannelManagementView()
            case .employeeAndCreaterManagement:
                MainEmployeeAndCreaterManagementView()
                
            default:
                // 기본 뷰를 여기에 표시
                HStack{
                    ScrollView(showsIndicators: false){
                        
                            VStack(spacing: 0){
                                
                                
                                
                                Group{ // 광고 프로젝트 관리
                                    Button(action: {
                                        
                                        withAnimation(.none){
                                            self.isMarketingProjectMenu.toggle()
                                        }
                                        
                                    }, label: {
                                        
                                        
                                        HStack{
                                            Image(systemName: "chart.line.uptrend.xyaxis")
                                                .resizable()
                                                .aspectRatio(contentMode: .fit)
                                                .frame(width: screenWidth/15.625, height: screenHeight/33.958)
                                                .foregroundColor(isMarketingProjectMenu ? Color(.white) : Color(.black))
                                                
                                            
                                            Text("광고 프로젝트 관리")
                                                .foregroundColor(isMarketingProjectMenu ? Color(.white) : Color(.black))
                                                .font(.headline)
                                            
                                            Spacer()
                                            
                                            Image( isMarketingProjectMenu ? "arrowUp" : "arrowBottom")
                                                .imageScale(.large)
                                            
                                        }
                                        
                                        
                                    })
                                    .padding(.horizontal, screenWidth/15)
                                    .frame(height: screenHeight/16)
                                    .background(isMarketingProjectMenu ? Color("mainColor") : Color(.white))
                                    
                                    
                                    if isMarketingProjectMenu {
                                        Button(action: {
                                            self.activeView = .marketingProjectMain
                                        }) {
                                            HStack{
                                                Text("프로젝트 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                        
                                        Button(action: {
                                            self.activeView = .customerManagement
                                        }) {
                                            HStack{
                                                Text("고객 (브랜드)관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                        
                                        Button(action: {
                                            self.activeView = .marketingChannelManagement
                                        }) {
                                            HStack{
                                                Text("마케팅 채널 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                    }
                                }//Group 광고 프로젝트 관리
                                
                                Group{ // 인사 관리
                                    
                                    Button(action: {
                                        
                                        
                                        withAnimation(.none){
                                            self.isPersonnelManagementMenu.toggle()
                                        }
                                        
                                        
                                    }, label: {
                                        
                                        
                                        HStack{
                                            Image(systemName: "person.2.crop.square.stack")
                                                .resizable()
                                                .aspectRatio(contentMode: .fit)
                                                .frame(width: screenWidth/15.625, height: screenHeight/33.958)
                                                .foregroundColor(isPersonnelManagementMenu ? Color(.white) : Color(.black))
                                                .imageScale(.large)
                                            
                                            Text("인사 관리")
                                                .foregroundColor(isPersonnelManagementMenu ? Color(.white) : Color(.black))
                                                .font(.headline)
                                            
                                            Spacer()
                                            
                                            Image( isPersonnelManagementMenu ? "arrowUp" : "arrowBottom")
                                                .imageScale(.large)
                                            
                                        }
                                        
                                        
                                    })
                                    .padding(.horizontal, screenWidth/15)
                                    .frame(height: screenHeight/16)
                                    .background(isPersonnelManagementMenu ? Color("mainColor") : Color(.white))

                                    
                                    
                                    if isPersonnelManagementMenu {
                                        Button(action: {
                                            self.activeView = .employeeAndCreaterManagement
                                        }) {
                                            HStack{
                                                Text("직원/크리에이터 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                        
                                        Button(action: {
                                            
                                        }) {
                                            HStack{
                                                Text("근태 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                        
                                        Button(action: {
                                            
                                        }) {
                                            HStack{
                                                Text("급여 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                    }
                                    
                                }//Group 인사관리
                                
                                Group{ //원가 관리
                                    
                                    Button(action: {
                                        
                                        withAnimation(.none){
                                            self.isCostPriceManagementMenu.toggle()
                                        }
                                        
                                        
                                    }, label: {
                                        
                                        
                                        HStack{
                                            Image(systemName: "plus.forwardslash.minus")
                                                .resizable()
                                                .aspectRatio(contentMode: .fit)
                                                .frame(width: screenWidth/15.625, height: screenHeight/33.958)
                                                .foregroundColor(isCostPriceManagementMenu ? Color(.white) : Color(.black))
                                                .imageScale(.large)
                                            
                                            Text("원가 관리")
                                                .foregroundColor(isCostPriceManagementMenu ? Color(.white) : Color(.black))
                                                .font(.headline)
                                            
                                            Spacer()
                                            
                                            Image( isCostPriceManagementMenu ? "arrowUp" : "arrowBottom")
                                                .imageScale(.large)
                                            
                                        }
                                        
                                        
                                    })
                                    .padding(.horizontal, screenWidth/15)
                                    .frame(height: screenHeight/16)
                                    .background(isCostPriceManagementMenu ? Color("mainColor") : Color(.white))
                                    
                                    
                                    if isCostPriceManagementMenu {
                                        Button(action: {
                                            
                                        }) {
                                            HStack{
                                                Text("직원/크리에이터 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                        
                                        Button(action: {
                                            
                                        }) {
                                            HStack{
                                                Text("근태 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                        
                                        Button(action: {
                                            
                                        }) {
                                            HStack{
                                                Text("급여 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                    }
                                    
                                }//Group 원가관리
                                
                                Group{ //매출 관리
                                    
                                    Button(action: {
                                        
                                        withAnimation(.none){
                                            self.isSalesManagementMenu.toggle()
                                        }
                                        
                                        
                                    }, label: {
                                        
                                        
                                        HStack{
                                            Image(systemName: "wonsign.square")
                                                .resizable()
                                                .aspectRatio(contentMode: .fit)
                                                .frame(width: screenWidth/15.625, height: screenHeight/33.958)
                                                .foregroundColor(isSalesManagementMenu ? Color(.white) : Color(.black))
                                                .imageScale(.large)
                                            
                                            Text("매출 관리")
                                                .foregroundColor(isSalesManagementMenu ? Color(.white) : Color(.black))
                                                .font(.headline)
                                            
                                            Spacer()
                                            
                                            Image( isSalesManagementMenu ? "arrowUp" : "arrowBottom")
                                                .imageScale(.large)
                                            
                                        }
                                        
                                        
                                    })
                                    .padding(.horizontal, screenWidth/15)
                                    .frame(height: screenHeight/16)
                                    .background(isSalesManagementMenu ? Color("mainColor") : Color(.white))
                                    
                                    
                                    if isSalesManagementMenu {
                                        Button(action: {
                                            
                                        }) {
                                            HStack{
                                                Text("직원/크리에이터 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                        
                                        Button(action: {
                                            
                                        }) {
                                            HStack{
                                                Text("근태 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                        
                                        Button(action: {
                                            
                                        }) {
                                            HStack{
                                                Text("급여 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                    }
                                    
                                }//Group 매출관리
                                
                                Group{ //메세지 관리
                                    
                                    Button(action: {
                                        
                                        withAnimation(.none){
                                            self.isMessageManagementMenu.toggle()
                                        }
                                        
                                        
                                    }, label: {
                                        
                                        
                                        HStack{
                                            Image(systemName: "ellipsis.message")
                                                .resizable()
                                                .aspectRatio(contentMode: .fit)
                                                .frame(width: screenWidth/15.625, height: screenHeight/33.958)
                                                .foregroundColor(isMessageManagementMenu ? Color(.white) : Color(.black))
                                                .imageScale(.large)
                                            
                                            Text("메세지 관리")
                                                .foregroundColor(isMessageManagementMenu ? Color(.white) : Color(.black))
                                                .font(.headline)
                                            
                                            Spacer()
                                            
                                            Image( isMessageManagementMenu ? "arrowUp" : "arrowBottom")
                                                .imageScale(.large)
                                            
                                        }
                                        
                                        
                                    })
                                    .padding(.horizontal, screenWidth/15)
                                    .frame(height: screenHeight/16)
                                    .background(isMessageManagementMenu ? Color("mainColor") : Color(.white))
                                    
                                    
                                    if isMessageManagementMenu {
                                        Button(action: {
                                            
                                        }) {
                                            HStack{
                                                Text("직원/크리에이터 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                        
                                        Button(action: {
                                            
                                        }) {
                                            HStack{
                                                Text("근태 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                        
                                        Button(action: {
                                            
                                        }) {
                                            HStack{
                                                Text("급여 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                    }
                                    
                                }//Group 메세지 관리
                                
                                Group{ //AI 분석
                                    
                                    Button(action: {
                                        
                                        withAnimation(.none){
                                            self.isAIAnalysisMenu.toggle()
                                        }
                                        
                                        
                                    }, label: {
                                        
                                        
                                        HStack{
                                            Image(systemName: "person")
                                                .resizable()
                                                .aspectRatio(contentMode: .fit)
                                                .frame(width: screenWidth/15.625, height: screenHeight/33.958)
                                                .foregroundColor(isAIAnalysisMenu ? Color(.white) : Color(.black))
                                                .imageScale(.large)
                                            
                                            Text("AI 분석")
                                                .foregroundColor(isAIAnalysisMenu ? Color(.white) : Color(.black))
                                                .font(.headline)
                                            
                                            Spacer()
                                            
                                            Image( isAIAnalysisMenu ? "arrowUp" : "arrowBottom")
                                                .imageScale(.large)
                                            
                                        }
                                        
                                        
                                    })
                                    .padding(.horizontal, screenWidth/15)
                                    .frame(height: screenHeight/16)
                                    .background(isAIAnalysisMenu ? Color("mainColor") : Color(.white))
                                    
                                    
                                    if isAIAnalysisMenu {
                                        Button(action: {
                                            
                                        }) {
                                            HStack{
                                                Text("직원/크리에이터 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                        
                                        Button(action: {
                                            
                                        }) {
                                            HStack{
                                                Text("근태 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                        
                                        Button(action: {
                                            
                                        }) {
                                            HStack{
                                                Text("급여 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                    }
                                    
                                }//Group AI 분석
                                
                                Group{ //마이페이지
                                    
                                    Button(action: {
                                        
                                        withAnimation(.none){
                                            self.isMyPageMenu.toggle()
                                        }
                                        
                                        
                                    }, label: {
                                        
                                        
                                        HStack{
                                            Image(systemName: "person")
                                                .foregroundColor(isMyPageMenu ? Color(.white) : Color(.black))
                                                .imageScale(.large)
                                            
                                            Text("마이페이지")
                                                .foregroundColor(isMyPageMenu ? Color(.white) : Color(.black))
                                                .font(.headline)
                                            
                                            Spacer()
                                            
                                            Image( isMyPageMenu ? "arrowUp" : "arrowBottom")
                                                .imageScale(.large)
                                            
                                        }
                                        
                                        
                                    })
                                    .padding(.horizontal, screenWidth/15)
                                    .frame(height: screenHeight/16)
                                    .background(isMyPageMenu ? Color("mainColor") : Color(.white))
                                    
                                    
                                    if isMyPageMenu {
                                        Button(action: {
                                            
                                        }) {
                                            HStack{
                                                Text("직원/크리에이터 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                        
                                        Button(action: {
                                            
                                        }) {
                                            HStack{
                                                Text("근태 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                        
                                        Button(action: {
                                            
                                        }) {
                                            HStack{
                                                Text("급여 관리")
                                                    .foregroundColor(.black)
                                                    .font(.headline)
                                                
                                                Spacer()
                                            }
                                        }
                                        .padding(.horizontal, screenWidth/7.2)
                                        .frame(height: screenHeight/16)
                                        .background(Color.white)
                                    }
                                    
                                }//Group 메세지 관리
                                
                                
                                
                                

    //                            Spacer()
                                                    
                                
                            }
                        
                        
                    }//ScrollView
                    .frame(width: screenWidth/1.23)
                    .background(.white)
                    
                    
                    Spacer()
                }//HStack
            }
            
            
                
            
            
            
            
            
            
            ZStack{
                
//
//                ZStack{
//                    CustomBottomNavigationBar(selectedTab: $selectedTab)
//                }
                
            }
            
                
            
        }//ZStack
        
        //        .edgesIgnoringSafeArea(.all)
        
        
    }
}

#Preview {
    MenuView()
}
