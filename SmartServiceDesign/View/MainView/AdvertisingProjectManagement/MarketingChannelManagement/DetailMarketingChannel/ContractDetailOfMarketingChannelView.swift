//
//  ContractDetailOfMarketingChannelView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/26/23.
//

import SwiftUI

struct ContractDetailOfMarketingChannelView: View {
    
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    var selectedDetailContractNameFor_DetailContractMarketingChannel: String = ""
    var selectedDetailMarketingChannelName: String = ""
    
    @State private var navigate: Bool = false
    
    @State private var showToast: Bool = false
    @State private var toastText: String = ""
    
    @State private var modificationModeFor_DetailContractMarketingChannel: Bool = false
    
    
    @State private var contractNameFor_DetailContractMarketingChannel: String = "유튜브 광고 2차"
    @State private var contractOneLineExplanFor_DetailContractMarketingChannel: String = "위해브 광고 2차"
    @State private var contractStartDateFor_DetailContractMarketingChannel: String = "2023.12.01"
    @State private var contractEndDateFor_DetailContractMarketingChannel: String = "2023.12.31"
    @State private var contractPriceFor_DetailContractMarketingChannel: String = "10,000,000원"
    @State private var contractStatusFor_DetailContractMarketingChannel: String = "진행"
    
    
    @State private var selecteContractStatusFor_DetailContractMarketingChannel: Bool = false
    @State private var contractStatusModificationModeFor_DetailContractMarketingChannel: Bool = false
    
    @State private var showingBusinessLicenseImagePickerFor_DetailContractMarketingChannel = false
    @State private var selectedBusniessLicenseImageFor_DetailContractMarketingChannel: UIImage? = nil
    
    
  
    
    var body: some View {
        
        ZStack {
            ScrollView(.vertical){
                VStack(spacing: 0){
                    
                    HStack{
                        
                    
                        
                        if modificationModeFor_DetailContractMarketingChannel{
                            
                            TextField(".", text: $contractNameFor_DetailContractMarketingChannel, prompt: Text("\(contractNameFor_DetailContractMarketingChannel)")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.title2)
                            .frame(height: screenHeight/29.1) //28
                            
                            
                        }else {
                            
                            Text("\(contractNameFor_DetailContractMarketingChannel)")
                                .font(.title2)
                                .frame(height: screenHeight/29.1) //28
                        }
                        
                        
                        
                        
                        
                        Spacer()
                        
                        if(modificationModeFor_DetailContractMarketingChannel){
                            Button(action: {
                                
                                
                                if (contractNameFor_DetailContractMarketingChannel == "" || contractOneLineExplanFor_DetailContractMarketingChannel == "" || contractStartDateFor_DetailContractMarketingChannel == "" || contractEndDateFor_DetailContractMarketingChannel == "" || contractPriceFor_DetailContractMarketingChannel == "" || selectedBusniessLicenseImageFor_DetailContractMarketingChannel == nil){
                                    
                                    toastText = "칸을 비울 수 없습니다."
                                    showToast = true
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                        self.showToast = false
                                        toastText = ""
                                    }
                                    
                                } else {
                                    
                                    modificationModeFor_DetailContractMarketingChannel = false
                                }
                                
                                
                                
                            }, label: {
                                Text("저장")
                                
                            })
                        } else {
                            Button(action: {
                                
                                modificationModeFor_DetailContractMarketingChannel = true
                                
                                
                            }, label: {
                                Text("수정")
                                
                            })
                            
                        }
                        
                        
                    }// HStack 계약 이름
                    .padding(.horizontal)
                    .padding(.vertical, screenHeight / 81.5)
//                    .padding(.top, screenHeight / 81.5)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        
                        Text("한줄 설명")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationModeFor_DetailContractMarketingChannel{
                            RightAlignedTextField(text: $contractOneLineExplanFor_DetailContractMarketingChannel)
                                .keyboardType(.numberPad)
                            
                            
                            
                        }else {
                            
                            Text("\(contractOneLineExplanFor_DetailContractMarketingChannel)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("계약기간")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationModeFor_DetailContractMarketingChannel{
//                            RightAlignedTextField(text: $typeOfBusinessForDetailBrand)
//                                .keyboardType(.default)
//                                .autocapitalization(.none)
                            
                            Text("\(contractStartDateFor_DetailContractMarketingChannel) ~ \(contractEndDateFor_DetailContractMarketingChannel)")
                                .foregroundColor(Color("color00000040"))

                            
                            
                        }else {
                            
                            Text("\(contractStartDateFor_DetailContractMarketingChannel) ~ \(contractEndDateFor_DetailContractMarketingChannel)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{
                        Text("계약금액")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationModeFor_DetailContractMarketingChannel{
                            RightAlignedTextField(text: $contractPriceFor_DetailContractMarketingChannel)
                                .keyboardType(.default)
                                .autocapitalization(.none)
                            
                            
                            
                        }else {
                            
                            Text("\(contractPriceFor_DetailContractMarketingChannel)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack(spacing: 0){
                        Text("진행상태")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        
                        HStack(spacing: 0){
                            
                            if modificationModeFor_DetailContractMarketingChannel{
                                
                                
                                Button(action: {
                                    selecteContractStatusFor_DetailContractMarketingChannel = true
                                }, label: {
                                    
                                    if selecteContractStatusFor_DetailContractMarketingChannel{
                                        
                                        HStack(spacing: screenWidth/128.3){ // spacing 3
                                            Button(action: {
                                                selecteContractStatusFor_DetailContractMarketingChannel = false
                                                
                                                contractStatusFor_DetailContractMarketingChannel = "요청"
                                                
                                            }, label: {
                                                Text("요청")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailContractMarketingChannel = false
                                                
                                                contractStatusFor_DetailContractMarketingChannel = "진행"
                                                
                                            }, label: {
                                                Text("진행")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailContractMarketingChannel = false
                                                
                                                contractStatusFor_DetailContractMarketingChannel = "피드백"
                                                
                                            }, label: {
                                                Text("피드백")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailContractMarketingChannel = false
                                                
                                                contractStatusFor_DetailContractMarketingChannel = "완료"
                                                
                                            }, label: {
                                                Text("완료")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailContractMarketingChannel = false
                                                
                                                contractStatusFor_DetailContractMarketingChannel = "보류"
                                                
                                            }, label: {
                                                Text("보류")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                        } //HStack
                                        
                                        
                                    }else{
                                        
                                        Button(action: {
                                            selecteContractStatusFor_DetailContractMarketingChannel = true
                                            
                                        }, label: {
                                            Text("\(contractStatusFor_DetailContractMarketingChannel)")
                                                .foregroundColor(.white)
                                                .bold()
                                                .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                .background(Color("mainColor"))
                                                .cornerRadius(16.0)
                                        })
                                    }
                                    
                                })
                            } else{
                                
                                Text("\(contractStatusFor_DetailContractMarketingChannel)")
                                    .foregroundColor(.white)
                                    .bold()
                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                    .background(Color("mainColor"))
                                    .cornerRadius(16.0)
                            }
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    Divider()
                        .padding(.leading)
                    
                   
                    
                    
                    
                    HStack(spacing: 0){
                        
                        
                        VStack(alignment: .leading){// 사업자등록증 이미지
                            
                            Text("계약서 이미지")
                                .padding(.top, screenHeight/30.75)
                            
                            ZStack{
                                
                                
                                if modificationModeFor_DetailContractMarketingChannel{
                                    
                                    Button(action: {
                                        showingBusinessLicenseImagePickerFor_DetailContractMarketingChannel = true
                                    }, label: {
                                        
                                        ZStack {
                                            if let image = selectedBusniessLicenseImageFor_DetailContractMarketingChannel {
                                                Image(uiImage: image)
                                                    .resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                    .cornerRadius(12.0)
                                                
                                            } else {
                                                Rectangle()
                                                    .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                    .cornerRadius(12.0)
                                                    .foregroundColor(.colorE5E5E5)
                                                
                                            }
                                        }
                                    })
                                    
                                }else{
                                    
                                    ZStack {
                                        if let image = selectedBusniessLicenseImageFor_DetailContractMarketingChannel {
                                            Image(uiImage: image)
                                                .resizable()
                                                .aspectRatio(contentMode: .fill)
                                                .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                .cornerRadius(12.0)
                                            
                                        } else {
                                            Rectangle()
                                                .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                .cornerRadius(12.0)
                                                .foregroundColor(.colorE5E5E5)
                                            
                                        }
                                    }
                                    
                                }
                            } //ZStack 사업자등록증이미지
                            .sheet(isPresented: $showingBusinessLicenseImagePickerFor_DetailContractMarketingChannel, onDismiss: loadBusniessLicenseImageFor_DetailContractMarketingChannel) {
                                ImagePicker(selectedImage: $selectedBusniessLicenseImageFor_DetailContractMarketingChannel)
                            }
                            
                            
                            
                        }//VStack 사업자등록증 이미지
                        
                        
                        
                        Spacer()
                        
                        
                      
                    }//HStack
                    .padding(.leading)
                    
                    
                    
                } //VStack
                
                
            }//ScrollView
            .toast(isShowing: $showToast, text: Text(toastText))
            
            
            
            
            
        }//ZStack
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: {
            
            self.presentationMode.wrappedValue.dismiss()
            
            
        }) {
            
            HStack(spacing: 0){
                
                Image(systemName: "chevron.backward")
                
            }
            
            
            
        })
        .navigationBarTitle("\(selectedDetailMarketingChannelName)", displayMode: .inline)
        
      
        
        
        
    }//body

    
  
    private func loadBusniessLicenseImageFor_DetailContractMarketingChannel() {
        guard let selectedBusniessLicenseImageForDetailBrand = selectedBusniessLicenseImageFor_DetailContractMarketingChannel else { return }
        // You can do something with the selected brand image here
    }
    
    
}

#Preview {
    ContractDetailOfMarketingChannelView()
}
