//
//  DetailMarketingChannelView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/26/23.
//

import SwiftUI

struct DetailMarketingChannelView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    var selectedDetailMarketingChannelName: String = ""
    
    @State private var navigate: Bool = false
    @State private var navigateContractDetailForDetailCustomerBrandView: Bool = false
    
    @State private var showToast: Bool = false
    @State private var toastText: String = ""
    
    @State private var modificationMode: Bool = false
    
    @State private var showingImagePickerFor_DetailMarketingChannel = false
    @State private var selectedDetailMarketingChannelImage: UIImage? = nil
    
    @State private var marketingChannelNameFor_DetailMarketingChannel: String = "유튜브 광고"
    
    @State private var oneLineExplanFor_DetailMarketingChannel: String = "한줄 설명 내용"
    
    @State private var managerNameFor_DetailMarketingChannel: String = "ㅁㅁㅁ"
    @State private var managerPhoneNumberFor_DetailMarketingChannel: String = "031-123-1234"
    @State private var managerEmailFor_DetailMarketingChannel: String = "bca321@mail.com"
    
    @State private var workAreaFor_DetailMarketingChannel: String = "광고마케팅"
    @State private var selecteWorkAreaFor_DetailMarketingChannel: Bool = false
    @State private var workAreaModificationModeFor_DetailMarketingChannel: Bool = false
    
    @State private var showingBankRecordCopyImagePickerFor_DetailMarketingChannel = false
    @State private var selectedBankRecordCopyImageFor_DetailMarketingChannel: UIImage? = nil
    
    @State private var contractImageForDetailBrandContractList: UIImage? = nil
    
    
    @State private var showAllContractList: Bool = false
    @State private var showShowAllButton: Bool = false
    
    
    
    
    var body: some View {
        
        ZStack {
            ScrollView(.vertical){
                VStack(spacing: 0){
                    
                    
                    HStack{
                        
                        
                        VStack(alignment: .leading){// 브랜드 이미지
                            ZStack{
                                if modificationMode{
                                    
                                    Button(action: {
                                        showingImagePickerFor_DetailMarketingChannel = true
                                    }, label: {
                                        
                                        ZStack {
                                            if let image = selectedDetailMarketingChannelImage {
                                                Image(uiImage: image)
                                                    .resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                    .cornerRadius(12.0)
                                                
                                            } else {
                                                Rectangle()
                                                    .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                    .cornerRadius(12.0)
                                                    .foregroundColor(.colorE5E5E5)
                                                
                                            }
                                        }
                                    })
                                    
                                }else{
                                    
                                    ZStack {
                                        if let image = selectedDetailMarketingChannelImage {
                                            Image(uiImage: image)
                                                .resizable()
                                                .aspectRatio(contentMode: .fill)
                                                .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                .cornerRadius(12.0)
                                            
                                        } else {
                                            Rectangle()
                                                .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                .cornerRadius(12.0)
                                                .foregroundColor(.colorE5E5E5)
                                            
                                        }
                                    }
                                    
                                }
                            } //ZStack BrandImage
                            .sheet(isPresented: $showingImagePickerFor_DetailMarketingChannel, onDismiss: loadDetailMarketingChannelImage) {
                                ImagePicker(selectedImage: $selectedDetailMarketingChannelImage)
                            }
                            
                        }//VStack 브랜드 이미지
                        
                        
                        if modificationMode{
                            
                            TextField(".", text: $marketingChannelNameFor_DetailMarketingChannel, prompt: Text("\(marketingChannelNameFor_DetailMarketingChannel)")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.title2)
                            
                        }else {
                            
                            Text("\(marketingChannelNameFor_DetailMarketingChannel)")
                                .font(.title2)
                        }
                        
                        
                        
                        
                        
                        Spacer()
                        
                        if(modificationMode){
                            Button(action: {
                                
                                
                                if (selectedDetailMarketingChannelImage == nil || marketingChannelNameFor_DetailMarketingChannel == "" ||  managerNameFor_DetailMarketingChannel == "" || managerPhoneNumberFor_DetailMarketingChannel == "" || managerEmailFor_DetailMarketingChannel == "" || workAreaFor_DetailMarketingChannel == ""){
                                    
                                    toastText = "칸을 비울 수 없습니다."
                                    showToast = true
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                        self.showToast = false
                                        toastText = ""
                                    }
                                    
                                } else {
                                    
                                    modificationMode = false
                                }
                                
                                
                                
                            }, label: {
                                Text("저장")
                                
                            })
                        } else {
                            Button(action: {
                                
                                modificationMode = true
                                
                                
                            }, label: {
                                Text("수정")
                                
                            })
                        }
                        
                        
                    }
                    .padding(.horizontal)
                    .padding(.vertical, screenHeight / 81.5)
                    .padding(.top, screenHeight / 81.5)
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("한줄설명")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            RightAlignedTextField(text: $oneLineExplanFor_DetailMarketingChannel)
                                .keyboardType(.default)
                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(oneLineExplanFor_DetailMarketingChannel)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{
                        Text("담당자명")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            RightAlignedTextField(text: $managerNameFor_DetailMarketingChannel)
                                .keyboardType(.default)
                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(managerNameFor_DetailMarketingChannel)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("담당자 전화번호")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            RightAlignedTextField(text: $managerPhoneNumberFor_DetailMarketingChannel)
                                .keyboardType(.numberPad)
                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(managerPhoneNumberFor_DetailMarketingChannel)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{
                        Text("담당자 이메일")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            RightAlignedTextField(text: $managerEmailFor_DetailMarketingChannel)
                                .keyboardType(.emailAddress)
                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(managerEmailFor_DetailMarketingChannel)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("업무영역")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        
                        
                        if modificationMode{
                            Button(action: {
//                                selecteWorkAreaForDetailBrand = true
                            }, label: {
                                
                                //HStack 으로 버튼 세 개 만든 후 각각 터치 이벤트에 selecteWorkAreaForDetailBrand = false 처리하기
                                
                                
                                
                                
                                
                                if workAreaModificationModeFor_DetailMarketingChannel{
                                    
                                    Button(action: {
                                        workAreaModificationModeFor_DetailMarketingChannel = false
                                        
                                        workAreaFor_DetailMarketingChannel = "광고마케팅"
                                        
                                    }, label: {
                                        Text("광고마케팅")
                                            .foregroundColor(.white)
                                            .bold()
                                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                            .background(Color("mainColor"))
                                            .cornerRadius(16.0)
                                    })
                                    
                                    Button(action: {
                                        workAreaModificationModeFor_DetailMarketingChannel = false
                                        
                                        workAreaFor_DetailMarketingChannel = "MCN"
                                        
                                    }, label: {
                                        Text("MCN")
                                            .foregroundColor(.white)
                                            .bold()
                                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                            .background(Color("mainColor"))
                                            .cornerRadius(16.0)
                                    })
                                    
                                    Button(action: {
                                        workAreaModificationModeFor_DetailMarketingChannel = false
                                        
                                        workAreaFor_DetailMarketingChannel = "기타"
                                        
                                    }, label: {
                                        Text("기타")
                                            .foregroundColor(.white)
                                            .bold()
                                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                            .background(Color("mainColor"))
                                            .cornerRadius(16.0)
                                    })
                                    
                                    
                                }else{
                                    
                                    Button(action: {
                                        workAreaModificationModeFor_DetailMarketingChannel = true
                                        
                                    }, label: {
                                        Text("\(workAreaFor_DetailMarketingChannel)")
                                            .foregroundColor(.white)
                                            .bold()
                                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                            .background(Color("mainColor"))
                                            .cornerRadius(16.0)
                                    })
                                }
                                
                                
                                
                                
                            })
                        } else{
                            
                            Text("\(workAreaFor_DetailMarketingChannel)")
                                .foregroundColor(.white)
                                .bold()
                                .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                .background(Color("mainColor"))
                                .cornerRadius(16.0)
                        }
                        
                        
                    }
                    .padding(.horizontal)
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    
                    VStack(spacing: 0) {
                        HStack{
                            
                            Text("계약 리스트")
                                .font(.title2)
                                .bold()
                                .padding()
                            //                                .padding(.top, -screenHeight/13.5)
                            Spacer()
                        }
                        .padding(0)
                        
                        Button(action: {
                            print("계약 추가 버튼 클릭됨")
                            
                            self.navigate = true
                            
                        }, label: {
                            
                            NavigationLink(destination: AddContractOfMarketingChannelView(selectedDetailMarketingChannelName: selectedDetailMarketingChannelName), isActive: $navigate) {
                                EmptyView()
                            }
                            
                            Text("+  계약 추가")
                                .foregroundColor(.white)
                                .font(.system(size: 16))
                                .bold()
                                .frame(width: screenWidth/1.25, height: screenHeight/20.375)
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        
                        
                        VStack{
                            
                            ForEach(contractDummyData.sorted(by: { $0.endDate > $1.endDate }).prefix(showAllContractList ? contractDummyData.count : 3)) { contract in
                                
                                NavigationLink(destination: ContractDetailOfMarketingChannelView(selectedDetailContractNameFor_DetailContractMarketingChannel: contract.repName, selectedDetailMarketingChannelName: selectedDetailMarketingChannelName)) {
                                    
                                    VStack{
                                        HStack{ //리스트항목의 모델
                                            
                                            if let image = contract.image {
                                                Image(uiImage: image)
                                                    .resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                    .cornerRadius(12.0)
                                            } else {
                                                Rectangle()
                                                    .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                    .cornerRadius(12.0)
                                                    .foregroundColor(.colorE5E5E5)
                                            }
                                            
                                            VStack(alignment: .leading){
                                                Text(contract.repName)
                                                    .foregroundColor(.black)
                                                
                                                Text("\(contract.formattedStartDate) ~ \(contract.formattedEndDate)")
                                                    .foregroundColor(Color("color00000040"))
                                                    .font(.caption)
                                            }
                                            
                                            Spacer()
                                            
                                            Text(contract.status)
                                                .foregroundColor(.white)
                                                .bold()
                                                .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                                .background(Color("mainColor"))
                                                .cornerRadius(16.0)
                                            
                                            
                                        }
                                        
                                        Divider()
                                            .padding(.leading, screenWidth/8)
                                    }
                                    
                                }//NavigationLink
                            }
                            
                            
                            
                            
                        } //VStack 계약 리스트
                        .listStyle(PlainListStyle())
                        .padding(.horizontal)
                        .padding(.top, screenHeight/50.9375)
                        
                        
                        if showShowAllButton{
                            
                            Button(action: {
                                
                                showShowAllButton = false
                                showAllContractList = true
                                
                            }, label: {
                                
                                Text("더 보기")
                                    .foregroundColor(.black)
                                    .font(.system(size: 16))
                                    .bold()
                                    .frame(width: screenWidth/1.25, height: screenHeight/20.375)
                            })
                            .background(.white)
                            .cornerRadius(4)
                            .padding()
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color(.black), lineWidth: 1)
                                .padding()
                            )
                            
                        }
                        
                        Spacer()
                        
                        
                        
                        
                    }//VStack
                    .frame(width: screenWidth/1.0932)
                    .background(Color.white)
                    .cornerRadius(20)
                    .shadow(color: Color("color00000040"), radius: 20, x: 0, y: 30)
                    .padding(.top, screenHeight/31.346)
                    
                    //                    .sheet(isPresented: $showingBankRecordCopyImagePickerForDetailBrand, onDismiss: loadContractImageForDetailBrandContractList) {
                    //                        ImagePicker(selectedImage: $selectedBankRecordCopyImageForDetailBrand)
                    //                    }
                    
                    
                    
                    
                    
                } //VStack
                
                
            }//ScrollView
            .toast(isShowing: $showToast, text: Text(toastText))
            
            
            
            
            
        }//ZStack
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: {
            
            self.presentationMode.wrappedValue.dismiss()
            
            
        }) {
            
            HStack(spacing: 0){
                
                Image(systemName: "chevron.backward")
                
            }
            
            
            
        })
        .navigationBarTitle("마케팅 채널 상세", displayMode: .inline)
        .onAppear {
            if contractDummyData.count > 3 {
                showShowAllButton = true
            }
        }
        
        
        
        
    }//body
    
    private func loadDetailMarketingChannelImage() {
        guard let selectedDetailMarketingChannelImage = selectedDetailMarketingChannelImage else { return }
        // You can do something with the selected brand image here
    }
    
    private func loadContractImageForDetailMarketingChannelContractList() {
        guard let selectedBankRecordCopyImageFor_DetailMarketingChannel = selectedBankRecordCopyImageFor_DetailMarketingChannel else { return }
        // You can do something with the selected brand image here
    }
    
}

#Preview {
    DetailMarketingChannelView()
}
