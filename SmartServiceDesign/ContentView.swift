//
//  ContentView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/11/23.
//

import SwiftUI
import CoreData

struct ContentView: View {
    
    @State var isContentReady: Bool = false
    
    @EnvironmentObject var loginData: LoginData
    
    var body: some View{
        
        ZStack{
            
            VStack{
//                Text("ContentView")
                if loginData.isLogin{
                    MainContainer()
                }else{
                    LoginView()
                }
            }
            
            if !isContentReady {
                SplashView()
                    .background(.white)
                    .edgesIgnoringSafeArea(.all)
                
            }
            
            
        }
        .onAppear{
            // 현재로부터 1.5초 후에 execute 의 코드 실행
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                
                isContentReady = true
            })
        }
        
        
    }
    
}

#Preview {
    ContentView().environmentObject(LoginData())
}

