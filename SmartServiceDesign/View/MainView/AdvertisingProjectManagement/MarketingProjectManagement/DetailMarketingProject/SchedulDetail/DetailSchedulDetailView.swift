//
//  DetailSchedulDetailView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/27/23.
//

import SwiftUI

struct DetailSchedulDetailView: View {
    
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @State private var navigate: Bool = false
    
    @State private var showToast: Bool = false
    @State private var toastText: String = ""
    
    @State private var modificationMode: Bool = false
   
    
    var getSchedulNameFor_DetailSchedulDetail: String = "2023년 3월 마케팅"
    @State private var schedulNameFor_DetailSchedulDetail: String = ""
    
    @State private var oneLineExplanFor_DetailSchedulDetail: String = "한줄 설명 내용"
    
    @State private var contractStartDateFor_DetailCreater: String = "2023.12.01"
    @State private var contractEndDateFor_DetailCreater: String = "2023.12.31"
    
    @State private var numberOfParticipantsFor_DetailSchedulDetail: String = "31"
    
    
    @State private var selecteWorkAreaFor_DetailSchedulDetail: Bool = false
    @State private var workAreaModificationModeFor_DetailSchedulDetail: Bool = false
    
    @State private var contractImageForDetailSchedulDetailContractList: UIImage? = nil
    
    
    @State private var showAllContractList: Bool = false
    @State private var showShowAllButton: Bool = false
    
    
    
    
    var body: some View {
        
        ZStack {
            ScrollView(.vertical){
                VStack(spacing: 0){
                    
                    
                    HStack{
                        
                        
                        
                        
                        
                            
                            HStack{
                                
                                //크리에이터이름
                                if modificationMode{
                                    
                                    TextField(".", text: $schedulNameFor_DetailSchedulDetail, prompt: Text("\(schedulNameFor_DetailSchedulDetail)")
                                        .foregroundColor(Color("hintTextColor")))
                                    .keyboardType(.default)
                                    .font(.title2)
                                    .frame(width: screenWidth/1.3, height: screenHeight/29.1)
    //                                .padding(.leading, -screenWidth/150)
                                    
                                    
                                }else {
                                    
                                    Text("\(schedulNameFor_DetailSchedulDetail)")
                                        .font(.title2)
                                        .frame(height: screenHeight/29.1)
                                        
//                                        .frame(width: screenWidth/6.3)
                                }
                            }
                            
                            
                        
                        
                        
                        
                        
                        
                        
                        
                        Spacer()
                        
                        if(modificationMode){
                            Button(action: {
                                
                                
                                if (schedulNameFor_DetailSchedulDetail == "" ||
                                    oneLineExplanFor_DetailSchedulDetail == "" ||
                                    numberOfParticipantsFor_DetailSchedulDetail == ""){
                                    
                                    toastText = "칸을 비울 수 없습니다."
                                    showToast = true
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                        self.showToast = false
                                        toastText = ""
                                    }
                                    
                                } else {
                                    
                                    modificationMode = false
                                }
                                
                                
                                
                            }, label: {
                                Text("저장")
                                
                            })
                        } else {
                            Button(action: {
                                
                                modificationMode = true
                                
                                
                            }, label: {
                                Text("수정")
                                
                            })
                        }
                        
                        
                    }
                    .padding(.horizontal)
                    .padding(.vertical, screenHeight / 81.5)
                    .padding(.top, screenHeight / 81.5)
                    
                    Divider()
                        .padding(.leading)
                   

                    
                    
                    HStack{
                        
                        Text("한줄 설명")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            RightAlignedTextField(text: $oneLineExplanFor_DetailSchedulDetail)
                                .keyboardType(.numberPad)
                            
                            
                            
                        }else {
                            
                            Text("\(oneLineExplanFor_DetailSchedulDetail)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    
                    HStack{
                        Text("계약기간")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
//                            RightAlignedTextField(text: $typeOfBusinessForDetailBrand)
//                                .keyboardType(.default)
//                                .autocapitalization(.none)
                            
                            Text("\(contractStartDateFor_DetailCreater) ~ \(contractEndDateFor_DetailCreater)")
                                .foregroundColor(Color("color00000040"))

                            
                            
                        }else {
                            
                            Text("\(contractStartDateFor_DetailCreater) ~ \(contractEndDateFor_DetailCreater)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("참여인원")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            RightAlignedTextField(text: $numberOfParticipantsFor_DetailSchedulDetail)
                                .keyboardType(.numberPad)
                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(numberOfParticipantsFor_DetailSchedulDetail)명")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
              
                    
                    VStack(spacing: 0) {
                        HStack{
                            
                            Text("과업 리스트")
                                .font(.title2)
                                .bold()
                                .padding()
                            //                                .padding(.top, -screenHeight/13.5)
                            Spacer()
                        }
                        .padding(0)
                        
                        Button(action: {
                            print("계약 추가 버튼 클릭됨")
                            
                            self.navigate = true
                            
                        }, label: {
                            
                            NavigationLink(destination: AddTaskView(titleProjectName: schedulNameFor_DetailSchedulDetail), isActive: $navigate) {
                                EmptyView()
                            }
                            
                            Text("+  과업 추가")
                                .foregroundColor(.white)
                                .font(.system(size: 16))
                                .bold()
                                .frame(width: screenWidth/1.25, height: screenHeight/20.375)
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        
                        
                        VStack{//계약 리스트 터치시
                            
                            ForEach(contractDummyData.sorted(by: { $0.endDate > $1.endDate }).prefix(showAllContractList ? contractDummyData.count : 3)) { contract in
                                
                                NavigationLink(destination:
                                        DetaileTaskView(selectedSchedulName: schedulNameFor_DetailSchedulDetail)) {
                                    
                                    VStack{
                                        HStack{ //리스트항목의 모델
                                            
                                            if let image = contract.image {
                                                Image(uiImage: image)
                                                    .resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                    .cornerRadius(12.0)
                                            } else {
                                                Rectangle()
                                                    .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                    .cornerRadius(12.0)
                                                    .foregroundColor(.colorE5E5E5)
                                            }
                                            
                                            VStack(alignment: .leading){
                                                Text(contract.repName)
                                                    .foregroundColor(.black)
                                                
                                                Text("\(contract.formattedStartDate) ~ \(contract.formattedEndDate)")
                                                    .foregroundColor(Color("color00000040"))
                                                    .font(.caption)
                                            }
                                            
                                            Spacer()
                                            
                                            Text(contract.status)
                                                .foregroundColor(.white)
                                                .bold()
                                                .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                                .background(Color("mainColor"))
                                                .cornerRadius(16.0)
                                            
                                            
                                        }
                                        
                                        Divider()
                                            .padding(.leading, screenWidth/8)
                                    }
                                    
                                }//NavigationLink
                            }
                            
                            
                            
                            
                        } //VStack 계약 리스트
                        .listStyle(PlainListStyle())
                        .padding(.horizontal)
                        .padding(.top, screenHeight/50.9375)
                        
                        
                        if showShowAllButton{
                            
                            Button(action: {
                                
                                showShowAllButton = false
                                showAllContractList = true
                                
                            }, label: {
                                
                                Text("더 보기")
                                    .foregroundColor(.black)
                                    .font(.system(size: 16))
                                    .bold()
                                    .frame(width: screenWidth/1.25, height: screenHeight/20.375)
                            })
                            .background(.white)
                            .cornerRadius(4)
                            .padding()
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color(.black), lineWidth: 1)
                                .padding()
                            )
                            
                        }
                        
                        Spacer()
                        
                        
                        
                        
                    }//VStack
                    .frame(width: screenWidth/1.0932)
                    .background(Color.white)
                    .cornerRadius(20)
                    .shadow(color: Color("color00000040"), radius: 20, x: 0, y: 30)
                    .padding(.top, screenHeight/31.346)
                    
                    //                    .sheet(isPresented: $showingBankRecordCopyImagePickerForDetailBrand, onDismiss: loadContractImageForDetailBrandContractList) {
                    //                        ImagePicker(selectedImage: $selectedBankRecordCopyImageForDetailBrand)
                    //                    }
                    
                    
                    
                    
                    
                } //VStack
                
                
            }//ScrollView
            .toast(isShowing: $showToast, text: Text(toastText))
            
            
            
            
            
        }//ZStack
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: {
            
            self.presentationMode.wrappedValue.dismiss()
            
            
        }) {
            
            HStack(spacing: 0){
                
                Image(systemName: "chevron.backward")
                
            }
            
            
            
        })
        .navigationBarTitle("\(schedulNameFor_DetailSchedulDetail)", displayMode: .inline)
        .onAppear {
            if contractDummyData.count > 3 {
                showShowAllButton = true
            }
            
            schedulNameFor_DetailSchedulDetail = getSchedulNameFor_DetailSchedulDetail
        }
        
        
        
        
    }//body
    
}

#Preview {
    DetailSchedulDetailView()
}
