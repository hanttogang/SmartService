//
//  FindPasswordView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/13/23.
//

import SwiftUI

struct AddDetailScheduleView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    
    var titleProjectName: String
    
    @State var schedulName: String = ""
    @State var schedulOneLineDescription: String = ""
    
    
    @State private var today = Date()

    
    @State private var showingDatePicker = false
    
    @State private var startDate = Date()
    @State private var startDateString: String = ""
    
    @State private var endDate = Date()
    @State private var endDateString: String = ""
    
    @State private var isStartDatePickerShown = false
    @State private var isEndDatePickerShown = false
    
    //
    @State private var isSelectedCustomerBrand: String = ""
    
    //범위 지정
    var dateRange: ClosedRange<Date> {
        let min = Calendar.current.date(byAdding: .year, value: -1, to: today)!
        let max = Calendar.current.date(byAdding: .year, value: 1, to: today)!
        return min...max
    }
    
    var body: some View {
        
        
        ZStack{
            
            
            //            NavigationView{
            ZStack {
                
                VStack(spacing: 0){
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("일정명")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField(".", text: $schedulName, prompt: Text("일정명을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 이름
                    .padding(.top, screenHeight / 54.133) //15
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("한 줄 설명")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField(".", text: $schedulOneLineDescription, prompt: Text("한 줄 설명을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 이름
                    .padding(.top, screenHeight / 54.133) //15
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("일정 등록")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        HStack{
                            Button(action: {
                                
                                showingDatePicker = true
                                
                                
                            }, label: {
                                
                                HStack{
                                    TextField("\(startDateString)", text: $startDateString)
                                        .foregroundColor(Color("hintTextColor"))
                                    
                                    TextField("\(startDateString)", text: $endDateString)
                                        .foregroundColor(Color("hintTextColor"))
                                }
                                .foregroundColor(Color("hintTextColor"))
                                .font(.system(size: 14))
                                .padding(.leading, 14)
                                .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                                .overlay(RoundedRectangle(cornerRadius: 4)
                                    .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                                         
                                         
                                )
                                .overlay{
                                    Text("\(startDateString) ~ \(endDateString)")
                                        .foregroundColor(Color("hintTextColor"))
                                }
                                
                                
//                                Text("\(startDateString) ~ \(endDateString)")
                                
                              
                                
                            })
                            .sheet(isPresented: $showingDatePicker) {
                                    VStack {
                                        DatePicker("시작 날짜", selection: $startDate, displayedComponents: .date)
                                        DatePicker("종료 날짜", selection: $endDate, displayedComponents: .date)
                                        Button("완료", action: {
                                            showingDatePicker = false
                                        })
                                    }
                                }
                            //TODO                                일정 등록 관련 TextField 작성해야함
                            //
//                            TextField(".", text: $startDateString, prompt: Text("일정을 등록해주세요.")
                               
                        }
                        
                       
                    }//VStack 이름
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    
                    HStack{
                        
                        Button(action: {
                            self.presentationMode.wrappedValue.dismiss()
                        }, label: {
                            Text("일정 추가")
                                .foregroundColor(.white)
                                .font(.system(size: 16))
                                .bold()
                                .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                            
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/21.945) // 37
                        
                        
                        
                    }
                    
                    
                    Spacer()
                    
                    
                    
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: Button(action: {
                    
                    self.presentationMode.wrappedValue.dismiss()
                    
                    
                }) {
                    
                    HStack(spacing: 0){
                        
                        Image(systemName: "chevron.backward")
                        
                    }
                    
                    
                    
                })
                .navigationBarTitle("\(titleProjectName)", displayMode: .inline)
                
                
                
            }//ZStack
            .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                UIApplication.shared.endEditing()
                
            }
            
            
            
            
            
            //    .toast(isShowing: $showToast, text: Text(toastText))
            
            
        }//ZStack
        
        
        
    }
    
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        return formatter
    }()
}

#Preview {
    AddDetailScheduleView(titleProjectName: "")
}
