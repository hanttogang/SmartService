//
//  File.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/15/23.
//

import Foundation

struct Project: Identifiable {
    var id = UUID()
    var icon: String
    var companyName: String
    var projectName: String
    var participantCount: Int
}

let projectDummyData = [
    Project(icon: "building.2", companyName: "회사 A", projectName: "프로젝트 1", participantCount: 5),
    Project(icon: "building.2.fill", companyName: "회사 B", projectName: "프로젝트 2", participantCount: 10),
    Project(icon: "building.2.crop.circle", companyName: "회사 C", projectName: "프로젝트 3", participantCount: 3),
    Project(icon: "building.2.crop.circle.fill", companyName: "회사 D", projectName: "프로젝트 4", participantCount: 7)
]
