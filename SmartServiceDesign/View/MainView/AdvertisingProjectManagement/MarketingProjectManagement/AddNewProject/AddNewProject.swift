//
//  AddNewProject.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/14/23.
//

import SwiftUI

struct AddNewProjectView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    
    
    @State var projectName: String = ""
    
    @State private var navigateCustomerSearchView: Bool = false
    //
    @State private var isSelectedCustomerBrand: String = ""
    //    @State private var isSearchingCustomerBrand: Bool = false
    
    //    @Binding var isSelectedCustomerBrand: String
    //    @Binding var isSearchingCustomerBrand: Bool = false
    @State private var selectedCustomerBrand: String = ""
    @State private var searchCustomerBrand: String = ""
    @State private var editText: Bool = false
    
    
    var body: some View {
        
        if(!navigateCustomerSearchView){
            ZStack {
                
                VStack(spacing: 0){
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("고객 (브랜드)")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        HStack(spacing: 0){
                            
                            HStack(spacing: 0){
                                Text(selectedCustomerBrand)
                                    .foregroundColor(Color("hintTextColor"))
                                    .font(.system(size: 14))
                                    .padding(.leading, 14)
                                
                                Spacer()
                                
                            }
                            
                            .frame(width: screenWidth / 1.358, height: screenHeight / 18.45)
                            .background(Color("colorF8F8F8"))
                            .cornerRadius(4)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                            )
                            .padding(.trailing, screenWidth / 17.857) //21
                            
                            
                            Button(action: {
                                
                                self.navigateCustomerSearchView = true
                                
                            }, label: {
                                
                                Circle()
                                    .frame(width: screenWidth/10.135)
                                    .foregroundColor(Color("mainColor"))
                                    .overlay{
                                        
                                        Image(systemName: "magnifyingglass")
                                            .foregroundColor(.white)
                                    }
                                
                                
                                
                            })
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        
                    }//VStack 고객(브랜드)
                    .padding(.top, screenHeight / 20.82) //39
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("프로젝트명")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField(".", text: $projectName, prompt: Text("프로젝트명을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 이름
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    
                    HStack{
                        
                        Button(action: {
                            self.navigateCustomerSearchView = false
                            
                            self.presentationMode.wrappedValue.dismiss()
                            
                        }, label: {
                            
                            Text("등록")
                                .foregroundColor(.white)
                                .font(.system(size: 16))
                                .bold()
                                .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                            
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/21.945) // 37
                        
                        
                        
                    }
                    
                    
                    Spacer()
                    
                    
                    
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: Button(action: {
                    
                    self.presentationMode.wrappedValue.dismiss()
                    
                    
                }) {
                    
                    HStack(spacing: 0){
                        
                        Image(systemName: "chevron.backward")
                        
                    }
                    
                    
                    
                })
                .navigationBarTitle("신규 프로젝트 등록", displayMode: .inline)
                
                
                
            }//ZStack 기본 뷰
        }else{
            ZStack{ //검색 뷰
                VStack(spacing: 0){
                    
                    
                    HStack{
                        
                        HStack{
                            
                        }
                        .frame(width: screenWidth, height: screenHeight/14.5)
                        .overlay{
                            
                            HStack{
                                
                                //검색창을 받을수있는 택스트필드
                                TextField("Search" , text : self.$searchCustomerBrand)
                                    .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                                    .foregroundColor(Color(.black))
                                //hint와 태두리에 간격을 띄우기위해 15정도의 간격을주고
                                    .padding(10)
                                //양옆은 추가로 15를 더줌
                                    .padding(.leading, screenWidth/12.5) //30
                                //배경색상은 자유롭게선택
                                    .background(Color(.white))
                                //검색창이 너무각지면 딱딱해보이기때문에 모서리를 둥글게
                                    .frame(width: screenWidth/1.3)
                                //숫자는 취향것
                                    .cornerRadius(15)
                                //내가만든 검색창 상단에
                                //돋보기를 넣어주기위해
                                //오버레이를 선언
                                    .overlay(
                                        
                                        //가로로 view를 쌓을수있도록 하나 만들고
                                        HStack{
                                            //맨오른쪽으로 밀기위해 Spacer()로 밀어준다.
                                            //magnifyingglass 를 사용
                                            //색상은 자유롭게 변경가능
                                            Image(systemName: "magnifyingglass")
                                                .foregroundColor(Color("hintTextColor"))
                                                .padding()
                                            
                                            
                                            
                                            Spacer()
                                            
                                            if self.editText{
                                                //x버튼이미지를 클릭하게되면 입력되어있던값들을 취소하고
                                                //키입력 이벤트를 종료해야한다.
                                                Button(action : {
                                                    //                                                searchButtonClick = false
                                                    self.editText = false
                                                    self.searchCustomerBrand = ""
                                                    //키보드에서 입력을 끝내게하는 코드
                                                    UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                                                }){
                                                    Image(systemName: "multiply.circle.fill")
                                                        .foregroundColor(Color("hintTextColor"))
                                                        .padding()
                                                }
                                            }
                                            
                                            
                                        }//HStack
                                        
                                        
                                        
                                        
                                        
                                        
                                    )//overlay
                                    .onTapGesture {
                                        self.editText = true
                                    }
                                
                                
                                //                            if !searchButtonClick{
                                //                                Button(action: {
                                //                                    searchButtonClick = true
                                //
                                //
                                //                                }, label: {
                                //                                    Text("검색")
                                //                                })
                                //                                .padding(.horizontal, screenWidth/46.875)
                                //                            }else {
                                Button(action: {
                                    self.navigateCustomerSearchView = false
                                    
                                }, label: {
                                    
                                    Text("취소")
                                    
                                    
                                    
                                })
                                .padding(.horizontal, screenWidth/46.875)
                                //                            }
                                
                                
                            }
                        }//overlay
                    }//HStack 검색창
                    .background(Color("colorF8F8F892"))
                    
                    HStack{
                        Text("고객(브랜드) 검색")
                            .font(.title2)
                            .padding(.leading, screenWidth/23.4375) //16
                            .padding(.top, screenWidth/20.833) //18
                        
                        Spacer()
                    }
                    Divider() // 가로 선
                        .padding(.top, 8)
                        .padding(.leading, screenWidth/23.4375) //16
                    
                    List(dummyData.filter{searchCustomerBrand.isEmpty ? true : $0.name.contains(searchCustomerBrand)}) { company in
                        
                        
                        VStack(alignment: .leading){
                            HStack(spacing: 0){
                                
                                Text(company.name)
                                    .frame(minWidth: 0, minHeight: 0, maxHeight: .infinity)
                                    .foregroundColor(.blue)
                                
                                
                                Spacer()
                            }
                            .onTapGesture {
                                print(company.name)
                                
                                
                                selectedCustomerBrand = company.name
                                
                                self.navigateCustomerSearchView = false
                                
                            }
                            
                            
                            
                        }
                        
                        
                        
                    }
                    
                    .listStyle(PlainListStyle())
                    .background(.white)
                    
                    
                    Spacer()
                    
                } //VStack
                .navigationBarHidden(true)
                
                
                
            }//ZStack 검색 뷰
            //    .toast(isShowing: $showToast, text: Text(toastText))
            //                .navigationBarHidden(true)
            .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                UIApplication.shared.endEditing()
                
            }
            
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }
}

#Preview {
    AddNewProjectView()
}
