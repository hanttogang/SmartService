//
//  LoginResult.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/11/23.
//

import Foundation

struct UserData: Codable {
    let deleteDt: String?
    let firstCreateDt: String
    let lastUpdateDt: String
    let userEmail: String
    let userIdx: Int
    let userName: String
    let userPhone: String
    let userType: String

    enum CodingKeys: String, CodingKey {
        case deleteDt = "delete_dt"
        case firstCreateDt = "first_create_dt"
        case lastUpdateDt = "last_update_dt"
        case userEmail = "user_email"
        case userIdx = "user_idx"
        case userName = "user_name"
        case userPhone = "user_phone"
        case userType = "user_type"
    }
}

struct SuccessResult: Codable {
    let data: UserData
    let result: Int
    let token: String
}
