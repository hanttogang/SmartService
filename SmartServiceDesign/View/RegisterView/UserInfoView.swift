//
//  UserInfoView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/12/23.
//

import SwiftUI
import Alamofire

enum Flavor: String, CaseIterable, Identifiable {
    case employee = "소속직원"
    case creater = "크리에이터"
    
    var id: Self { self }
}

struct UserInfoView: View {
    
    let defaultUrl = "\(ApiClient.BASE_URL)"
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @State private var showToast: Bool = false
    @State private var toastText: String = ""
    
    @State private var inputEmailInfo: String = ""
    @State private var inputPasswordInfo: String = ""
    @State private var inputCheckPasswordtInfo: String = ""
    @State private var inputNameInfo: String = ""
    @State private var inputPhoneNumInfo: String = ""
    
    @State private var AlarmAgreeChecked: Bool = true
    
    @State private var navigate: Bool = false
    
    @State private var selectedUserType: Flavor = Flavor.employee
//    @State private var initSelectedUserType = Flavor.employee
    
    
    init() {
        let appearance = UINavigationBarAppearance()
        //        appearance.configureWithOpaqueBackground()
        //        appearance.backgroundColor = UIColor(named: "textColorHint") // 원하는 색상으로 변경하세요.
        appearance.shadowColor = nil // 이 부분을 추가하면 네비게이션 바 하단 선이 사라집니다.
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().compactAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        
//        // Picker의 배경색을 하얀색으로 설정
//        UISegmentedControl.appearance().backgroundColor = UIColor.white
//        // 선택된 세그먼트의 색상을 파란색으로 설정
//        UISegmentedControl.appearance().selectedSegmentTintColor = .systemBlue
//
//        UISegmentedControl.appearance().setTitleTextAttributes([.foregroundColor: UIColor.white], for: .selected)
//        
//        // 세그먼트의 텍스트 색상을 하얀색으로 설정
//        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
//            UISegmentedControl.appearance().setTitleTextAttributes(attributes, for: .selected)
//        
//        // 선택되지 않은 세그먼트의 텍스트 색상을 파란색으로 설정
//        let attributesBlue = [NSAttributedString.Key.foregroundColor: UIColor.systemBlue]
//            UISegmentedControl.appearance().setTitleTextAttributes(attributesBlue, for: .normal)
        
    }
    
    
    
    var body: some View {
        
        
        
        NavigationView{
            ZStack{
                ScrollView{
                    
                    
                    VStack{ //회원분류
                        
                        HStack(spacing: 0){
                            Text("회원 분류")
                                .font(.system(size: 14))
                                
                            Text("  *")
                                .font(.system(size: 14))
                                .foregroundColor(.red)
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack(spacing: 0) {
                            ForEach(Flavor.allCases, id: \.self) { flavor in
                                Button(action: {
                                    selectedUserType = flavor
                                    
                                    print("\(flavor)")
                                }) {
                                    Text(flavor == .employee ? "소속직원" : "크리에이터")
                                        .frame(width: screenWidth/2.272, height: screenHeight/23.97)
                                        .font(.system(size: 16))
                                    
                                        
                                    
                                }
                                .background(selectedUserType == flavor ? Color.blue : Color.clear)
                                .foregroundColor(selectedUserType == flavor ? .white : .blue)
                                .cornerRadius(4)
                                .overlay(RoundedRectangle(cornerRadius: 4)
                                    .stroke(Color("mainColor"), lineWidth: 1)
                                )
                                
                                
                                
                                
                            }
                        }
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("mainColor"), lineWidth: 1)
                        )
                        
                        
//                        Picker("UserType", selection: $selectedUserType) {
//                            
//                            Text("소속직원").tag(Flavor.employee)
//                            
//                            
//                            Text("크리에이터").tag(Flavor.creater)
//                            
//                        }
//                        .padding(.horizontal, screenWidth / 15.625)
//                        .pickerStyle(.segmented)
                        
                        
                        
                    } //VStack 회원분류
                    .padding(.top, screenHeight / 61.6)
                    
                    
                    VStack{ //이메일 입력
                        
                        HStack(spacing: 0){
                            Text("이메일")
                                .font(.system(size: 14))
                            Text("  *")
                                .font(.system(size: 14))
                                .foregroundColor(.red)
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField(".", text: $inputEmailInfo, prompt: Text("이메일을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.namePhonePad)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    } //VStack 아이디
                    .padding(.top, screenHeight / 61.6)
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("비밀번호")
                                .font(.system(size: 14))
                            Text("  *")
                                .font(.system(size: 14))
                                .foregroundColor(.red)
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        HStack{
                            Text("영문, 숫자, 특수문자 조합으로 8자리 이상 입력해주세요.")
                                .font(.caption)
                                .foregroundColor(Color("hintTextColor"))
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        HStack{
                            SecureField(".", text: $inputPasswordInfo, prompt: Text("비밀번호를 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.numbersAndPunctuation)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    } //VStack 비밀번호
                    .padding(.top, screenHeight / 77.0)
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("비밀번호 확인")
                                .font(.system(size: 14))
                            Text("  *")
                                .font(.system(size: 14))
                                .foregroundColor(.red)
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            SecureField(".", text: $inputCheckPasswordtInfo, prompt: Text("비밀번호를 확인해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.numbersAndPunctuation)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 비밀번호 확인
                    .padding(.top, screenHeight / 77.0)
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("이름")
                                .font(.system(size: 14))
                            Text("  *")
                                .font(.system(size: 14))
                                .foregroundColor(.red)
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField(".", text: $inputNameInfo, prompt: Text("이름을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 이름
                    .padding(.top, screenHeight / 77.0)
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("휴대폰번호")
                                .font(.system(size: 14))
                            Text("  *")
                                .font(.system(size: 14))
                                .foregroundColor(.red)
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField(".", text: $inputPhoneNumInfo, prompt: Text("휴대폰번호를 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.numberPad)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 휴대폰번호
                    .padding(.top, screenHeight / 77.0)
                    
                    HStack{
                        
                        Button {
                            
                            AlarmAgreeChecked.toggle()
                            
                        } label: {
                            Image(systemName: AlarmAgreeChecked ? "checkmark.square.fill" : "square")
                                .foregroundColor(.blue)
                                .cornerRadius(4)

                            
                            Text("알림 동의 체크")
                                .font(.system(size: 14))
                                .foregroundColor(.black)
                        }
                        
                        Spacer()
                    }
                    .padding(.leading, screenWidth / 15.625)
                    .padding(.vertical, screenHeight / 25.375)
                    
                    
                    HStack{
                        
                        Button(action: {
                            print("회원가입 이전 버튼 클릭")
                            
                            self.presentationMode.wrappedValue.dismiss()
                            
                        }, label: {
                            Text("이전")
                                .foregroundColor(Color("color828282"))
                                .font(.system(size: 16))
                                .bold()
                                .frame(width: screenWidth / 3.125, height: screenHeight / 15.03)
                        })
                        .background(Color("colorE0E0E0"))
                        .cornerRadius(4)
                        
                        
                        
                        
                        Button(action: {
                            print("회원가입 다음 버튼 클릭")
                            
                            if(inputEmailInfo == "" || inputPasswordInfo == "" || inputCheckPasswordtInfo == "" || inputNameInfo == "" || inputPhoneNumInfo == "" ){
                                
                                toastText = "칸을 비울 수 없습니다."
                                showToast = true
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                    self.showToast = false
                                    toastText = ""
                                }
                                
                                //뷰 테스트를 위해 임시로 true
                                self.navigate = true
                                
                            }else if(inputPasswordInfo != inputCheckPasswordtInfo){
                                toastText = "비밀번호를 확인해주세요"
                                showToast = true
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                    self.showToast = false
                                    toastText = ""
                                }
                            }else if (!validatePassword(password: inputPasswordInfo)){
                                toastText = "비밀번호 규정을 확인하세요"
                                showToast = true
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                    self.showToast = false
                                    toastText = ""
                                }
                            } else if (!validateEmail(email: inputEmailInfo)){
                                toastText = "올바른 이메일을 입력해주세요"
                                showToast = true
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                    self.showToast = false
                                    toastText = ""
                                }
                            }else{
                                
                                registerUser(email: inputEmailInfo, password: inputPasswordInfo, userName: inputNameInfo, userType: "\(selectedUserType)", userPhone: inputPhoneNumInfo)

                                
                                
                            }
                            
                            
                            
                        }, label: {
                            Text("다음")
                                .foregroundColor(.white)
                                .font(.system(size: 16))
                                .bold()
                                .frame(width: screenWidth / 1.893, height: screenHeight / 15.03)
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        
                        
                        
                        NavigationLink(destination: RegistrationCompletedView(), isActive: $navigate) {
                            EmptyView()
                        }
                        .hidden() // NavigationLink를 숨김
                        
                    }//HStack 하단 버튼
                    .padding(.bottom, screenHeight/25.375)
                    
                    
                    
                    
                }//ScrollView
                
                
                
            }//ZStack
            .navigationBarBackButtonHidden(true)
            .navigationBarItems(leading: Button(action: {
                self.presentationMode.wrappedValue.dismiss()
            }) {
                
                HStack(spacing: 0){
                    
                    Image(systemName: "chevron.backward")
                    
                }
                
                
                
            })
            .navigationBarTitle("회원 정보")
            
        }//NavigationView
        .toast(isShowing: $showToast, text: Text(toastText))
        .navigationBarHidden(true)
        .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
            UIApplication.shared.endEditing()
            
        }
        
        
    }
    
    func validatePassword(password: String) -> Bool {
        // 정규표현식: 최소 하나의 소문자, 최소 하나의 대문자, 최소 하나의 숫자, 최소 하나의 특수문자를 포함하며, 총 8자 이상
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*\\d)(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}")
        return passwordTest.evaluate(with: password)
    }
    
    func validateEmail(email: String) -> Bool {
        // 정규표현식: 이메일 형식
        let emailTest = NSPredicate(format: "SELF MATCHES %@", "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}")
        return emailTest.evaluate(with: email)
        
        /*
         [A-Z0-9a-z._%+-]+: 알파벳 대소문자, 숫자, 그리고 ., _, %, +, -를 포함하는 하나 이상의 문자
         @: @ 문자
         [A-Za-z0-9.-]+: 알파벳 대소문자, 숫자, 그리고 ., -를 포함하는 하나 이상의 문자
         \\.: . 문자
         [A-Za-z]{2,64}: 알파벳 대소문자 2개 이상 64개 이하
         */
    }
    
    func registerUser(email: String, password: String, userName: String, userType: String, userPhone: String) {
        let parameters: [String: Any] = [
            "user_email": email,
            "user_pwd": password,
            "user_name": userName,
            "user_type": userType,
            "user_phone": userPhone
        ]

        AF.request("\(defaultUrl)/api/auth/signUp", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                switch response.result {
                    
                case .success(let value):
                    
                    print("api 연결 성공: \(value)")
                    
                    guard let json = value as? [String: Any],
                                      let result = json["result"] as? Int else {
                                    print("응답 형식이 올바르지 않습니다: \(value)")
                                    return
                                }

                                if result == 0 {
                                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                                    print("회원가입 실패: \(errorMessage ?? "알 수 없는 오류")")
                                } else {
                                    print("회원가입 성공: \(value)")
                                    
                                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                                    self.navigate = true
                                    
                                }
                    
                    
                    
                case .failure(let error):
                    // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                    print("회원가입 실패: \(error)")
                    
                }
            }
    }
    
    
//    func register(registerUser: RegisterUser, completion: @escaping (Result<Data?, AFError>) -> Void) {
//        let url = "\(ApiClient.BASE_URL)/api/auth/signup"
//        let headers: HTTPHeaders = [
//            "accept": "application/json",
//            "Content-Type": "application/json"
//        ]
//        
//        AF.request(url, method: .post, parameters: registerUser, encoder: JSONParameterEncoder.default, headers: headers)
//            .validate()
//            .response { response in
//                switch response.result {
//                case .success(let data):
//                    completion(.success(data))
//                case .failure(let error):
//                    if let data = response.data {
//                        let decoder = JSONDecoder()
//                        if let errorResponse = try? decoder.decode(ErrorResponse.self, from: data),
//                           errorResponse.err.status == 400 && errorResponse.err.errorMessage == "Duplicate ID" {
//                            print("called - register:  중복된 아이디")
//                            
//                            toastText = "이미 가입된 아이디입니다."
//                            showToast = true
//                            
//                            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//                                self.showToast = false
//                                toastText = ""
//                            }
//                            
//                        }
//                    } else {
//                        completion(.failure(error))
//                    }
//                }
//            }
//    }
    
    
}

#Preview {
    UserInfoView()
}
