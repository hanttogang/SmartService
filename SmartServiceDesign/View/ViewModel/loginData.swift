//
//  loginData.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/11/23.
//

import Foundation

class LoginData: ObservableObject {
    
    
    @Published var isLogin: Bool = false
    @Published var token: String = ""
    
    @Published var userData: UserData?
    
}
