//
//  Company.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/14/23.
//

import Foundation

struct Company: Identifiable {
    var id = UUID()
    var name: String
}

let dummyData = [
    Company(name: "Apple"),
    Company(name: "Google"),
    Company(name: "Microsoft"),
    Company(name: "Facebook"),
    Company(name: "Amazon"),
    Company(name: "Netflix"),
    Company(name: "Samsung"),
    Company(name: "LG"),
    Company(name: "Kakao"),
    Company(name: "Naver")
]
