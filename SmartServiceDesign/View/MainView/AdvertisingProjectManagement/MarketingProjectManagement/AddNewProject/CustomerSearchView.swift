////
////  CustomerSearchView.swift
////  SmartServiceDesign
////
////  Created by Teameverywhere on 12/14/23.
////
//
//import SwiftUI
//
//struct CustomerSearchView: View {
//    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
//    
//    let screenWidth = UIScreen.main.bounds.width
//    let screenHeight = UIScreen.main.bounds.height
//    
//    @State private var searchCustomerBrand: String = ""
//    @State private var editText: Bool = false
//    
//    @State private var searchCancelButtonClick: Bool = false
//    
//    @State private var selectedCustomerList: Company?
//    @State private var isSearching = false
//    @State private var showAddNewProject = false
//    @State private var selectedCustomerBrand: String = ""
//    
//    
//    var body: some View {
//        
//        ZStack{
//            VStack(spacing: 0){
//                
//                
//                HStack{
//                    
//                    HStack{
//                        
//                    }
//                    .frame(width: screenWidth, height: screenHeight/14.5)
//                    .overlay{
//                        
//                        HStack{
//                            
//                            //검색창을 받을수있는 택스트필드
//                            TextField("Search" , text : self.$searchCustomerBrand)
//                                .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
//                                .foregroundColor(Color(.black))
//                            //hint와 태두리에 간격을 띄우기위해 15정도의 간격을주고
//                                .padding(10)
//                            //양옆은 추가로 15를 더줌
//                                .padding(.leading, screenWidth/12.5) //30
//                            //배경색상은 자유롭게선택
//                                .background(Color(.white))
//                            //검색창이 너무각지면 딱딱해보이기때문에 모서리를 둥글게
//                                .frame(width: screenWidth/1.3)
//                            //숫자는 취향것
//                                .cornerRadius(15)
//                            //내가만든 검색창 상단에
//                            //돋보기를 넣어주기위해
//                            //오버레이를 선언
//                                .overlay(
//                                    
//                                    //가로로 view를 쌓을수있도록 하나 만들고
//                                    HStack{
//                                        //맨오른쪽으로 밀기위해 Spacer()로 밀어준다.
//                                        //magnifyingglass 를 사용
//                                        //색상은 자유롭게 변경가능
//                                        Image(systemName: "magnifyingglass")
//                                            .foregroundColor(Color("hintTextColor"))
//                                            .padding()
//                                        
//                                        
//                                        
//                                        Spacer()
//                                        
//                                        if self.editText{
//                                            //x버튼이미지를 클릭하게되면 입력되어있던값들을 취소하고
//                                            //키입력 이벤트를 종료해야한다.
//                                            Button(action : {
////                                                searchButtonClick = false
//                                                self.editText = false
//                                                self.searchCustomerBrand = ""
//                                                //키보드에서 입력을 끝내게하는 코드
//                                                UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
//                                            }){
//                                                Image(systemName: "multiply.circle.fill")
//                                                    .foregroundColor(Color("hintTextColor"))
//                                                    .padding()
//                                            }
//                                        }
//                                        
//                                        
//                                    }//HStack
//                                    
//                                    
//                                    
//                                    
//                                    
//                                    
//                                )//overlay
//                                .onTapGesture {
//                                    self.editText = true
//                                }
//                            
//                            
////                            if !searchButtonClick{
////                                Button(action: {
////                                    searchButtonClick = true
////                                    
////                                    
////                                }, label: {
////                                    Text("검색")
////                                })
////                                .padding(.horizontal, screenWidth/46.875)
////                            }else {
//                                Button(action: {
////                                    searchButtonClick = false
////                                    self.editText = false
////                                    
////                                    searchCustomerBrand = ""
////                                    isSearching = false
////                                    
////                                    searchCancelButtonClick = true
//                                    
//                                    self.presentationMode.wrappedValue.dismiss()
//                                    
//                                }, label: {
//                                    
//                                        Text("취소")
//                                    
//                                    
//                                    
//                                })
//                                .padding(.horizontal, screenWidth/46.875)
////                            }
//                            
//                            
//                        }
//                    }//overlay
//                }//HStack 검색창
//                .background(Color("colorF8F8F892"))
//                
//                HStack{
//                    Text("고객(브랜드) 검색")
//                        .font(.title2)
//                        .padding(.leading, screenWidth/23.4375) //16
//                        .padding(.top, screenWidth/20.833) //18
//                    
//                    Spacer()
//                }
//                Divider() // 가로 선
//                    .padding(.top, 8)
//                    .padding(.leading, screenWidth/23.4375) //16
//                
//                List(dummyData.filter{ searchCustomerBrand.isEmpty ? true : $0.name.contains(searchCustomerBrand)}) { company in
//                    
//                    NavigationLink(destination: AddNewProjectView() {
//                        VStack(alignment: .leading){
//                            HStack(spacing: 0){
//                                
//                                Text(company.name)
//                                    .frame(minWidth: 0, minHeight: 0, maxHeight: .infinity)
//                                    .foregroundColor(.blue)
//                                    
//                                
//                                Spacer()
//                            }
//                           
//                            
//                        }
//    //                    .onTapGesture {
//    //                            print(company.name)
//    //
//    //                            isSearching = false
//    //                            selectedCustomerBrand = company.name
//    //
//    //                            showAddNewProject = true
//    //                        }
//                    }
//                    
//                    
//                    
//                }
//                
//                .listStyle(PlainListStyle())
//                .background(.white)
//                
//                
//                Spacer()
//                
//            } //VStack
//            .navigationBarHidden(true)
//            .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
//                UIApplication.shared.endEditing()
//                
//            }
//
//            
//        }//ZStack
//        
//    }
//}
//
//#Preview {
//    CustomerSearchView()
//}
