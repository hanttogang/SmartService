//
//  RegistrationCustomerBrand.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 1/4/24.
//

import Foundation

class RegistrationCustomerBrandData: ObservableObject {
    @Published var brandName: String = ""
    @Published var registrationNumber: String = ""
    @Published var businessType: String = ""
    @Published var address: String = ""
    @Published var addressDetail: String = ""
    @Published var representativeName: String = ""
    @Published var representativePhone: String = ""
    @Published var representativeEmail: String = ""
    @Published var managerName: String = ""
    @Published var managerPhone: String = ""
    @Published var managerEmail: String = ""
    @Published var manageType: String = ""
    @Published var brandImage: String = ""
    @Published var registrationImage: String = ""
    @Published var bankbookCopy: String = ""
}
