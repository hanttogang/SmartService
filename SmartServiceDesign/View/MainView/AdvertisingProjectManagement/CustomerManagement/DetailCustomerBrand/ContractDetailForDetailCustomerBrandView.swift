//
//  ContractDetailForDetailCustomerBrandView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/22/23.
//

import SwiftUI
import Alamofire

struct ContractDetailForDetailCustomerBrandView: View {
    
    var selectedCustomerBrandContractIdx: Int
    // 이 init 메소드를 통해 외부에서 selectedCustomerBrandContractIdx 값을 설정할 수 있습니다.
    init(selectedCustomerBrandContractIdx: Int) {
        self.selectedCustomerBrandContractIdx = selectedCustomerBrandContractIdx
    }
    
    
    //Api
    @EnvironmentObject var loginData: LoginData
    let defaultUrl = "\(ApiClient.BASE_URL)"
    let imageS3Url = "https://smart-service.s3.ap-northeast-2.amazonaws.com"
    
    @State private var contractImageStringFor_DetailContractBrand = ""
    
    @State private var contractDataList = [ContractData]()
    @State private var contractCount: Int = 0
    
    
    
    
    
    //View
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    var selectedDetailCustomerName: String = ""
    
    @State private var navigate: Bool = false
    
    @State private var showToast: Bool = false
    @State private var toastText: String = ""
    
    @State private var modificationModeFor_DetailContractBrand: Bool = false
    
    
    @State private var contractNameFor_DetailContractBrand: String = "유튜브 PPL 계약"
    @State private var contractOneLineExplanFor_DetailContractBrand: String = "위해브 유튜브 PPL 계약"
    @State private var contractStartDateFor_DetailContractBrand: String = "2023.12.01"
    @State private var contractEndDateFor_DetailContractBrand: String = "2023.12.31"
    @State private var contractPriceFor_DetailContractBrand: String = "0"
    @State private var contractStatusFor_DetailContractBrand: String = ""
    
    
    @State private var selecteContractStatusFor_DetailContractBrand: Bool = false
    @State private var contractStatusModificationModeFor_DetailContractBrand: Bool = false
    
    @State private var showingBusinessLicenseImagePickerForDetailBrand = false
    @State private var selectedBusniessLicenseImageForDetailBrand: UIImage? = nil
    
    
    
    
    var body: some View {
        
        ZStack {
            ScrollView(.vertical){
                VStack(spacing: 0){
                    
                    HStack{
                        
                        
                        
                        if modificationModeFor_DetailContractBrand{
                            
                            TextField(".", text: $contractNameFor_DetailContractBrand, prompt: Text("\(contractNameFor_DetailContractBrand)")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.title2)
                            
                            
                        }else {
                            
                            Text("\(contractNameFor_DetailContractBrand)")
                                .font(.title2)
                        }
                        
                        
                        
                        
                        
                        Spacer()
                        
                        if(modificationModeFor_DetailContractBrand){
                            Button(action: {
                                
                                
                                if (contractNameFor_DetailContractBrand == "" || contractOneLineExplanFor_DetailContractBrand == "" || contractStartDateFor_DetailContractBrand == "" || contractEndDateFor_DetailContractBrand == "" || contractPriceFor_DetailContractBrand == ""){
                                    
                                    toastText = "칸을 비울 수 없습니다."
                                    showToast = true
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                        self.showToast = false
                                        toastText = ""
                                    }
                                    
                                } else {
                                    patchCustomerBrandContract()
                                    modificationModeFor_DetailContractBrand = false
                                }
                                
                                
                                
                            }, label: {
                                Text("저장")
                                
                            })
                        } else {
                            Button(action: {
                                
                                modificationModeFor_DetailContractBrand = true
                                
                                
                            }, label: {
                                Text("수정")
                                
                            })
                            
                        }
                        
                        
                    }// HStack 계약 이름
                    .padding(.horizontal)
                    .padding(.vertical, screenHeight / 81.5)
                    //                    .padding(.top, screenHeight / 81.5)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        
                        Text("한줄 설명")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationModeFor_DetailContractBrand{
                            RightAlignedTextField(text: $contractOneLineExplanFor_DetailContractBrand)
                                .keyboardType(.numberPad)
                            
                            
                            
                        }else {
                            
                            Text("\(contractOneLineExplanFor_DetailContractBrand)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("계약기간")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationModeFor_DetailContractBrand{
                            //                            RightAlignedTextField(text: $typeOfBusinessForDetailBrand)
                            //                                .keyboardType(.default)
                            //                                .autocapitalization(.none)
                            
                            Text("\(contractStartDateFor_DetailContractBrand) ~ \(contractStartDateFor_DetailContractBrand)")
                                .foregroundColor(Color("color00000040"))
                            
                            
                            
                        }else {
                            
                            Text("\(contractStartDateFor_DetailContractBrand) ~ \(contractStartDateFor_DetailContractBrand)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{
                        Text("계약금액")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationModeFor_DetailContractBrand{
                            RightAlignedTextField(text: $contractPriceFor_DetailContractBrand)
                                .keyboardType(.default)
                                .autocapitalization(.none)
//                            
//                            TextField("계약금액을 입력해주세요", text: $contractPriceForCustomerBrandString, onCommit: {
//                                if let validNumber = Int(contractPriceForCustomerBrandString) {
//                                    contractPriceForCustomerBrand = validNumber
//                                }
//                            
                            
                        }else {
                            
                            Text("\(contractPriceFor_DetailContractBrand)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack(spacing: 0){
                        Text("진행상태")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        
                        HStack(spacing: 0){
                            
                            if modificationModeFor_DetailContractBrand{
                                
                                
                                Button(action: {
                                    selecteContractStatusFor_DetailContractBrand = true
                                }, label: {
                                    
                                    if selecteContractStatusFor_DetailContractBrand{
                                        
                                        HStack(spacing: screenWidth/128.3){ // spacing 3
                                            Button(action: {
                                                selecteContractStatusFor_DetailContractBrand = false
                                                
                                                contractStatusFor_DetailContractBrand = "요청"
                                                
                                            }, label: {
                                                Text("요청")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailContractBrand = false
                                                
                                                contractStatusFor_DetailContractBrand = "진행"
                                                
                                            }, label: {
                                                Text("진행")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailContractBrand = false
                                                
                                                contractStatusFor_DetailContractBrand = "피드백"
                                                
                                            }, label: {
                                                Text("피드백")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailContractBrand = false
                                                
                                                contractStatusFor_DetailContractBrand = "완료"
                                                
                                            }, label: {
                                                Text("완료")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailContractBrand = false
                                                
                                                contractStatusFor_DetailContractBrand = "보류"
                                                
                                            }, label: {
                                                Text("보류")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                        } //HStack
                                        
                                        
                                    }else{
                                        
                                        Button(action: {
                                            selecteContractStatusFor_DetailContractBrand = true
                                            
                                        }, label: {
                                            Text("\(contractStatusFor_DetailContractBrand)")
                                                .foregroundColor(.white)
                                                .bold()
                                                .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                .background(Color("mainColor"))
                                                .cornerRadius(16.0)
                                        })
                                    }
                                    
                                })
                            } else{
                                
                                Text("\(contractStatusFor_DetailContractBrand)")
                                    .foregroundColor(.white)
                                    .bold()
                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                    .background(Color("mainColor"))
                                    .cornerRadius(16.0)
                            }
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    
                    
                    
                    HStack(spacing: 0){
                        
                        
                        VStack(alignment: .leading){// 계약서 이미지
                            
                            Text("계약서 이미지")
                                .padding(.top, screenHeight/30.75)
                            
                            ZStack{
                                
                                
                                if modificationModeFor_DetailContractBrand{
                                    
                                    Button(action: {
                                        showingBusinessLicenseImagePickerForDetailBrand = true
                                    }, label: {
                                        
                                        ZStack {
                                            if let image = selectedBusniessLicenseImageForDetailBrand {
                                                Image(uiImage: image)
                                                    .resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                    .cornerRadius(12.0)
                                                
                                            } else {
                                                
                                                if contractImageStringFor_DetailContractBrand != ""{
                                                    
                                                    
                                                    AsyncImage(url: URL(string: imageS3Url + "/" + contractImageStringFor_DetailContractBrand)) { image in
                                                        image.resizable()
                                                             .aspectRatio(contentMode: .fill)
                                                             .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                             .cornerRadius(12.0)
                                                    } placeholder: {
                                                        ProgressView()
                                                            .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                    }
                                                    
                                                }
                                            }
                                            
                                            
                                        }
                                    })
                                    
                                }else{
                                    
                                    ZStack {
                                        if let image = selectedBusniessLicenseImageForDetailBrand {
                                            Image(uiImage: image)
                                                .resizable()
                                                .aspectRatio(contentMode: .fill)
                                                .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                .cornerRadius(12.0)
                                            
                                        } else {
                                            
                                            if contractImageStringFor_DetailContractBrand != ""{
                                                
                                                
                                                AsyncImage(url: URL(string: imageS3Url + "/" + contractImageStringFor_DetailContractBrand)) { image in
                                                    image.resizable()
                                                         .aspectRatio(contentMode: .fill)
                                                         .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                         .cornerRadius(12.0)
                                                } placeholder: {
                                                    ProgressView()
                                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                }
                                                
                                            }
                                        }
                                        
                                        
                                    }
                                    
                                }
                            } //ZStack 사업자등록증이미지
                            .sheet(isPresented: $showingBusinessLicenseImagePickerForDetailBrand, onDismiss: loadBusniessLicenseImage) {
                                ImagePicker(selectedImage: $selectedBusniessLicenseImageForDetailBrand)
                            }
                            
                            
                            
                        }//VStack 사업자등록증 이미지
                        
                        
                        
                        Spacer()
                        
                        
                        
                    }//HStack
                    .padding(.leading)
                    
                    
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    HStack(spacing: 0){
                        Image(systemName: "chevron.backward")
                    }
                })
                .navigationBarTitle("\(selectedDetailCustomerName) 계약 상세", displayMode: .inline)
                .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                    UIApplication.shared.endEditing()
                }
                
                
                
            }//ScrollView
            
            
            
        }//ZStack
        .toast(isShowing: $showToast, text: Text(toastText))
        
        .onAppear {
            contractInquiry()
        }
        
        
        
        
    }//body
    
    
    
    private func loadBusniessLicenseImage() {
        guard let selectedBusniessLicenseImageForDetailBrand = selectedBusniessLicenseImageForDetailBrand else { return }
        // You can do something with the selected brand image here
        uploadImage(image: selectedBusniessLicenseImageForDetailBrand, imageType: "contract")
        print(contractImageStringFor_DetailContractBrand)
    }
    
    
    //Api 를통해 필요한 값 호출 (url, key 값)
    private func getApiCommonBrandImageURL(imageType: String, completion: @escaping (String?, String?) -> Void) {
        
        let headers: HTTPHeaders = [
            "accept": "application/json",
            "Authorization": "Bearer \(loginData.token)"
        ]
        
        let parameters: Parameters = [
            "mimetype": "image/png",
            "type": "brand",
            "extension": "png"
        ]
        
        AF.request("\(defaultUrl)/api/common/image/url", method: .get, parameters: parameters, headers: headers).responseJSON { response in
            switch response.result {
            case .success(let value):
                if let JSON = value as? [String: Any] {
                    let url = JSON["data"] as? String
                    let key = JSON["key"] as? String
                    completion(url, key)
                }
            case .failure(let error):
                print(error)
                completion(nil, nil)
            }
        }
    }
    
    //사진 선택시 uploadImag 함수를 호출
    private func uploadImage(image: UIImage, imageType: String) {
        getApiCommonBrandImageURL(imageType: imageType) { (url, key) in
            
            guard let preSignedUrl = url else {
                print("Failed to get pre-signed URL.")
                return
            }
            
            guard let imageData = image.pngData() else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: url!, method: .put).response { response in
                switch response.result {
                case .success:
                    print("Image uploaded successfully.")
                case .failure(let error):
                    print("Failed to upload image: \(error)")
                }
            }
            
            
            guard let imageData = image.jpegData(compressionQuality: 1.0) else {
                print("Could not convert image to data.")
                return
            }
            
            AF.upload(imageData, to: preSignedUrl).response { response in
                if let error = response.error {
                    print("Image upload failed with error: \(error)")
                } else {
                    print("Image uploaded successfully.")
                    switch imageType {
                    case "contract":
                        contractImageStringFor_DetailContractBrand = key!
                        
                        print("\(contractImageStringFor_DetailContractBrand)")
                   
                        
                    default:
                        print("Unknown image type: \(imageType)")
                    }
                }
            }
        }
    }
    
    
    
    
    
    private func dateFormatter(getDate: String) -> String {
        
        let isoDateFormatter = ISO8601DateFormatter()
        isoDateFormatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]

        let getDate = getDate
        if let date = isoDateFormatter.date(from: getDate) {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy.MM.dd"
            let dateString = dateFormatter.string(from: date)
//            print(dateString) //ex "2024.01.05" 출력
            return dateString
        } else {
            print("날짜 변환에 실패했습니다.")
            return ""
        }

    }
    
    
    
    //계약 가져오는 api
    private func contractInquiry() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        AF.request("\(defaultUrl)/api/brand-contract?brand_contract_idx=\(selectedCustomerBrandContractIdx)&page=1&limit=100", method: .get, headers: headers).responseDecodable(of: ContractDataResponse.self) { response in
            switch response.result {
    
            case .success(let response):
                // 성공적으로 데이터를 받아왔을 때 로직을 작성합니다.
                
                for contractData in response.data {
                    
                    print("광고 번호: \(contractData.brandContractIdx)")
                    
                    contractNameFor_DetailContractBrand = contractData.contractName
                    contractOneLineExplanFor_DetailContractBrand = contractData.oneLineExplan
                    contractStartDateFor_DetailContractBrand = dateFormatter(getDate: contractData.contractStartDt)
                    contractEndDateFor_DetailContractBrand = dateFormatter(getDate: contractData.contractEndDt)
                    contractStatusFor_DetailContractBrand = contractData.contractStatus
                    contractPriceFor_DetailContractBrand = String(contractData.contractPrice)
                    contractImageStringFor_DetailContractBrand = contractData.contractImage
                    
                    
                }
                
                
                DispatchQueue.main.async {
//                    contractDataList = response.data
//                    
//                    contractCount = response.pagination.total
                    
//                    print("\(selectedCustomerBrandContractIdx) 계약 리스트 호출 성공 \(response.data)")
//                    
                }
                
                
                
                
            case .failure(let error):
                // API 호출이 실패했을 때 에러 처리 로직을 작성합니다.
                print(error)
            }
        }
    }
    
    //최종적으로 계약 수정
    private func patchCustomerBrandContract() {
        
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(loginData.token)",
            "Accept": "application/json"
        ]
        
        let parameters: [String: Any] = [
            "contract_name": contractNameFor_DetailContractBrand,
              "one_line_explan": contractOneLineExplanFor_DetailContractBrand,
              "contract_start_dt": "2023-01-01",
              "contract_end_dt": "2023-12-31",
              "contract_price": contractPriceFor_DetailContractBrand,
              "contract_status": contractStatusFor_DetailContractBrand,
              "contract_image": contractImageStringFor_DetailContractBrand,
              "brand_image": contractImageStringFor_DetailContractBrand //필요없는 값인듯?
        ]
        
        
        AF.request("\(defaultUrl)/api/brand-contract/\(selectedCustomerBrandContractIdx)",
                   method: .patch,
                   parameters: parameters,
                   encoding: JSONEncoding.default,
                   headers: headers)
        .responseJSON { response in
            switch response.result {
                
            case .success(let value):
                
                print("api 연결 성공: \(value)")
                
                guard let json = value as? [String: Any],
                      let result = json["result"] as? Int else {
                    print("응답 형식이 올바르지 않습니다: \(value)")
                    return
                }
                
                if result == 0 {
                    let errorMessage = (json["err"] as? [String: Any])?["errorMessage"] as? String
                    print("고객 브랜드 수정 실패: \(errorMessage ?? "알 수 없는 오류")")
                } else {
                    print("고객 브랜드 수정 성공: \(value)")
                    
                    // 성공한 경우, value를 사용하여 필요한 작업을 수행
                    
                }
                
                
                
            case .failure(let error):
                // 실패한 경우, error를 사용하여 필요한 작업을 수행합니다.
                print("고객 브랜드 수정 실패: \(error)")
                
            }
        }
    }
    
}

//#Preview {
//    ContractDetailForDetailCustomerBrandView()
//}
