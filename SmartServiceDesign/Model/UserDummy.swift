//
//  UserDummy.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/26/23.
//

import Foundation
import SwiftUI
import UIKit

struct UserDummy: Identifiable {
    var id = UUID()
    var position: String
    var department: String
    var name: String
    var type: String
    var email: String
    var image: UIImage?
    var phone: String
}

let userDummyData = [
    UserDummy(position: "팀장", department: "마케팅", name: "김영희", type: "employee", email: "younghee.kim@example.com", image: UIImage(named: "userImage1"), phone: "010-1234-5678"),
    UserDummy(position: "사원", department: "마케팅", name: "이철수", type: "employee", email: "cheolsu.lee@example.com", image: UIImage(named: "userImage2"), phone: "010-2345-6789"),
    UserDummy(position: "대표", department: "기획", name: "박지성", type: "creater", email: "jisung.park@example.com", image: UIImage(named: "userImage3"), phone: "010-3456-7890"),
    UserDummy(position: "팀장", department: "기획", name: "김나리", type: "employee", email: "nari.kim@example.com", image: UIImage(named: "userImage4"), phone: "010-4567-8901"),
    UserDummy(position: "사원", department: "디자인", name: "이민호", type: "creater", email: "minho.lee@example.com", image: UIImage(named: "userImage5"), phone: "010-5678-9012"),
    UserDummy(position: "팀장", department: "디자인", name: "박선영", type: "employee", email: "sunyoung.park@example.com", image: UIImage(named: "userImage6"), phone: "010-6789-0123"),
    UserDummy(position: "대표", department: "마케팅", name: "김지훈", type: "creater", email: "jihun.kim@example.com", image: UIImage(named: "userImage7"), phone: "010-7890-1234"),
    UserDummy(position: "사원", department: "기획", name: "이은지", type: "employee", email: "eunji.lee@example.com", image: UIImage(named: "userImage8"), phone: "010-8901-2345"),
    UserDummy(position: "팀장", department: "디자인", name: "박민수", type: "creater", email: "minsu.park@example.com", image: UIImage(named: "userImage9"), phone: "010-9012-3456")
]


