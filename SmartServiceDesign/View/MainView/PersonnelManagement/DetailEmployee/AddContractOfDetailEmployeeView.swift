//
//  AddContractOfDetailEmployeeView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/27/23.
//

import SwiftUI

enum ContractStatusOfDetailEmployee: CaseIterable {
    case new
    case update
    case change
    case complete
    case resignation
}

struct AddContractOfDetailEmployeeView: View {
    
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @State private var showToast: Bool = false
    @State private var toastText: String = ""
    
    @State private var contractNameFor_DetailEmployee: String = ""
    @State private var contractOneLineExplanFor_DetailEmployee: String = ""
    @State private var contractStartDateFor_DetailEmployee: String = "2023.12.01"
    @State private var contractEndDateFor_DetailEmployee: String = "2023.12.31"
    @State private var contractPriceFor_DetailEmployee: String = ""
    @State private var contractStatusFor_DetailEmployee: String = "요청"
    
    @State private var showingImagePickerContractImageFor_DetailEmployee = false
    @State private var selectedContractImageFor_DetailEmployee: UIImage? = nil
    
    @State private var contractStatus: ContractStatusOfDetailEmployee = ContractStatusOfDetailEmployee.new
    
    var selectedDetailEmployeeName: String = ""
    
    @State private var navigate: Bool = false
    
    var body: some View {
        
        
        ZStack{
            
            
            ScrollView{
                
                VStack(spacing: 0){
                    
                    
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("계약명")
                                .font(.system(size: 14))
                            
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $contractNameFor_DetailEmployee, prompt: Text("계약명을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 계약명
                    .padding(.top, screenHeight / 25.46875) //32
                    
                    
                    
                    VStack{ // 한줄 설명
                        
                        HStack(spacing: 0){
                            Text("한줄 설명")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $contractOneLineExplanFor_DetailEmployee, prompt: Text("한줄 설명을 입력해주세요")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.phonePad)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 한줄 설명
                    .padding(.top, screenHeight / 54.133) //15
                    
                    VStack{ // 계약기간
                        
                        HStack(spacing: 0){
                            Text("계약기간")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            Text("\(contractStartDateFor_DetailEmployee) ~ \(contractEndDateFor_DetailEmployee)")
                                .foregroundColor(Color("hintTextColor"))
                            .keyboardType(.emailAddress)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            
                            Spacer()
                            
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 계약기간
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    VStack{//계약 금액
                        
                        HStack(spacing: 0){
                            Text("계약 금액")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $contractPriceFor_DetailEmployee, prompt: Text("계약금액을 입력해주세요")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.phonePad)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 계약 금액
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    VStack{ //진행상태
                        
                        HStack(spacing: 0){
                            Text("진행상태")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        
                        HStack(spacing: screenWidth / 125 ){ //SelectedBtn
                            
                            Spacer()
                            
                            Button(action: {
                                
                                contractStatus = .new
                                contractStatusFor_DetailEmployee = "신규"
                                print("\(contractStatusFor_DetailEmployee)")
                                
                            }, label: {
                                Text("신규")
                                    .foregroundColor(contractStatus == .new ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(contractStatus == .new ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            Button(action: {
                                
                                contractStatus = .update
                                contractStatusFor_DetailEmployee = "갱신"
                                print("\(contractStatusFor_DetailEmployee)")
                                
                            }, label: {
                                Text("갱신")
                                    .foregroundColor(contractStatus == .update ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(contractStatus == .update ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            Button(action: {
                                
                                contractStatus = .change
                                contractStatusFor_DetailEmployee = "변경"
                                print("\(contractStatusFor_DetailEmployee)")
                                
                            }, label: {
                                Text("변경")
                                    .foregroundColor(contractStatus == .change ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(contractStatus == .change ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            Button(action: {
                                
                                contractStatus = .complete
                                contractStatusFor_DetailEmployee = "완료"
                                print("\(contractStatusFor_DetailEmployee)")
                                
                            }, label: {
                                Text("완료")
                                    .foregroundColor(contractStatus == .complete ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(contractStatus == .complete ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            Button(action: {
                                
                                contractStatus = .resignation
                                contractStatusFor_DetailEmployee = "퇴사"
                                
                                print("\(contractStatusFor_DetailEmployee)")
                                
                            }, label: {
                                Text("퇴사")
                                    .foregroundColor(contractStatus == .resignation ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(contractStatus == .resignation ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            
                            
                            Spacer()
                            
                            
                        }//HStack (SelectedBtn 을 위한 HStack)
                        
                    } //VStack 진행상태
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    
                    VStack(alignment: .leading){//계약서 등록
                        
                        HStack(spacing: 0){
                            Text("계약서 등록")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        
                        Button(action: {
                            showingImagePickerContractImageFor_DetailEmployee = true
                        }, label: {
                            
                            ZStack {
                                if let image = selectedContractImageFor_DetailEmployee {
                                    Image(uiImage: image)
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                } else {
                                    Rectangle()
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                        .foregroundColor(.colorE5E5E5)
                                    
                                    Circle()
                                        .frame(width: screenWidth/10.416)
                                        .foregroundColor(.colorEFEFF4)
                                        .overlay{
                                            Image(systemName: "plus")
                                                .imageScale(.medium)
                                        }
                                }
                            }
                            
                        })
                        .padding(.leading, screenWidth / 18.75)
                        
                    }//VStack 계약서 등록 이미지
                    .padding(.top, screenHeight / 54.133) //20    54.133 = 15
                    .sheet(isPresented: $showingImagePickerContractImageFor_DetailEmployee, onDismiss: loadContractImageForDetailEmployee) {
                        ImagePicker(selectedImage: $selectedContractImageFor_DetailEmployee)
                    }
                    
                    HStack{
                        
                        Button(action: {
                            
                            
                            if(contractNameFor_DetailEmployee == "" || contractOneLineExplanFor_DetailEmployee == "" || contractStartDateFor_DetailEmployee == "" || contractEndDateFor_DetailEmployee == "" || contractPriceFor_DetailEmployee == "" || contractStatusFor_DetailEmployee == ""){
                                
                                toastText = "칸을 비울 수 없습니다."
                                showToast = true
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                    self.showToast = false
                                    toastText = ""
                                }
                                
                                self.presentationMode.wrappedValue.dismiss()
                            }else{
                                self.presentationMode.wrappedValue.dismiss()
                            }
                            
                            
                        }, label: {
                            
                                Text("계약 추가")
                                    .foregroundColor(.white)
                                    .font(.system(size: 16))
                                    .bold()
                                    .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                            
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/67.9166) // 37
                        
                        
                        
                    }
                    
                    
                    
                    
                    Spacer()
                    
                    
                    
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: Button(action: {
                    
                    self.presentationMode.wrappedValue.dismiss()
                    
                }) {
                    
                    HStack(spacing: 0){
                        
                        Image(systemName: "chevron.backward")
                        
                    }
                    
                    
                    
                })
                .navigationBarTitle("\(selectedDetailEmployeeName)", displayMode: .inline)
                
                
            }//ScrollView
            .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                UIApplication.shared.endEditing()
                
            }
            .toast(isShowing: $showToast, text: Text(toastText))
            
            
        }//ZStack
        
    }//body
    
    func loadContractImageForDetailEmployee() {
        guard let selectedContractImageFor_DetailEmployee = selectedContractImageFor_DetailEmployee else { return }
        // You can do something with the selected brand image here
    }
    
}

#Preview {
    AddContractOfDetailEmployeeView()
}
