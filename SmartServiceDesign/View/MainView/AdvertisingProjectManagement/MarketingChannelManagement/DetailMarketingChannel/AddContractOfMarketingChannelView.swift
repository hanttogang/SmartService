//
//  AddContractOfMarketingChannelView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/26/23.
//

import SwiftUI



enum ContractStatusOfDetailMarketingChannel: CaseIterable {
    case request
    case ongoing
    case feedback
    case complete
    case hold
}

struct AddContractOfMarketingChannelView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @State private var showToast: Bool = false
    @State private var toastText: String = ""
    
    @State private var contractNameFor_MarketingChannel: String = ""
    @State private var contractOneLineExplanFor_MarketingChannel: String = ""
    @State private var contractStartDateFor_MarketingChannel: String = "2023.12.01"
    @State private var contractEndDateFor_MarketingChannel: String = "2023.12.31"
    @State private var contractPriceFor_MarketingChannel: String = ""
    @State private var contractStatusFor_MarketingChannel: String = "요청"
    
    @State private var showingImagePickerContractImageFor_MarketingChannel = false
    @State private var selectedContractImageFor_MarketingChannel: UIImage? = nil
    
    @State private var contractStatus: ContractStatusOfDetailMarketingChannel = ContractStatusOfDetailMarketingChannel.request
    
    var selectedDetailMarketingChannelName: String = ""
    
    @State private var navigate: Bool = false
    
    var body: some View {
        
        
        ZStack{
            
            ScrollView{
                
                VStack(spacing: 0){
                    
                    
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("계약명")
                                .font(.system(size: 14))
                            
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $contractNameFor_MarketingChannel, prompt: Text("계약명을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 계약명
                    .padding(.top, screenHeight / 25.46875) //32
                    
                    
                    
                    VStack{ // 한줄 설명
                        
                        HStack(spacing: 0){
                            Text("한줄 설명")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $contractOneLineExplanFor_MarketingChannel, prompt: Text("한줄 설명을 입력해주세요")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.phonePad)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 한줄 설명
                    .padding(.top, screenHeight / 54.133) //15
                    
                    VStack{ // 계약기간
                        
                        HStack(spacing: 0){
                            Text("계약기간")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            Text("\(contractStartDateFor_MarketingChannel) ~ \(contractEndDateFor_MarketingChannel)")
                                .foregroundColor(Color("hintTextColor"))
                            .keyboardType(.emailAddress)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            
                            Spacer()
                            
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 계약기간
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    VStack{//계약 금액
                        
                        HStack(spacing: 0){
                            Text("계약 금액")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $contractPriceFor_MarketingChannel, prompt: Text("계약금액을 입력해주세요")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.phonePad)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 계약 금액
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    VStack{ //진행상태
                        
                        HStack(spacing: 0){
                            Text("진행상태")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        
                        HStack(spacing: screenWidth / 125 ){ //SelectedBtn
                            
                            Spacer()
                            
                            Button(action: {
                                
                                contractStatus = .request
                                contractStatusFor_MarketingChannel = "요청"
                                print("\(contractStatusFor_MarketingChannel)")
                                
                            }, label: {
                                Text("요청")
                                    .foregroundColor(contractStatus == .request ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(contractStatus == .request ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            Button(action: {
                                
                                contractStatus = .ongoing
                                contractStatusFor_MarketingChannel = "진행"
                                print("\(contractStatusFor_MarketingChannel)")
                                
                            }, label: {
                                Text("진행")
                                    .foregroundColor(contractStatus == .ongoing ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(contractStatus == .ongoing ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            Button(action: {
                                
                                contractStatus = .feedback
                                contractStatusFor_MarketingChannel = "피드백"
                                print("\(contractStatusFor_MarketingChannel)")
                                
                            }, label: {
                                Text("피드백")
                                    .foregroundColor(contractStatus == .feedback ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(contractStatus == .feedback ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            Button(action: {
                                
                                contractStatus = .complete
                                contractStatusFor_MarketingChannel = "완료"
                                print("\(contractStatusFor_MarketingChannel)")
                                
                            }, label: {
                                Text("완료")
                                    .foregroundColor(contractStatus == .complete ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(contractStatus == .complete ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            Button(action: {
                                
                                contractStatus = .hold
                                contractStatusFor_MarketingChannel = "보류"
                                
                                print("\(contractStatusFor_MarketingChannel)")
                                
                            }, label: {
                                Text("보류")
                                    .foregroundColor(contractStatus == .hold ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(contractStatus == .hold ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            
                            
                            Spacer()
                            
                            
                        }//HStack (SelectedBtn 을 위한 HStack)
                        
                    } //VStack 진행상태
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    
                    VStack(alignment: .leading){//계약서 등록
                        
                        HStack(spacing: 0){
                            Text("계약서 등록")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        
                        Button(action: {
                            showingImagePickerContractImageFor_MarketingChannel = true
                        }, label: {
                            
                            ZStack {
                                if let image = selectedContractImageFor_MarketingChannel {
                                    Image(uiImage: image)
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                } else {
                                    Rectangle()
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                        .foregroundColor(.colorE5E5E5)
                                    
                                    Circle()
                                        .frame(width: screenWidth/10.416)
                                        .foregroundColor(.colorEFEFF4)
                                        .overlay{
                                            Image(systemName: "plus")
                                                .imageScale(.medium)
                                        }
                                }
                            }
                            
                        })
                        .padding(.leading, screenWidth / 18.75)
                        
                    }//VStack 계약서 등록 이미지
                    .padding(.top, screenHeight / 54.133) //20    54.133 = 15
                    .sheet(isPresented: $showingImagePickerContractImageFor_MarketingChannel, onDismiss: loadContractImageForMarketingChannel) {
                        ImagePicker(selectedImage: $selectedContractImageFor_MarketingChannel)
                    }
                    
                    HStack{
                        
                        Button(action: {
                            self.presentationMode.wrappedValue.dismiss()
                        }, label: {
                            
                                Text("계약 추가")
                                    .foregroundColor(.white)
                                    .font(.system(size: 16))
                                    .bold()
                                    .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                            
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/67.9166) // 37
                        
                        
                        
                    }
                    
                    
                    
                    
                    Spacer()
                    
                    
                    
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: Button(action: {
                    
                    self.presentationMode.wrappedValue.dismiss()
                    
                    
                }) {
                    
                    HStack(spacing: 0){
                        
                        Image(systemName: "chevron.backward")
                        
                    }
                    
                    
                    
                })
                .navigationBarTitle("\(selectedDetailMarketingChannelName) 계약 추가", displayMode: .inline)
                .toast(isShowing: $showToast, text: Text(toastText))
                
            }//ScrollView
            .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                UIApplication.shared.endEditing()
                
            }
            
            
        }//ZStack
        
    }//body
    
    func loadContractImageForMarketingChannel() {
        guard let selectedContractImageFor_MarketingChannel = selectedContractImageFor_MarketingChannel else { return }
        // You can do something with the selected brand image here
    }
    
}

#Preview {
    AddContractOfMarketingChannelView()
}
