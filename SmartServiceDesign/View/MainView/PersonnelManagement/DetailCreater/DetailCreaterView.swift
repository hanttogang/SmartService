//
//  DetailCreaterView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/27/23.
//

import SwiftUI

struct DetailCreaterView: View {
    
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @State private var navigate: Bool = false
    @State private var navigateContractDetailForDetailCustomerBrandView: Bool = false
    
    @State private var showToast: Bool = false
    @State private var toastText: String = ""
    
    @State private var modificationMode: Bool = false
    
    @State private var showingImagePickerFor_DetailCreaterInfo = false
    @State private var selectedDetailCreaterInfo: UIImage? = nil
    
    var getEmployeeNameFor_DetailCreaterInfo: String = "박지성"
    @State private var createrNameFor_DetailCreaterInfo: String = ""

    
    @State private var classificationFor_DetailCreaterInfo: String = "소속직원"
    
    @State private var oneLineExplanFor_DetailCreaterInfo: String = "한줄 설명 내용"
    @State private var createrPhoneNumberFor_DetailCreaterInfo: String = "031-123-1234"
    @State private var createrEmailFor_DetailCreaterInfo: String = "bca321@mail.com"
    
    @State private var workAreaFor_DetailDetailCreaterInfo: String = "광고마케팅"
    @State private var selecteWorkAreaFor_DetailCreaterInfo: Bool = false
    @State private var workAreaModificationModeFor_DetailCreaterInfo: Bool = false
    
    @State private var contractImageForDetailCreaterInfoContractList: UIImage? = nil
    
    
    @State private var showAllContractList: Bool = false
    @State private var showShowAllButton: Bool = false
    
    
    
    
    var body: some View {
        
        ZStack {
            ScrollView(.vertical){
                VStack(spacing: 0){
                    
                    
                    HStack{
                        
                        
                        VStack(alignment: .leading){// 브랜드 이미지
                            ZStack{
                                if modificationMode{
                                    
                                    Button(action: {
                                        showingImagePickerFor_DetailCreaterInfo = true
                                    }, label: {
                                        
                                        ZStack {
                                            if let image = selectedDetailCreaterInfo {
                                                Image(uiImage: image)
                                                    .resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                    .cornerRadius(12.0)
                                                
                                            } else {
                                                Rectangle()
                                                    .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                    .cornerRadius(12.0)
                                                    .foregroundColor(.colorE5E5E5)
                                                
                                            }
                                        }
                                    })
                                    
                                }else{
                                    
                                    ZStack {
                                        if let image = selectedDetailCreaterInfo {
                                            Image(uiImage: image)
                                                .resizable()
                                                .aspectRatio(contentMode: .fill)
                                                .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                .cornerRadius(12.0)
                                            
                                        } else {
                                            Rectangle()
                                                .frame(width: screenWidth/11.363, height: screenHeight/24.696)
                                                .cornerRadius(12.0)
                                                .foregroundColor(.colorE5E5E5)
                                            
                                        }
                                    }
                                    
                                }
                            } //ZStack BrandImage
                            .sheet(isPresented: $showingImagePickerFor_DetailCreaterInfo, onDismiss: loadDetailCreaterInfoImage) {
                                ImagePicker(selectedImage: $selectedDetailCreaterInfo)
                            }
                            
                        }//VStack 크리에이터 이미지
                        
                        HStack(){
                            
                            HStack{
                                
                                //크리에이터이름
                                if modificationMode{
                                    
                                    TextField(".", text: $createrNameFor_DetailCreaterInfo, prompt: Text("\(createrNameFor_DetailCreaterInfo)")
                                        .foregroundColor(Color("hintTextColor")))
                                    .keyboardType(.default)
                                    .font(.title2)
                                    .frame(width: screenWidth/2)//
    //                                .padding(.leading, -screenWidth/150)
                                    
                                    
                                }else {
                                    
                                    Text("\(createrNameFor_DetailCreaterInfo)")
                                        .font(.title2)
                                    
//                                        .frame(width: screenWidth/6.3)
                                }
                            }
                            
                        }
                        
                        
                        
                        
                        
                        
                        
                        Spacer()
                        
                        if(modificationMode){
                            Button(action: {
                                
                                
                                if (selectedDetailCreaterInfo == nil || createrNameFor_DetailCreaterInfo == "" || classificationFor_DetailCreaterInfo == "" || createrPhoneNumberFor_DetailCreaterInfo == "" || createrEmailFor_DetailCreaterInfo == ""){
                                    
                                    toastText = "칸을 비울 수 없습니다."
                                    showToast = true
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                        self.showToast = false
                                        toastText = ""
                                    }
                                    
                                } else {
                                    
                                    modificationMode = false
                                }
                                
                                
                                
                            }, label: {
                                Text("저장")
                                
                            })
                        } else {
                            Button(action: {
                                
                                modificationMode = true
                                
                                
                            }, label: {
                                Text("수정")
                                
                            })
                        }
                        
                        
                    }
                    .padding(.horizontal)
                    .padding(.vertical, screenHeight / 81.5)
                    .padding(.top, screenHeight / 81.5)
                    
                    Divider()
                        .padding(.leading)
                   

                    HStack{
                        Text("분류")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        Text("크리에이터")
                            .foregroundColor(.white)
                            .bold()
                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                            .background(Color("mainColor"))
                            .cornerRadius(16.0)
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    
                    HStack{
                        Text("한줄 설명")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            RightAlignedTextField(text: $oneLineExplanFor_DetailCreaterInfo)
                                .keyboardType(.default)
                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(oneLineExplanFor_DetailCreaterInfo)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("전화번호")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            RightAlignedTextField(text: $createrPhoneNumberFor_DetailCreaterInfo)
                                .keyboardType(.numberPad)
                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(createrPhoneNumberFor_DetailCreaterInfo)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{
                        Text("이메일")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationMode{
                            RightAlignedTextField(text: $createrEmailFor_DetailCreaterInfo)
                                .keyboardType(.emailAddress)
                                .autocapitalization(.none)
                            
                        }else {
                            
                            Text("\(createrEmailFor_DetailCreaterInfo)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("업무영역")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        
                        
                        if modificationMode{
                            Button(action: {
//                                selecteWorkAreaForDetailBrand = true
                            }, label: {
                                
                                //HStack 으로 버튼 세 개 만든 후 각각 터치 이벤트에 selecteWorkAreaForDetailBrand = false 처리하기
                                
                                
                                
                                
                                
                                if workAreaModificationModeFor_DetailCreaterInfo{
                                    
                                    Button(action: {
                                        workAreaModificationModeFor_DetailCreaterInfo = false
                                        
                                        workAreaFor_DetailDetailCreaterInfo = "광고마케팅"
                                        
                                    }, label: {
                                        Text("광고마케팅")
                                            .foregroundColor(.white)
                                            .bold()
                                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                            .background(Color("mainColor"))
                                            .cornerRadius(16.0)
                                    })
                                    
                                    Button(action: {
                                        workAreaModificationModeFor_DetailCreaterInfo = false
                                        
                                        workAreaFor_DetailDetailCreaterInfo = "MCN"
                                        
                                    }, label: {
                                        Text("MCN")
                                            .foregroundColor(.white)
                                            .bold()
                                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                            .background(Color("mainColor"))
                                            .cornerRadius(16.0)
                                    })
                                    
                                    Button(action: {
                                        workAreaModificationModeFor_DetailCreaterInfo = false
                                        
                                        workAreaFor_DetailDetailCreaterInfo = "기타"
                                        
                                    }, label: {
                                        Text("기타")
                                            .foregroundColor(.white)
                                            .bold()
                                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                            .background(Color("mainColor"))
                                            .cornerRadius(16.0)
                                    })
                                    
                                    
                                }else{
                                    
                                    Button(action: {
                                        workAreaModificationModeFor_DetailCreaterInfo = true
                                        
                                    }, label: {
                                        Text("\(workAreaFor_DetailDetailCreaterInfo)")
                                            .foregroundColor(.white)
                                            .bold()
                                            .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                            .background(Color("mainColor"))
                                            .cornerRadius(16.0)
                                    })
                                }
                                
                                
                                
                                
                            })
                        } else{
                            
                            Text("\(workAreaFor_DetailDetailCreaterInfo)")
                                .foregroundColor(.white)
                                .bold()
                                .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                .background(Color("mainColor"))
                                .cornerRadius(16.0)
                        }
                        
                        
                    }
                    .padding(.horizontal)
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    
                    VStack(spacing: 0) {
                        HStack{
                            
                            Text("계약 리스트")
                                .font(.title2)
                                .bold()
                                .padding()
                            //                                .padding(.top, -screenHeight/13.5)
                            Spacer()
                        }
                        .padding(0)
                        
                        Button(action: {
                            print("계약 추가 버튼 클릭됨")
                            
                            self.navigate = true
                            
                        }, label: {
                            
                            NavigationLink(destination: AddContractOfDetailCreaterView(selectedDetailCreaterName: createrNameFor_DetailCreaterInfo), isActive: $navigate) {
                                EmptyView()
                            }
                            
                            Text("+  계약 추가")
                                .foregroundColor(.white)
                                .font(.system(size: 16))
                                .bold()
                                .frame(width: screenWidth/1.25, height: screenHeight/20.375)
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        
                        
                        VStack{//계약 리스트 터치시
                            
                            ForEach(contractDummyData.sorted(by: { $0.endDate > $1.endDate }).prefix(showAllContractList ? contractDummyData.count : 3)) { contract in
                                
                                NavigationLink(destination:
                                        ContractDetailOfCreaterView(selectedDetailCreaterName: createrNameFor_DetailCreaterInfo, selectedDetailContractNameFor_DetailCreater: contract.repName)) {
                                    
                                    VStack{
                                        HStack{ //리스트항목의 모델
                                            
                                            if let image = contract.image {
                                                Image(uiImage: image)
                                                    .resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                    .cornerRadius(12.0)
                                            } else {
                                                Rectangle()
                                                    .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                    .cornerRadius(12.0)
                                                    .foregroundColor(.colorE5E5E5)
                                            }
                                            
                                            VStack(alignment: .leading){
                                                Text(contract.repName)
                                                    .foregroundColor(.black)
                                                
                                                Text("\(contract.formattedStartDate) ~ \(contract.formattedEndDate)")
                                                    .foregroundColor(Color("color00000040"))
                                                    .font(.caption)
                                            }
                                            
                                            Spacer()
                                            
                                            Text(contract.status)
                                                .foregroundColor(.white)
                                                .bold()
                                                .frame(width: screenWidth/4.6875, height: screenHeight/29.107)
                                                .background(Color("mainColor"))
                                                .cornerRadius(16.0)
                                            
                                            
                                        }
                                        
                                        Divider()
                                            .padding(.leading, screenWidth/8)
                                    }
                                    
                                }//NavigationLink
                            }
                            
                            
                            
                            
                        } //VStack 계약 리스트
                        .listStyle(PlainListStyle())
                        .padding(.horizontal)
                        .padding(.top, screenHeight/50.9375)
                        
                        
                        if showShowAllButton{
                            
                            Button(action: {
                                
                                showShowAllButton = false
                                showAllContractList = true
                                
                            }, label: {
                                
                                Text("더 보기")
                                    .foregroundColor(.black)
                                    .font(.system(size: 16))
                                    .bold()
                                    .frame(width: screenWidth/1.25, height: screenHeight/20.375)
                            })
                            .background(.white)
                            .cornerRadius(4)
                            .padding()
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color(.black), lineWidth: 1)
                                .padding()
                            )
                            
                        }
                        
                        Spacer()
                        
                        
                        
                        
                    }//VStack
                    .frame(width: screenWidth/1.0932)
                    .background(Color.white)
                    .cornerRadius(20)
                    .shadow(color: Color("color00000040"), radius: 20, x: 0, y: 30)
                    .padding(.top, screenHeight/31.346)
                    
                    //                    .sheet(isPresented: $showingBankRecordCopyImagePickerForDetailBrand, onDismiss: loadContractImageForDetailBrandContractList) {
                    //                        ImagePicker(selectedImage: $selectedBankRecordCopyImageForDetailBrand)
                    //                    }
                    
                    
                    
                    
                    
                } //VStack
                
                
            }//ScrollView
            .toast(isShowing: $showToast, text: Text(toastText))
            
            
            
            
            
        }//ZStack
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: {
            
            self.presentationMode.wrappedValue.dismiss()
            
            
        }) {
            
            HStack(spacing: 0){
                
                Image(systemName: "chevron.backward")
                
            }
            
            
            
        })
        .navigationBarTitle("\(createrNameFor_DetailCreaterInfo)", displayMode: .inline)
        .onAppear {
            if contractDummyData.count > 3 {
                showShowAllButton = true
            }
            
            createrNameFor_DetailCreaterInfo = getEmployeeNameFor_DetailCreaterInfo
        }
        
        
        
        
    }//body
    
    private func loadDetailCreaterInfoImage() {
        guard let selectedDetailCreaterInfo = selectedDetailCreaterInfo else { return }
        // You can do something with the selected brand image here
    }
    
}

#Preview {
    DetailCreaterView()
}
