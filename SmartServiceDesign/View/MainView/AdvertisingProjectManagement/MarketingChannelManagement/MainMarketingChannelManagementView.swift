//
//  MainMarketingChannelManagementView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/26/23.
//

import SwiftUI

struct MainMarketingChannelManagementView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    init() {
        let appearance = UINavigationBarAppearance()
        //                appearance.configureWithOpaqueBackground()
        //                appearance.backgroundColor = UIColor(.white) // 원하는 색상으로 변경하세요.
        appearance.shadowColor = nil // 이 부분을 추가하면 네비게이션 바 하단 선이 사라집니다.
        UINavigationBar.appearance().standardAppearance = appearance
        UINavigationBar.appearance().compactAppearance = appearance
        UINavigationBar.appearance().scrollEdgeAppearance = appearance
        
    }
    
    
    @State var isShowingMenu: Bool = false
    
    @State private var searchMarketingChannelText: String = ""
    @State private var editMarketingChannelText: Bool = false
    
    @State var marketingChannelName: String = ""
    
    var body: some View {
        
        ZStack{
            
            NavigationView{
                
                
                VStack{
                    
                    HStack(spacing: 0){
                        
                        Button(action: {
                            withAnimation {
                                self.isShowingMenu = true
                            }
                        }) {
                            
                            HStack(spacing: 0){
                                Image("img_menu")
                            }
                            .padding(.leading)
                            
                        }
                        
                        Spacer()
                    }
                    
                    
                    HStack{
                        
                        Text("마케팅 채널 관리")
                            .font(.title)
                            .bold()
                        
                        
                        Spacer()
                        
                    }
                    .padding(.leading, 16)
                    
                    
                    
                    HStack{
                        
                        HStack{
                            
                        }
                        .frame(width: screenWidth, height: screenHeight/14.5)
                        .overlay{
                            //검색창을 받을수있는 택스트필드
                            TextField("Search" , text : self.$searchMarketingChannelText)
                                .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                                .foregroundColor(Color("hintTextColor"))
                            //hint와 태두리에 간격을 띄우기위해 15정도의 간격을주고
                                .padding(10)
                            //양옆은 추가로 15를 더줌
                                .padding(.leading, screenWidth/12.5) //30
                            //배경색상은 자유롭게선택
                                .background(Color(.white))
                            //검색창이 너무각지면 딱딱해보이기때문에 모서리를 둥글게
                            //숫자는 취향것
                                .cornerRadius(15)
                            //내가만든 검색창 상단에
                            //돋보기를 넣어주기위해
                            //오버레이를 선언
                                .overlay(
                                    //HStack을 선언하여
                                    //가로로 view를 쌓을수있도록 하나 만들고
                                    HStack{
                                        //맨오른쪽으로 밀기위해 Spacer()로 밀어준다.
                                        
                                        //xcode에서 지원해주는 이미지
                                        //                                            if !self.editText {
                                        //magnifyingglass 를 사용
                                        //색상은 자유롭게 변경가능
                                        Image(systemName: "magnifyingglass")
                                            .foregroundColor(Color("hintTextColor"))
                                            .padding()
                                        //                                            }
                                        
                                        
                                        
                                        Spacer()
                                        
                                        if self.editMarketingChannelText{
                                            //x버튼이미지를 클릭하게되면 입력되어있던값들을 취소하고
                                            //키입력 이벤트를 종료해야한다.
                                            Button(action : {
                                                self.editMarketingChannelText = false
                                                self.searchMarketingChannelText = ""
                                                //키보드에서 입력을 끝내게하는 코드
                                                UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                                            }){
                                                Image(systemName: "multiply.circle.fill")
                                                    .foregroundColor(Color("hintTextColor"))
                                                    .padding()
                                            }
                                        }
                                        
                                    }
                                ).onTapGesture {
                                    self.editMarketingChannelText = true
                                }
                                .padding(.horizontal, 21.945) //37
                            
                            
                            
                        }//overlay
                        
                        
                        
                        
                        
                    }//HStack
                    .background(Color("colorF8F8F892"))
                    
                    
                    HStack{
                        
                        Button(action: {
                            
                        }, label: {
                            NavigationLink {
                                
                                RegistrationMarketingChannelView()
                                //                                    AddNewProject(isSelectedCustomerBrand: "", isSearchingCustomerBrand: false)
                            } label: {
                                Text("+  마케팅 채널 등록")
                                    .foregroundColor(.white)
                                    .bold()
                                    .font(.system(size: 16))
                                    .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                            }
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/50.75)
                        
                        
                        
                    }
                    
                    
                    ScrollView {
                        VStack(spacing: 0) {
                            ForEach(projectDummyData.filter { // filter 메서드를 사용하여 결과를 필터링합니다.
                                searchMarketingChannelText.isEmpty || $0.companyName.contains(searchMarketingChannelText) || $0.projectName.contains(searchMarketingChannelText)
                                // 회사 이름 또는 프로젝트 이름을 기준으로 검색
                            })  { project in
                                
                                NavigationLink(destination: DetailMarketingChannelView(selectedDetailMarketingChannelName: project.companyName)){
                                    
                                    
                                    VStack {
                                        HStack {
                                            Image(systemName: project.icon)
                                                .resizable()
                                                .aspectRatio(contentMode: .fit)
                                                .frame(width: screenWidth/6.25)
                                            
                                            VStack(alignment: .leading){
                                                Text(project.companyName)
                                                    .font(.title2)
                                                    .bold()
                                                    .foregroundColor(.black)
                                                
                                                
                                                
                                                Text(project.projectName)
                                                    .foregroundColor(.black)
                                                
                                                Text("등록일: \(project.participantCount)")
                                                    .foregroundColor(.gray)
                                                    .font(.caption)
                                            }
                                            
                                            Spacer()
                                            
                                            Image(systemName: "chevron.right")
                                                .padding()
                                                .foregroundColor(.gray)
                                        }
                                        
                                        .padding(.top)
                                        
                                        Divider()
                                    }//VStack
                                    .background(Color.white)
                                    .padding(.leading)
                                }//NavigationLink
                                
                            }
                        }
                    }//ScrollView
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    Spacer()
                    
                    
                    
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                //                    .navigationBarItems(leading: Button(action: {
                //
                //                        withAnimation {
                //                            self.isShowingMenu = true
                //                        }
                //
                //                    }) {
                //
                //                        HStack(spacing: 0){
                //
                //                            Image("img_menu")
                //
                //                        }
                //
                //
                //
                //
                //                    })
                
                
                
                
                
                
                
                
                
                
            }//NavigationView
            //    .toast(isShowing: $showToast, text: Text(toastText))
            .navigationBarHidden(true)
            .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                UIApplication.shared.endEditing()
                
            }
            
            // isShowingMenu 상태에 따라 메뉴 여닫기 zIndex 으로 조정
            if isShowingMenu {
                
                MenuView()
                    .transition(.move(edge: .leading))
                    .zIndex(2)
                
                // 메뉴 외부를 클릭하면 메뉴를 닫습니다.
                Button(action: {
                    withAnimation {
                        self.isShowingMenu = false
                    }
                }) {
                    Color.gray
                        .edgesIgnoringSafeArea(.all)
                        .opacity(0.5)
                }
                .zIndex(1)
                
            }
            
        }//ZStack
    }
}

#Preview {
    MainMarketingChannelManagementView()
}
