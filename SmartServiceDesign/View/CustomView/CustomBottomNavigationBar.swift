//
//  CustomBottomNavigationBar.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/14/23.
//

import SwiftUI


enum Tab {
    case d
    case message
    case myPage
}

struct CustomBottomNavigationBar: View {
    @Binding var selectedTab: Tab
    
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    var body: some View {
        
        
        ZStack{
            
            switch selectedTab {
            case .d:
                
                MainMarketingProjectManagementView()
            case .message:
                MainMarketingProjectManagementView()
                
            case .myPage:
//                AddNewProject()
                MainMarketingProjectManagementView()
            }
            
            VStack(spacing: 0){
                Spacer()
                
                HStack{
                    
                    
                    
                    Button {
                        selectedTab = .message
                    } label: {
                        VStack() {
                            
                            Image(systemName: "list.bullet")
                                .foregroundColor(selectedTab == .message ? .blue : .gray)
                                .imageScale(.large)
                                
                                
                            
                            Text("메세지 관리")
                                .foregroundColor(selectedTab == .message ? .blue : .gray)
                                .padding(.vertical, 4)
                            
                        }
                        .frame(width: screenWidth/2, height: screenHeight/13.588)
                        
                    }
                    .padding(.top, 4)
                    
                    //                        .overlay(
                    //                            selectedTab == .message ? Rectangle()
                    //                                .frame(height: 1)
                    //                                .foregroundColor(.black)
                    //                            : nil,
                    //                            alignment: .top
                    //                        )
                    //                .offset(x: -5)
                    
                    
                    
                    Spacer()
                    
                    
                    Button {
                        selectedTab = .myPage
                    } label: {
                        VStack() {
                            Image(systemName: "person.crop.circle")
                                .foregroundColor(selectedTab == .myPage ? .blue : .gray)
                                .imageScale(.large)
                            
                            
                            
                            Text("마이 페이지")
                                
                                .foregroundColor(selectedTab == .myPage ? .blue : .gray)
                                .padding(.vertical, 4)
                            
                            
                        }
                        .frame(width: screenWidth/2, height: screenHeight/13.588)
                        
                        //
                        //                            .overlay(
                        //                                selectedTab == .myPage ? Rectangle()
                        //                                    .frame(height: 1)
                        //                                    .foregroundColor(.black)
                        //                                : nil,
                        //                                alignment: .top
                        //                            )
                        //                    .offset(x: 5)
                    }
                    .padding(.top, 4)
                    
                    
                }//HStack
                .background(.colorF8F8F8)
                
            }//VStack
//            .padding(.vertical, 20)
            
            
        }//ZStack
//        .edgesIgnoringSafeArea(.all)
        
        
        
    }//body
}

//#Preview {
//    CustomBottomNavigationBar()
//}
