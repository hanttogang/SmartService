//
//  TopAlignedTextField.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/20/23.
//

import Foundation
import SwiftUI

struct TopAlignedTextField: UIViewRepresentable {
    @Binding var text: String

    func makeUIView(context: Context) -> UITextField {
        let textField = UITextField()
        
        textField.contentVerticalAlignment = .top
        textField.placeholder = "메모를 입력해주세요" // 힌트 텍스트 설정
        
        
        return textField
    }

    func updateUIView(_ uiView: UITextField, context: Context) {
        uiView.text = text
    }
}
