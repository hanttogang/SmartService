//
//  FindPasswordView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/13/23.
//

import SwiftUI



enum ProgressStatus {
    case request
    case ongoing
    case feedback
    case complete
    case hold
}

struct AddTaskView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    
    var titleProjectName: String
    
    @State var taskName: String = ""
    @State var taskOneLineDescription: String = ""
    
    @State private var startDate = Date()
    @State private var endDate = Date()
    @State private var isStartDatePickerShown = false
    @State private var isEndDatePickerShown = false
    
    //
    @State private var isSelectedCustomerBrand: String = ""
    
    @State private var selectedBtn = ProgressStatus.request
    
    @State private var addManagerViewBtn: Bool = false
    
    @State var searchManagerName: String = ""
    @State private var editText: Bool = false
    
    @State private var selectedManagerName: String = ""
    
    
    
    var body: some View {
        
        
        ZStack {
            
            ZStack{
                
                VStack(spacing: 0){
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("일정명")
                                .font(.system(size: 14))
                                .padding(.leading)
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField(".", text: $taskName, prompt: Text("과업명을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 이름
                    .padding(.top, screenHeight / 54.133) //15
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("한 줄 설명")
                                .font(.system(size: 14))
                                .padding(.leading)
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField(".", text: $taskOneLineDescription, prompt: Text("한 줄 설명을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 이름
                    .padding(.top, screenHeight / 54.133) //15
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("일정")
                                .font(.system(size: 14))
                                .padding(.leading)
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        HStack{
                            
                            //TODO                                일정 등록 관련 TextField 작성해야함
                            //
                            //                                TextField(".", text: $projectName, prompt: Text("프로젝트명을 입력해주세요.")
                            //                                    .foregroundColor(Color("hintTextColor")))
                            //                                .keyboardType(.default)
                            //                                .font(.system(size: 14))
                            //                                .padding(.leading, 14)
                            //                                .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 이름
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    VStack{
                        
                        HStack{
                            Text("진행상태")
                                .font(.system(size: 14))
                                .padding(.leading)
                            
                            Spacer()
                        }
                        .padding(.horizontal, screenWidth / 15.625)
                        
                        HStack(spacing: screenWidth / 125 ){ //SelectedBtn
                            
                            Spacer()
                            
                            Button(action: {
                                
                                selectedBtn = .request
                                
                            }, label: {
                                Text("요청")
                                    .foregroundColor(selectedBtn == .request ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(selectedBtn == .request ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            Button(action: {
                                
                                selectedBtn = .ongoing
                                
                            }, label: {
                                Text("진행")
                                    .foregroundColor(selectedBtn == .ongoing ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(selectedBtn == .ongoing ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            Button(action: {
                                
                                selectedBtn = .feedback
                                
                            }, label: {
                                Text("피드백")
                                    .foregroundColor(selectedBtn == .feedback ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(selectedBtn == .feedback ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            Button(action: {
                                
                                selectedBtn = .complete
                                
                            }, label: {
                                Text("완료")
                                    .foregroundColor(selectedBtn == .complete ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(selectedBtn == .complete ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            Button(action: {
                                
                                selectedBtn = .hold
                                
                            }, label: {
                                Text("보류")
                                    .foregroundColor(selectedBtn == .hold ? .white : .black)
                                
                            })
                            .frame(width: screenWidth/5.859, height: screenHeight/29.107)
                            .background(selectedBtn == .hold ? Color("mainColor") : Color("colorEFEFF4"))
                            .cornerRadius(16.0)
                            
                            
                            
                            Spacer()
                            
                            
                        }//HStack (SelectedBtn 을 위한 HStack)
                        .padding(.horizontal, screenWidth / 15.625)
                        
                        HStack{
                            Text("참여 인원 추가")
                                .padding(.leading)
                            
                            
                            Button(action: {
                                
                                print("참여 인원 추가 버튼 클릭")
                                
                                self.addManagerViewBtn = true
                                
                            }, label: {
                                
                            
                                Circle()
                                    .frame(width: screenWidth/12.5)
                                    .foregroundColor(Color("colorEFEFF4"))
                                    .overlay{
                                        Text("+")
                                            .foregroundColor(Color("mainColor"))
                                            .bold()
                                    }
                                
                            })
                            
                            
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        .padding(.top, screenHeight / 54.133) //15
                        
                        
                    }
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    
                    HStack{
                        
                        Button(action: {
                            self.presentationMode.wrappedValue.dismiss()
                        }, label: {
                            
                            Text("과업 추가")
                                .foregroundColor(.white)
                                .font(.system(size: 16))
                                .bold()
                                .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                            
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/21.945) // 37
                        
                        
                        
                    }
                    
                    
                    Spacer()
                    
                    
                    
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: Button(action: {
                    
                    self.presentationMode.wrappedValue.dismiss()
                    
                    
                }) {
                    
                    HStack(spacing: 0){
                        
                        Image(systemName: "chevron.backward")
                        
                    }
                    
                    
                    
                })
                .navigationBarTitle("\(titleProjectName)", displayMode: .inline)
            }
            
            if(addManagerViewBtn){
                ZStack{
                    VStack(spacing: 0){
                        
                        
                        HStack{
                            
                            HStack{
                                
                            }
                            .frame(width: screenWidth, height: screenHeight/14.5)
                            .overlay{
                                
                                HStack{
                                    
                                    //검색창을 받을수있는 택스트필드
                                    TextField("Search" , text : self.$searchManagerName)
                                        .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                                        .foregroundColor(Color(.black))
                                        .padding(10)
                                        .padding(.leading, screenWidth/12.5) //30
                                        .background(Color(.white))
                                        .frame(width: screenWidth/1.3)
                                        .cornerRadius(15)
                                        .overlay(
                                            
                                            //가로로 view를 쌓을수있도록 하나 만들고
                                            HStack{
                                                Image(systemName: "magnifyingglass")
                                                    .foregroundColor(Color("hintTextColor"))
                                                    .padding()
                                                
                                                Spacer()
                                                
                                                if self.editText{
                                                    Button(action : {
                                                        
                                                        self.editText = false
                                                        self.searchManagerName = ""
                                                        
                                                        UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                                                    }){
                                                        Image(systemName: "multiply.circle.fill")
                                                            .foregroundColor(Color("hintTextColor"))
                                                            .padding()
                                                    }
                                                }
                                                
                                                
                                            }//HStack
                                            
                                            
                                            
                                            
                                            
                                            
                                        )//overlay
                                        .onTapGesture {
                                            self.editText = true
                                        }
                                    
                                    Button(action: {
                                        
                                        print("취소 버튼 클릭")
                                        self.addManagerViewBtn = false
                                        
                                    }, label: {
                                        Text("취소")
                                    })
                                    .padding(.horizontal, screenWidth/46.875)
                                    //                            }
                                    
                                    
                                }
                            }//overlay
                        }//HStack 검색창
                        .background(Color("colorF8F8F892"))
                        
                        HStack{
                            Text("담당자 검색")
                                .font(.title2)
                                .padding(.leading, screenWidth/23.4375) //16
                                .padding(.top, screenWidth/20.833) //18
                                
                            
                            Spacer()
                        }
                        
                        Divider() // 가로 선
                            .padding(.top, 8)
                            .padding(.leading, screenWidth/23.4375) //16
                        
                        List(dummyData.filter{ searchManagerName.isEmpty ? true : $0.name.contains(searchManagerName)}) { company in
                            VStack(alignment: .leading){
                                HStack(spacing: 0){
                                    
                                    Text(company.name)
                                        .frame(minWidth: 0, minHeight: 0, maxHeight: .infinity)
                                        .foregroundColor(.blue)
                                    
                                    
                                    Spacer()
                                }
                                
                                
                            }
                            .swipeActions {
                                Button(action: {
                                    print("제외 버튼 클릭")
                                    // 선택 버튼 클릭 시 수행할 작업을 여기에 추가합니다.
                                }) {
                                    Text("제외")
                                }
                                .tint(.red)
                                
                                Button(action: {
                                    print("선택 버튼 클릭")
                                    // 제외 버튼 클릭 시 수행할 작업을 여기에 추가합니다.
                                }) {
                                    Text("선택")
                                }
                                .tint(.gray)
                                
                                
                            }
                            .onTapGesture {
                                print(company.name)
                                
                                selectedManagerName = "\(company.name)"
                                self.addManagerViewBtn = false
                                
                            }
                            
                            
                        }//List
                        .listStyle(PlainListStyle())
                        .background(.white)
                        
                        
                        Spacer()
                        
                    } //VStack
                    .navigationBarHidden(true)
                    .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                        UIApplication.shared.endEditing()
                        
                    }
                    
                }//ZStack
                .padding()
                .background(.white)
                
            }
        }//ZStack
        //    .toast(isShowing: $showToast, text: Text(toastText))
        .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
            UIApplication.shared.endEditing()
        }
        
        
        
        
        
        
    }
    
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        return formatter
    }()
}
//
//struct AddTask_PreviewPreviewProvider {
//
//
//    static var previews: some View {
//        AddTask(titleProjectName: "", selectedBtn: .request)
//    }
//}


#Preview {
    AddTaskView(titleProjectName: "")
}
