//
//  MainContainer.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/14/23.
//

import SwiftUI

struct MainContainer: View {
    
    @State var selectedTab: Tab = .d
    
    @State var isShowingMenu: Bool = false
    
    var rootPage: Bool = true
    
    var body: some View {
        
        ZStack{
          
           
            
            VStack{
                
//                if rootPage{
//                    HStack(spacing: 0){
//                        
//                        Button(action: {
//                            withAnimation {
//                                self.isShowingMenu = true
//                            }
//                        }) {
//                            
//                            HStack(spacing: 0){
//                                Image("img_menu")
//                            }
//                            .padding(.leading)
//                            
//                        }
//                        
//                        Spacer()
//                    }
//                }
                
                ZStack{
                    
                    MainMarketingProjectManagementView()
//
//                    switch selectedTab {
//                    case .d:
//                        MainMarketingProjectManagementView()
////                        EmptyView()
//                    case .message:
//                        MainMarketingProjectManagementView()
//                        
//                    case .myPage:
//        //                AddNewProject()
//                        MainMarketingProjectManagementView()
//                    }
//                    
//                    
//                   
//                    CustomBottomNavigationBar(selectedTab: $selectedTab)
                }
                
            }
            
            
            // isShowingMenu 상태에 따라 메뉴 여닫기 zIndex 으로 조정
            if isShowingMenu {
                
                MenuView()
                    .transition(.move(edge: .leading))
                    .zIndex(2)
                
                // 메뉴 외부를 클릭하면 메뉴를 닫습니다.
                Button(action: {
                    withAnimation {
                        self.isShowingMenu = false
                    }
                }) {
                    Color.gray
                        .edgesIgnoringSafeArea(.all)
                        .opacity(0.5)
                }
                .zIndex(1)
                
            }
            
            
        }
        
        
    }
    
}

#Preview {
    MainContainer()
}
