//
//  RegistrationEmployeeView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/26/23.
//

import SwiftUI

enum UserTypeInRegistrationEmployeeView: String, CaseIterable, Identifiable {
    case employee = "employee"
    case creater = "creater"
    
    var id: Self { self }
}

struct RegistrationEmployeeView: View {
    
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    
    @State private var showToast: Bool = false
    @State private var toastText: String = ""
    
    @State private var selectedUserType: UserTypeInRegistrationEmployeeView = UserTypeInRegistrationEmployeeView.employee
    @State private var userTypeText: String = ""
    
    @State private var userNameFor_EmployeeAndCreaterManagement: String = ""
    @State private var userPositionFor_EmployeeAndCreaterManagement: String = ""
    
    @State private var userDepartmentFor_EmployeeAndCreaterManagement: String = ""
    @State private var userPhoneNumberFor_EmployeeAndCreaterManagement: String = ""
    @State private var userEmailFor_EmployeeAndCreaterManagement: String = ""
    
    @State private var showingUserImagePickerFor_EmployeeAndCreaterRegistration = false
    @State private var selectedUserImageFor_EmployeeAndCreaterRegistration: UIImage? = nil
    
    
    @State private var navigate: Bool = false
    
    var body: some View {
        
        
        ZStack{
            
            
            ScrollView{
                
                HStack(spacing: 0) {
                    ForEach(UserTypeInRegistrationEmployeeView.allCases, id: \.self) { flavor in
                        Button(action: {
                            selectedUserType = flavor
                            userTypeText = "\(flavor)"
                            print("\(flavor)")
                        }) {
                            Text(flavor == .employee ? "소속직원" : "크리에이터")
                                .frame(width: screenWidth/2.272, height: screenHeight/23.97)
                                .font(.system(size: 16))
                            
                                
                            
                        }
                        .background(selectedUserType == flavor ? Color.blue : Color.clear)
                        .foregroundColor(selectedUserType == flavor ? .white : .blue)
                        .cornerRadius(4)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("mainColor"), lineWidth: 1)
                        )
                        
                        
                        
                        
                    }
                }
                .overlay(RoundedRectangle(cornerRadius: 4)
                    .stroke(Color("mainColor"), lineWidth: 1)
                )
                .padding(.top, screenHeight / 25.46875) //32
                
                
                VStack(spacing: 0){
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("이름")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $userNameFor_EmployeeAndCreaterManagement, prompt: Text("이름을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 이름
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("직위")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $userPositionFor_EmployeeAndCreaterManagement, prompt: Text("직위를 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 직위
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("담당부서")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $userDepartmentFor_EmployeeAndCreaterManagement, prompt: Text("담당부서를 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 담당부서
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("전화번호")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $userPhoneNumberFor_EmployeeAndCreaterManagement, prompt: Text("전화번호를 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.phonePad)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 전화번호
                    .padding(.top, screenHeight / 54.133) //15
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("이메일")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $userEmailFor_EmployeeAndCreaterManagement, prompt: Text("이메일을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.emailAddress)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 이메일
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    Spacer()
                    
                    
                    VStack(alignment: .leading){//계약서 등록
                        
                        HStack(spacing: 0){
                            Text("대표 이미지")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        
                        Button(action: {
                            showingUserImagePickerFor_EmployeeAndCreaterRegistration = true
                        }, label: {
                            
                            ZStack {
                                if let image = selectedUserImageFor_EmployeeAndCreaterRegistration {
                                    Image(uiImage: image)
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                } else {
                                    Rectangle()
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                        .foregroundColor(.colorE5E5E5)
                                    
                                    Circle()
                                        .frame(width: screenWidth/10.416)
                                        .foregroundColor(.colorEFEFF4)
                                        .overlay{
                                            Image(systemName: "plus")
                                                .imageScale(.medium)
                                        }
                                }
                            }
                            
                        })
                        .padding(.leading, screenWidth / 18.75)
                        
                    }//VStack 대표 이미지
                    .padding(.top, screenHeight / 54.133) //20    54.133 = 15
                    .sheet(isPresented: $showingUserImagePickerFor_EmployeeAndCreaterRegistration, onDismiss: loadUserImageForEmployeeAndCreaterRegistration) {
                        ImagePicker(selectedImage: $selectedUserImageFor_EmployeeAndCreaterRegistration)
                    }
                    
                    HStack{
                        
                        Button(action: {
                            
                            if(userNameFor_EmployeeAndCreaterManagement == "" || userPositionFor_EmployeeAndCreaterManagement == "" || userDepartmentFor_EmployeeAndCreaterManagement == "" || userPhoneNumberFor_EmployeeAndCreaterManagement == "" || userEmailFor_EmployeeAndCreaterManagement == "" || selectedUserImageFor_EmployeeAndCreaterRegistration == nil){
                                
                                toastText = "칸을 비울 수 없습니다."
                                showToast = true
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                    self.showToast = false
                                    toastText = ""
                                }
                                
                            }else{
                                
                                self.presentationMode.wrappedValue.dismiss()
                            }
                             
                            
                            
                        }, label: {
                            
                                Text("직원/크리에이터 등록")
                                    .foregroundColor(.white)
                                    .font(.system(size: 16))
                                    .bold()
                                    .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                            
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/67.9166) // 37
                        
                        
                        
                    }
                    
                    
                    
                    
                    Spacer()
                    
                    
                    
                    
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: Button(action: {
                    
                    self.presentationMode.wrappedValue.dismiss()
                    
                    
                }) {
                    
                    HStack(spacing: 0){
                        
                        Image(systemName: "chevron.backward")
                        
                    }
                    
                })
                .navigationBarTitle("직원/크리에이터 등록", displayMode: .inline)
                
                
            }//ScrollView
            .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                UIApplication.shared.endEditing()
                
            }
            .toast(isShowing: $showToast, text: Text(toastText))
            
        }//ZStack
        
    }
    
    private func loadUserImageForEmployeeAndCreaterRegistration() {
        guard let selectedUserImageFor_EmployeeAndCreaterRegistration = selectedUserImageFor_EmployeeAndCreaterRegistration else { return }
        // You can do something with the selected brand image here
    }
    
}

#Preview {
    RegistrationEmployeeView()
}
