//
//  SmartServiceDesignApp.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/11/23.
//

import SwiftUI

@main
struct SmartServiceDesignApp: App {
    let persistenceController = PersistenceController.shared

    let loginData = LoginData()
    let registrationCustomerBrand = RegistrationCustomerBrandData()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(loginData)
                .environmentObject(registrationCustomerBrand)
        }
    }
}
