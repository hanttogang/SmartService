//
//  ContractDetailOfCreater.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/27/23.
//

import SwiftUI

struct ContractDetailOfCreaterView: View {
    
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    
    var selectedDetailCreaterName: String = ""
    var selectedDetailContractNameFor_DetailCreater: String = ""
    
    
    @State private var navigate: Bool = false
    
    @State private var showToast: Bool = false
    @State private var toastText: String = ""
    
    @State private var modificationModeFor_DetailCreater: Bool = false
    
    
    @State private var contractNameFor_DetailCreater: String = "2023 광고 계약"
    @State private var contractOneLineExplanFor_DetailCreater: String = "광고계약 3차"
    @State private var contractStartDateFor_DetailCreater: String = "2023.12.01"
    @State private var contractEndDateFor_DetailCreater: String = "2023.12.31"
    @State private var contractPriceFor_DetailCreater: String = "10,000,000원"
    @State private var contractStatusFor_DetailCreater: String = "신규"
    
    
    @State private var selecteContractStatusFor_DetailCreater: Bool = false
    @State private var contractStatusModificationModeFor_DetailCreater: Bool = false
    
    @State private var showingBusinessLicenseImagePickerFor_DetailCreater = false
    @State private var selectedBusniessLicenseImageFor_DetailCreater: UIImage? = nil
    
    
  
    
    var body: some View {
        
        ZStack {
            ScrollView(.vertical){
                VStack(spacing: 0){
                    
                    HStack{
                        
                    
                        
                        if modificationModeFor_DetailCreater{
                            
                            TextField(".", text: $contractNameFor_DetailCreater, prompt: Text("\(contractNameFor_DetailCreater)")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.title2)
                            .frame(height: screenHeight/29.1) //28
                            
                            
                        }else {
                            
                            Text("\(contractNameFor_DetailCreater)")
                                .font(.title2)
                                .frame(height: screenHeight/29.1) //28
                        }
                        
                        
                        
                        
                        
                        Spacer()
                        
                        if(modificationModeFor_DetailCreater){
                            Button(action: {
                                
                                
                                if (contractNameFor_DetailCreater == "" || contractOneLineExplanFor_DetailCreater == "" || contractStartDateFor_DetailCreater == "" || contractEndDateFor_DetailCreater == "" || contractPriceFor_DetailCreater == "" || selectedBusniessLicenseImageFor_DetailCreater == nil){
                                    
                                    toastText = "칸을 비울 수 없습니다."
                                    showToast = true
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                        self.showToast = false
                                        toastText = ""
                                    }
                                    
                                } else {
                                    
                                    modificationModeFor_DetailCreater = false
                                }
                                
                                
                                
                            }, label: {
                                Text("저장")
                                
                            })
                        } else {
                            Button(action: {
                                
                                modificationModeFor_DetailCreater = true
                                
                                
                            }, label: {
                                Text("수정")
                                
                            })
                            
                        }
                        
                        
                    }// HStack 계약 이름
                    .padding(.horizontal)
                    .padding(.vertical, screenHeight / 81.5)
//                    .padding(.top, screenHeight / 81.5)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        
                        Text("한줄 설명")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationModeFor_DetailCreater{
                            RightAlignedTextField(text: $contractOneLineExplanFor_DetailCreater)
                                .keyboardType(.numberPad)
                            
                            
                            
                        }else {
                            
                            Text("\(contractOneLineExplanFor_DetailCreater)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("계약기간")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationModeFor_DetailCreater{
//                            RightAlignedTextField(text: $typeOfBusinessForDetailBrand)
//                                .keyboardType(.default)
//                                .autocapitalization(.none)
                            
                            Text("\(contractStartDateFor_DetailCreater) ~ \(contractEndDateFor_DetailCreater)")
                                .foregroundColor(Color("color00000040"))

                            
                            
                        }else {
                            
                            Text("\(contractStartDateFor_DetailCreater) ~ \(contractEndDateFor_DetailCreater)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{
                        Text("계약금액")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationModeFor_DetailCreater{
                            RightAlignedTextField(text: $contractPriceFor_DetailCreater)
                                .keyboardType(.default)
                                .autocapitalization(.none)
                            
                            
                            
                        }else {
                            
                            Text("\(contractPriceFor_DetailCreater)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack(spacing: 0){
                        Text("진행상태")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        
                        HStack(spacing: 0){
                            
                            if modificationModeFor_DetailCreater{
                                
                                
                                Button(action: {
                                    selecteContractStatusFor_DetailCreater = true
                                }, label: {
                                    
                                    if selecteContractStatusFor_DetailCreater{
                                        
                                        HStack(spacing: screenWidth/128.3){ // spacing 3
                                            Button(action: {
                                                selecteContractStatusFor_DetailCreater = false
                                                
                                                contractStatusFor_DetailCreater = "신규"
                                                
                                            }, label: {
                                                Text("신규")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailCreater = false
                                                
                                                contractStatusFor_DetailCreater = "갱신"
                                                
                                            }, label: {
                                                Text("갱신")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailCreater = false
                                                
                                                contractStatusFor_DetailCreater = "변경"
                                                
                                            }, label: {
                                                Text("변경")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailCreater = false
                                                
                                                contractStatusFor_DetailCreater = "완료"
                                                
                                            }, label: {
                                                Text("완료")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailCreater = false
                                                
                                                contractStatusFor_DetailCreater = "신규"
                                                
                                            }, label: {
                                                Text("신규")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                        } //HStack
                                        
                                        
                                    }else{
                                        
                                        Button(action: {
                                            selecteContractStatusFor_DetailCreater = true
                                            
                                        }, label: {
                                            Text("\(contractStatusFor_DetailCreater)")
                                                .foregroundColor(.white)
                                                .bold()
                                                .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                .background(Color("mainColor"))
                                                .cornerRadius(16.0)
                                        })
                                    }
                                    
                                })
                            } else{
                                
                                Text("\(contractStatusFor_DetailCreater)")
                                    .foregroundColor(.white)
                                    .bold()
                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                    .background(Color("mainColor"))
                                    .cornerRadius(16.0)
                            }
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    Divider()
                        .padding(.leading)
                    
                   
                    
                    
                    
                    HStack(spacing: 0){
                        
                        
                        VStack(alignment: .leading){// 사업자등록증 이미지
                            
                            Text("계약서 이미지")
                                .padding(.top, screenHeight/30.75)
                            
                            ZStack{
                                
                                
                                if modificationModeFor_DetailCreater{
                                    
                                    Button(action: {
                                        showingBusinessLicenseImagePickerFor_DetailCreater = true
                                    }, label: {
                                        
                                        ZStack {
                                            if let image = selectedBusniessLicenseImageFor_DetailCreater {
                                                Image(uiImage: image)
                                                    .resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                    .cornerRadius(12.0)
                                                
                                            } else {
                                                Rectangle()
                                                    .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                    .cornerRadius(12.0)
                                                    .foregroundColor(.colorE5E5E5)
                                                
                                            }
                                        }
                                    })
                                    
                                }else{
                                    
                                    ZStack {
                                        if let image = selectedBusniessLicenseImageFor_DetailCreater {
                                            Image(uiImage: image)
                                                .resizable()
                                                .aspectRatio(contentMode: .fill)
                                                .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                .cornerRadius(12.0)
                                            
                                        } else {
                                            Rectangle()
                                                .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                .cornerRadius(12.0)
                                                .foregroundColor(.colorE5E5E5)
                                            
                                        }
                                    }
                                    
                                }
                            } //ZStack 사업자등록증이미지
                            .sheet(isPresented: $showingBusinessLicenseImagePickerFor_DetailCreater, onDismiss: loadBusniessLicenseImageFor_DetailCreater) {
                                ImagePicker(selectedImage: $selectedBusniessLicenseImageFor_DetailCreater)
                            }
                            
                            
                            
                        }//VStack 사업자등록증 이미지
                        
                        
                        
                        Spacer()
                        
                        
                      
                    }//HStack
                    .padding(.leading)
                    
                    
                    
                } //VStack
                
                
            }//ScrollView
            .toast(isShowing: $showToast, text: Text(toastText))
            
            
            
            
            
        }//ZStack
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: {
            
            self.presentationMode.wrappedValue.dismiss()
            
            
        }) {
            
            HStack(spacing: 0){
                
                Image(systemName: "chevron.backward")
                
            }
            
            
            
        })
        .navigationBarTitle("\(selectedDetailCreaterName)", displayMode: .inline)
        
      
        
        
        
    }//body

    
  
    private func loadBusniessLicenseImageFor_DetailCreater() {
        guard let selectedBusniessLicenseImageFor_DetailCreater = selectedBusniessLicenseImageFor_DetailCreater else { return }
        // You can do something with the selected brand image here
    }
}

#Preview {
    ContractDetailOfCreaterView()
}
