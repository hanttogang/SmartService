//
//  DetailedTaskView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/19/23.
//

import SwiftUI


struct DetaileTaskView: View {
    
    var selectedSchedulName: String = ""
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    
    var selectedDetailTaskName: String = ""
    var selectedDetailTNameFor_DetailTask: String = ""
    
    
    @State private var navigate: Bool = false
    
    @State private var showToast: Bool = false
    @State private var toastText: String = ""
    
    @State private var modificationModeFor_DetailTask: Bool = false
    
    
    @State private var taskNameFor_DetailTask: String = "유튜브 PPL 브랜디드"
    @State private var taskOneLineExplanFor_DetailTask: String = "2023년 3월 유튜브 PPL"
    @State private var taskStartDateFor_DetailTask: String = "2023.12.01"
    @State private var taskEndDateFor_DetailTask: String = "2023.12.31"
    
    @State private var taskNumberOfParticipantsFor_DetailTask: String = "10"
    @State private var taskStatusFor_DetailTask: String = "신규"
    @State private var hideStatusFor_DetailTask: String = "미처리"
    
    
    @State private var selecteTaskStatusFor_DetailTask: Bool = false
    @State private var selecteHideStatusFor_DetailTask: Bool = false
    
    @State private var taskStatusModificationModeFor_DetailTask: Bool = false
    
    
    @State private var showMemoList: Bool = false
    @State private var showShowAllButton: Bool = true
    
    @State var isShowingMemo: Bool = false
    @State var writedMemo: String = ""
    
    var body: some View {
        ZStack{
            ZStack {
                ScrollView(.vertical){
                    VStack(spacing: 0){
                        
                        HStack{
                            
                        
                            
                            if modificationModeFor_DetailTask{
                                
                                TextField(".", text: $taskNameFor_DetailTask, prompt: Text("\(taskNameFor_DetailTask)")
                                    .foregroundColor(Color("hintTextColor")))
                                .keyboardType(.default)
                                .font(.title2)
                                .frame(height: screenHeight/29.1) //28
                                
                                
                            }else {
                                
                                Text("\(taskNameFor_DetailTask)")
                                    .font(.title2)
                                    .frame(height: screenHeight/29.1) //28
                            }
                            
                            
                            
                            
                            
                            Spacer()
                            
                            if(modificationModeFor_DetailTask){
                                Button(action: {
                                    
                                    
                                    if (taskNameFor_DetailTask == "" || taskOneLineExplanFor_DetailTask == "" || taskStartDateFor_DetailTask == "" || taskEndDateFor_DetailTask == "" || taskNumberOfParticipantsFor_DetailTask == ""){
                                        
                                        toastText = "칸을 비울 수 없습니다."
                                        showToast = true
                                        
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                            self.showToast = false
                                            toastText = ""
                                        }
                                        
                                    } else {
                                        
                                        modificationModeFor_DetailTask = false
                                    }
                                    
                                    
                                    
                                }, label: {
                                    Text("저장")
                                    
                                })
                            } else {
                                Button(action: {
                                    
                                    modificationModeFor_DetailTask = true
                                    
                                    
                                }, label: {
                                    Text("수정")
                                    
                                })
                                
                            }
                            
                            
                        }// HStack 계약 이름
                        .padding(.horizontal)
                        .padding(.vertical, screenHeight / 81.5)
    //                    .padding(.top, screenHeight / 81.5)
                        
                        
                        Divider()
                            .padding(.leading)
                        
                        
                        HStack{
                            
                            Text("한줄 설명")
                                .padding(.vertical, screenHeight / 81.5)
                            
                            Spacer()
                            
                            if modificationModeFor_DetailTask{
                                RightAlignedTextField(text: $taskOneLineExplanFor_DetailTask)
                                    .keyboardType(.numberPad)
                                
                                
                                
                            }else {
                                
                                Text("\(taskOneLineExplanFor_DetailTask)")
                                    .foregroundColor(Color("color00000040"))
                            }
                            
                            
                            
                            
                            
                            
                        }
                        .padding(.horizontal)
                        
                        
                        Divider()
                            .padding(.leading)
                        
                        
                        HStack{
                            Text("일정")
                                .padding(.vertical, screenHeight / 81.5)
                            
                            Spacer()
                            
                            if modificationModeFor_DetailTask{
    //                            RightAlignedTextField(text: $typeOfBusinessForDetailBrand)
    //                                .keyboardType(.default)
    //                                .autocapitalization(.none)
                                
                                Text("\(taskStartDateFor_DetailTask) ~ \(taskEndDateFor_DetailTask)")
                                    .foregroundColor(Color("color00000040"))

                                
                                
                            }else {
                                
                                Text("\(taskStartDateFor_DetailTask) ~ \(taskEndDateFor_DetailTask)")
                                    .foregroundColor(Color("color00000040"))
                            }
                            
                        }
                        .padding(.horizontal)
                        
                        
                        Divider()
                            .padding(.leading)
                        
                        HStack{
                            Text("참여인원")
                                .padding(.vertical, screenHeight / 81.5)
                            
                            Spacer()
                            
                            if modificationModeFor_DetailTask{
                                RightAlignedTextField(text: $taskNumberOfParticipantsFor_DetailTask)
                                    .keyboardType(.default)
                                    .autocapitalization(.none)
                                
                                
                                
                            }else {
                                
                                Text("\(taskNumberOfParticipantsFor_DetailTask)")
                                    .foregroundColor(Color("color00000040"))
                            }
                            
                            
                            
                        }
                        .padding(.horizontal)
                        
                        
                        Divider()
                            .padding(.leading)
                        
                        
                        HStack(spacing: 0){
                            Text("진행상태")
                                .padding(.vertical, screenHeight / 81.5)
                            
                            Spacer()
                            
                            
                            HStack(spacing: 0){
                                
                                if modificationModeFor_DetailTask{
                                    
                                    
                                    Button(action: {
                                        taskStatusModificationModeFor_DetailTask = true
                                    }, label: {
                                        
                                        if taskStatusModificationModeFor_DetailTask{
                                            
                                            HStack(spacing: screenWidth/128.3){ // spacing 3
                                                Button(action: {
                                                    taskStatusModificationModeFor_DetailTask = false
                                                    
                                                    taskStatusFor_DetailTask = "신규"
                                                    
                                                }, label: {
                                                    Text("신규")
                                                        .foregroundColor(.white)
                                                        .bold()
                                                        .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                        .background(Color("mainColor"))
                                                        .cornerRadius(16.0)
                                                })
                                                
                                                Button(action: {
                                                    taskStatusModificationModeFor_DetailTask = false
                                                    
                                                    taskStatusFor_DetailTask = "갱신"
                                                    
                                                }, label: {
                                                    Text("갱신")
                                                        .foregroundColor(.white)
                                                        .bold()
                                                        .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                        .background(Color("mainColor"))
                                                        .cornerRadius(16.0)
                                                })
                                                
                                                Button(action: {
                                                    taskStatusModificationModeFor_DetailTask = false
                                                    
                                                    taskStatusFor_DetailTask = "변경"
                                                    
                                                }, label: {
                                                    Text("변경")
                                                        .foregroundColor(.white)
                                                        .bold()
                                                        .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                        .background(Color("mainColor"))
                                                        .cornerRadius(16.0)
                                                })
                                                
                                                Button(action: {
                                                    taskStatusModificationModeFor_DetailTask = false
                                                    
                                                    taskStatusFor_DetailTask = "완료"
                                                    
                                                }, label: {
                                                    Text("완료")
                                                        .foregroundColor(.white)
                                                        .bold()
                                                        .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                        .background(Color("mainColor"))
                                                        .cornerRadius(16.0)
                                                })
                                                
                                                Button(action: {
                                                    taskStatusModificationModeFor_DetailTask = false
                                                    
                                                    taskStatusFor_DetailTask = "신규"
                                                    
                                                }, label: {
                                                    Text("신규")
                                                        .foregroundColor(.white)
                                                        .bold()
                                                        .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                        .background(Color("mainColor"))
                                                        .cornerRadius(16.0)
                                                })
                                            } //HStack
                                            
                                            
                                        }else{
                                            
                                            Button(action: {
                                                taskStatusModificationModeFor_DetailTask = true
                                                
                                            }, label: {
                                                Text("\(taskStatusFor_DetailTask)")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                        }
                                        
                                    })
                                } else{
                                    
                                    Text("\(taskStatusFor_DetailTask)")
                                        .foregroundColor(.white)
                                        .bold()
                                        .frame(width: screenWidth/7, height: screenHeight/29.107)
                                        .background(Color("mainColor"))
                                        .cornerRadius(16.0)
                                }
                            }
                            
                        }
                        .padding(.horizontal)
                        
                        Divider()
                            .padding(.leading)
                        
                        
                        
                        HStack(spacing: 0){
                            Text("숨김처리")
                                .padding(.vertical, screenHeight / 81.5)
                            
                            Spacer()
                            
                            
                            HStack(spacing: 0){
                                
                                if modificationModeFor_DetailTask{
                                    
                                    
                                    Button(action: {
                                        selecteHideStatusFor_DetailTask = true
                                    }, label: {
                                        
                                        if selecteHideStatusFor_DetailTask{
                                            
                                            HStack(spacing: screenWidth/128.3){ // spacing 3
                                                Button(action: {
                                                    selecteHideStatusFor_DetailTask = false
                                                    
                                                    hideStatusFor_DetailTask = "숨김"
                                                    
                                                }, label: {
                                                    Text("숨김")
                                                        .foregroundColor(.white)
                                                        .bold()
                                                        .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                        .background(Color("mainColor"))
                                                        .cornerRadius(16.0)
                                                })
                                                
                                                Button(action: {
                                                    selecteHideStatusFor_DetailTask = false
                                                    
                                                    hideStatusFor_DetailTask = "미처리"
                                                    
                                                }, label: {
                                                    Text("미처리")
                                                        .foregroundColor(.white)
                                                        .bold()
                                                        .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                        .background(Color("mainColor"))
                                                        .cornerRadius(16.0)
                                                })
                                                
                                            } //HStack
                                            
                                            
                                        }else{
                                            
                                            Button(action: {
                                                selecteHideStatusFor_DetailTask = true
                                                
                                            }, label: {
                                                Text("\(hideStatusFor_DetailTask)")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                        }
                                        
                                    })
                                } else{
                                    
                                    Text("\(hideStatusFor_DetailTask)")
                                        .foregroundColor(.white)
                                        .bold()
                                        .frame(width: screenWidth/7, height: screenHeight/29.107)
                                        .background(Color("mainColor"))
                                        .cornerRadius(16.0)
                                }
                            }
                            
                        }
                        .padding(.horizontal)
                        
                        Divider()
                            .padding(.leading)
                        
                       
                        
                        
                        HStack{
                            
                            Button(action: {
                                print("메모 작성 버튼 클릭")
                                
                                isShowingMemo = true
                            }, label: {
                                    Text("메모 작성")
                                        .foregroundColor(.white)
                                        .font(.system(size: 16))
                                        .bold()
                                        .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                                }
                            )
                            .background(Color("mainColor"))
                            .cornerRadius(4)
                            .padding(.top, screenHeight/21.945) // 37
                            
                            
                            
                        }
                        
                        VStack(spacing: 0) {
                            
                            
                            VStack{//메모 리스트
                                
                                ForEach(contractDummyData.sorted(by: { $0.endDate > $1.endDate }).prefix(showMemoList ? contractDummyData.count : 3)) { contract in
                                    
                                   
                                        VStack{
                                            HStack{ //리스트항목의 모델
                                                
                                                if let image = contract.image {
                                                    Image(uiImage: image)
                                                        .resizable()
                                                        .aspectRatio(contentMode: .fill)
                                                        .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                        .cornerRadius(12.0)
                                                } else {
                                                    Rectangle()
                                                        .frame(width: screenWidth/9.375, height: screenHeight/20.375)
                                                        .cornerRadius(12.0)
                                                        .foregroundColor(.colorE5E5E5)
                                                }
                                                
                                                VStack(alignment: .leading){
                                                    Text(contract.repName)
                                                        .foregroundColor(.black)
                                                    
                                                    Text("\(contract.formattedStartDate) ~ \(contract.formattedEndDate)")
                                                        .foregroundColor(Color("color00000040"))
                                                        .font(.caption)
                                                }
                                                
                                                Spacer()
                                                
                                            }
                                            
                                            Divider()
                                                .padding(.leading, screenWidth/8)
                                        }
                                        
                                    
                                    
                                }
                                
                                
                                
                                
                            } //VStack 계약 리스트
                            .listStyle(PlainListStyle())
                            .padding(.horizontal)
                            .padding(.top, screenHeight/50.9375)
                            
                            
                            if showShowAllButton{
                                
                                Button(action: {
                                    
                                    showShowAllButton = false
                                    showMemoList = true
                                    
                                }, label: {
                                    
                                    Text("더 보기")
                                        .foregroundColor(.black)
                                        .font(.system(size: 16))
                                        .bold()
                                        .frame(width: screenWidth/1.25, height: screenHeight/20.375)
                                })
                                .background(.white)
                                .cornerRadius(4)
                                .padding()
                                .overlay(RoundedRectangle(cornerRadius: 4)
                                    .stroke(Color(.black), lineWidth: 1)
                                    .padding()
                                )
                                
                            }
                            
                            Spacer()
                            
                            
                            
                            
                        }//VStack
                        .frame(width: screenWidth/1.0932)
                        .background(Color.white)
                        .cornerRadius(20)
                        .shadow(color: Color("color00000040"), radius: 20, x: 0, y: 30)
                        .padding(.top, screenHeight/31.346)
                        
                        
                    } //VStack
                    
                    
                }//ScrollView
                .toast(isShowing: $showToast, text: Text(toastText))
                
                
                
                
                
            }//ZStack
            .navigationBarBackButtonHidden(true)
            .navigationBarItems(leading: Button(action: {
                
                self.presentationMode.wrappedValue.dismiss()
                
                
            }) {
                
                HStack(spacing: 0){
                    
                    Image(systemName: "chevron.backward")
                    
                }
                
                
                
            })
            .navigationBarTitle("\(selectedDetailTaskName)", displayMode: .inline)
            
            
            
            ZStack{
                // isShowingMenu 상태에 따라 메뉴 여닫기 zIndex 으로 조정
                if isShowingMemo {
                    
                    ZStack{//메모 뷰
                            
                        VStack{
                            VStack{
                                Spacer()
                                
                                Text("메모 작성")
                                    .padding(.vertical, screenHeight/74.09)
                                    .bold()
                            }
                            .padding(.bottom, 0)
                            
                            Divider()
                            
                            
                            
                            VStack{
                                TopAlignedTextField(text: $writedMemo)
                                    .frame(width: screenWidth/1.2, height: screenHeight/8.15)
                                    .autocapitalization(.none)
                                    .foregroundColor(Color("hintTextColor"))
                                
                            }
                            
                            Spacer()
                            
                            Divider()
                            
                            HStack{
                                Spacer()
                                
                                Button(action: {
                                    self.isShowingMemo = false
                                }, label: {
                                    
                                    Text("등록")
                                        .bold()
                                        .foregroundColor(.black)
                                    
                                    Image(systemName: "chevron.right")
                                        .foregroundColor(.black)
                                })
                            }
                            .padding(.vertical, screenHeight/74.09)
                            .padding(.trailing, screenWidth/25)
                            
                        }
                        .frame(width: screenWidth/1.0932, height: screenHeight/3.844)
                        .background(.colorF8F8F892)
                        .cornerRadius(8.0)
                        
                    }//ZStack 메모
                        .transition(.move(edge: .leading))
                        .zIndex(2)
                    
                    // 메뉴 외부를 클릭하면 메뉴를 닫습니다.
                    Button(action: {
    //                    withAnimation {
                            self.isShowingMemo = false
    //                    }
                    }) {
                        Color.gray
                            .edgesIgnoringSafeArea(.all)
                            .opacity(0.9)
                    }
                    .zIndex(1)
                    
                }
            }
                
          
        }//ZStack
        
        
        
        
        
    }//body

    
}

#Preview {
    DetaileTaskView()
}
