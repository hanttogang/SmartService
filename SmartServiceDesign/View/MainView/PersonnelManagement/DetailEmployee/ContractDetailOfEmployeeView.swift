//
//  ContractDetailOfEmployeeView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/27/23.
//

import SwiftUI

struct ContractDetailOfEmployeeView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    var selectedDetailContractNameFor_DetailEmployee: String = ""
    var selectedDetailEmployeeName: String = ""
    
    @State private var navigate: Bool = false
    
    @State private var showToast: Bool = false
    @State private var toastText: String = ""
    
    @State private var modificationModeFor_DetailEmployee: Bool = false
    
    
    @State private var contractNameFor_DetailEmployee: String = "2023 보직 변경"
    @State private var contractOneLineExplanFor_DetailEmployee: String = "보직변경 2차"
    @State private var contractStartDateFor_DetailEmployee: String = "2023.12.01"
    @State private var contractEndDateFor_DetailEmployee: String = "2023.12.31"
    @State private var contractPriceFor_DetailEmployee: String = "10,000,000원"
    @State private var contractStatusFor_DetailEmployee: String = "신규"
    
    
    @State private var selecteContractStatusFor_DetailEmployee: Bool = false
    @State private var contractStatusModificationModeFor_DetailEmployee: Bool = false
    
    @State private var showingBusinessLicenseImagePickerFor_DetailEmployee = false
    @State private var selectedBusniessLicenseImageFor_DetailEmployee: UIImage? = nil
    
    
  
    
    var body: some View {
        
        ZStack {
            ScrollView(.vertical){
                VStack(spacing: 0){
                    
                    HStack{
                        
                    
                        
                        if modificationModeFor_DetailEmployee{
                            
                            TextField(".", text: $contractNameFor_DetailEmployee, prompt: Text("\(contractNameFor_DetailEmployee)")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.title2)
                            .frame(height: screenHeight/29.1) //28
                            
                            
                        }else {
                            
                            Text("\(contractNameFor_DetailEmployee)")
                                .font(.title2)
                                .frame(height: screenHeight/29.1) //28
                        }
                        
                        
                        
                        
                        
                        Spacer()
                        
                        if(modificationModeFor_DetailEmployee){
                            Button(action: {
                                
                                
                                if (contractNameFor_DetailEmployee == "" || contractOneLineExplanFor_DetailEmployee == "" || contractStartDateFor_DetailEmployee == "" || contractEndDateFor_DetailEmployee == "" || contractPriceFor_DetailEmployee == "" || selectedBusniessLicenseImageFor_DetailEmployee == nil){
                                    
                                    toastText = "칸을 비울 수 없습니다."
                                    showToast = true
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                        self.showToast = false
                                        toastText = ""
                                    }
                                    
                                } else {
                                    
                                    modificationModeFor_DetailEmployee = false
                                }
                                
                                
                                
                            }, label: {
                                Text("저장")
                                
                            })
                        } else {
                            Button(action: {
                                
                                modificationModeFor_DetailEmployee = true
                                
                                
                            }, label: {
                                Text("수정")
                                
                            })
                            
                        }
                        
                        
                    }// HStack 계약 이름
                    .padding(.horizontal)
                    .padding(.vertical, screenHeight / 81.5)
//                    .padding(.top, screenHeight / 81.5)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        
                        Text("한줄 설명")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationModeFor_DetailEmployee{
                            RightAlignedTextField(text: $contractOneLineExplanFor_DetailEmployee)
                                .keyboardType(.numberPad)
                            
                            
                            
                        }else {
                            
                            Text("\(contractOneLineExplanFor_DetailEmployee)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    
                    HStack{
                        Text("계약기간")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationModeFor_DetailEmployee{
//                            RightAlignedTextField(text: $typeOfBusinessForDetailBrand)
//                                .keyboardType(.default)
//                                .autocapitalization(.none)
                            
                            Text("\(contractStartDateFor_DetailEmployee) ~ \(contractEndDateFor_DetailEmployee)")
                                .foregroundColor(Color("color00000040"))

                            
                            
                        }else {
                            
                            Text("\(contractStartDateFor_DetailEmployee) ~ \(contractEndDateFor_DetailEmployee)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack{
                        Text("계약금액")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        if modificationModeFor_DetailEmployee{
                            RightAlignedTextField(text: $contractPriceFor_DetailEmployee)
                                .keyboardType(.default)
                                .autocapitalization(.none)
                            
                            
                            
                        }else {
                            
                            Text("\(contractPriceFor_DetailEmployee)")
                                .foregroundColor(Color("color00000040"))
                        }
                        
                        
                        
                    }
                    .padding(.horizontal)
                    
                    
                    Divider()
                        .padding(.leading)
                    
                    HStack(spacing: 0){
                        Text("진행상태")
                            .padding(.vertical, screenHeight / 81.5)
                        
                        Spacer()
                        
                        
                        HStack(spacing: 0){
                            
                            if modificationModeFor_DetailEmployee{
                                
                                
                                Button(action: {
                                    selecteContractStatusFor_DetailEmployee = true
                                }, label: {
                                    
                                    if selecteContractStatusFor_DetailEmployee{
                                        
                                        HStack(spacing: screenWidth/128.3){ // spacing 3
                                            Button(action: {
                                                selecteContractStatusFor_DetailEmployee = false
                                                
                                                contractStatusFor_DetailEmployee = "신규"
                                                
                                            }, label: {
                                                Text("신규")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailEmployee = false
                                                
                                                contractStatusFor_DetailEmployee = "갱신"
                                                
                                            }, label: {
                                                Text("갱신")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailEmployee = false
                                                
                                                contractStatusFor_DetailEmployee = "변경"
                                                
                                            }, label: {
                                                Text("변경")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailEmployee = false
                                                
                                                contractStatusFor_DetailEmployee = "완료"
                                                
                                            }, label: {
                                                Text("완료")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                            
                                            Button(action: {
                                                selecteContractStatusFor_DetailEmployee = false
                                                
                                                contractStatusFor_DetailEmployee = "퇴사"
                                                
                                            }, label: {
                                                Text("퇴사")
                                                    .foregroundColor(.white)
                                                    .bold()
                                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                    .background(Color("mainColor"))
                                                    .cornerRadius(16.0)
                                            })
                                        } //HStack
                                        
                                        
                                    }else{
                                        
                                        Button(action: {
                                            selecteContractStatusFor_DetailEmployee = true
                                            
                                        }, label: {
                                            Text("\(contractStatusFor_DetailEmployee)")
                                                .foregroundColor(.white)
                                                .bold()
                                                .frame(width: screenWidth/7, height: screenHeight/29.107)
                                                .background(Color("mainColor"))
                                                .cornerRadius(16.0)
                                        })
                                    }
                                    
                                })
                            } else{
                                
                                Text("\(contractStatusFor_DetailEmployee)")
                                    .foregroundColor(.white)
                                    .bold()
                                    .frame(width: screenWidth/7, height: screenHeight/29.107)
                                    .background(Color("mainColor"))
                                    .cornerRadius(16.0)
                            }
                        }
                        
                    }
                    .padding(.horizontal)
                    
                    Divider()
                        .padding(.leading)
                    
                   
                    
                    
                    
                    HStack(spacing: 0){
                        
                        
                        VStack(alignment: .leading){// 사업자등록증 이미지
                            
                            Text("계약서 이미지")
                                .padding(.top, screenHeight/30.75)
                            
                            ZStack{
                                
                                
                                if modificationModeFor_DetailEmployee{
                                    
                                    Button(action: {
                                        showingBusinessLicenseImagePickerFor_DetailEmployee = true
                                    }, label: {
                                        
                                        ZStack {
                                            if let image = selectedBusniessLicenseImageFor_DetailEmployee {
                                                Image(uiImage: image)
                                                    .resizable()
                                                    .aspectRatio(contentMode: .fill)
                                                    .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                    .cornerRadius(12.0)
                                                
                                            } else {
                                                Rectangle()
                                                    .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                    .cornerRadius(12.0)
                                                    .foregroundColor(.colorE5E5E5)
                                                
                                            }
                                        }
                                    })
                                    
                                }else{
                                    
                                    ZStack {
                                        if let image = selectedBusniessLicenseImageFor_DetailEmployee {
                                            Image(uiImage: image)
                                                .resizable()
                                                .aspectRatio(contentMode: .fill)
                                                .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                .cornerRadius(12.0)
                                            
                                        } else {
                                            Rectangle()
                                                .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                                .cornerRadius(12.0)
                                                .foregroundColor(.colorE5E5E5)
                                            
                                        }
                                    }
                                    
                                }
                            } //ZStack 사업자등록증이미지
                            .sheet(isPresented: $showingBusinessLicenseImagePickerFor_DetailEmployee, onDismiss: loadBusniessLicenseImageFor_DetailEmployee) {
                                ImagePicker(selectedImage: $selectedBusniessLicenseImageFor_DetailEmployee)
                            }
                            
                            
                            
                        }//VStack 사업자등록증 이미지
                        
                        
                        
                        Spacer()
                        
                        
                      
                    }//HStack
                    .padding(.leading)
                    
                    
                    
                } //VStack
                
                
            }//ScrollView
            .toast(isShowing: $showToast, text: Text(toastText))
            
            
            
            
            
        }//ZStack
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action: {
            
            self.presentationMode.wrappedValue.dismiss()
            
            
        }) {
            
            HStack(spacing: 0){
                
                Image(systemName: "chevron.backward")
                
            }
            
            
            
        })
        .navigationBarTitle("\(selectedDetailEmployeeName)", displayMode: .inline)
        
      
        
        
        
    }//body

    
  
    private func loadBusniessLicenseImageFor_DetailEmployee() {
        guard let selectedBusniessLicenseImageFor_DetailEmployee = selectedBusniessLicenseImageFor_DetailEmployee else { return }
        // You can do something with the selected brand image here
    }
    
}

#Preview {
    ContractDetailOfEmployeeView()
}
