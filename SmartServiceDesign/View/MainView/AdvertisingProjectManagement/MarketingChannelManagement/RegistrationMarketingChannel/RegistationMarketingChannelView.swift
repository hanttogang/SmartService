//
//  RegistrationMarketingChannelView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/26/23.
//

import SwiftUI


enum RegistrationMarketingChannelWorkArea: CaseIterable {
    case advertisingMarketing
    case mcn
    case anotherExample
}


struct RegistrationMarketingChannelView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
    @State private var showToast: Bool = false
    @State private var toastText: String = ""
    
    @State private var channelNameFor_MarketingChannelRegistration: String = ""
    @State private var channelOneLineExplanFor_MarketingChannelRegistration: String = ""
    
    @State private var nameOfManagerFor_MarketingChannelRegistration: String = ""
    @State private var phoneNumberOfManagerFor_MarketingChannelRegistration: String = ""
    @State private var emailOfManagerFor_MarketingChannelRegistration: String = ""
    
    @State private var selectedRegistrationMarketingChannelWorkArea: RegistrationMarketingChannelWorkArea = .advertisingMarketing
    
    @State private var showingImagePickerFor_MarketingChannelRegistration = false
    @State private var selectedContractImageFor_MarketingChannelRegistration: UIImage? = nil
    
    
    @State private var navigate: Bool = false
    
    var body: some View {
        
        
        ZStack{
            
            
            
            ScrollView{
                
                VStack(spacing: 0){
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("채널명")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $channelNameFor_MarketingChannelRegistration, prompt: Text("채널명을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 채널 명
                    .padding(.top, screenHeight / 25.46875) //32
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("한줄 설명")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $channelOneLineExplanFor_MarketingChannelRegistration, prompt: Text("한줄 설명을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 한줄 설명
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("담당자명")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $nameOfManagerFor_MarketingChannelRegistration, prompt: Text("담당자명을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.default)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 담당자 명
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("담당자 전화번호")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $phoneNumberOfManagerFor_MarketingChannelRegistration, prompt: Text("담당자 전화번호를 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.phonePad)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 담당자 전화번호
                    .padding(.top, screenHeight / 54.133) //15
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("담당자 이메일")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack{
                            TextField("", text: $emailOfManagerFor_MarketingChannelRegistration, prompt: Text("담당자 이메일을 입력해주세요.")
                                .foregroundColor(Color("hintTextColor")))
                            .keyboardType(.emailAddress)
                            .font(.system(size: 14))
                            .padding(.leading, 14)
                            .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                        }
                        .frame(width: screenWidth / 1.14, height: screenHeight / 18.45)
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("textFieldStrokeColor"), lineWidth: 1)
                        )
                        
                    }//VStack 담당자 이메일
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    VStack{
                        
                        HStack(spacing: 0){
                            Text("업무영역")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        HStack(spacing: 0) {
                            
                            Button(action: {
                                
                                selectedRegistrationMarketingChannelWorkArea = .advertisingMarketing
                                
                            }) {
                                Text("광고마케팅")
                                    .frame(width: screenWidth/3.26, height: screenHeight/23.97)
                                    .font(.system(size: 16))
                                
                                
                            }
                            .background(selectedRegistrationMarketingChannelWorkArea == .advertisingMarketing ? Color.blue : Color.clear)
                            .foregroundColor(selectedRegistrationMarketingChannelWorkArea == .advertisingMarketing ? .white : .blue)
                            .cornerRadius(4)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("mainColor"), lineWidth: 1)
                            )
                            
                            Button(action: {
                                
                                selectedRegistrationMarketingChannelWorkArea = .mcn
                                
                            }) {
                                Text("MCN")
                                    .frame(width: screenWidth/3.26, height: screenHeight/23.97)
                                    .font(.system(size: 16))
                                
                                
                            }
                            .background(selectedRegistrationMarketingChannelWorkArea == .mcn ? Color.blue : Color.clear)
                            .foregroundColor(selectedRegistrationMarketingChannelWorkArea == .mcn ? .white : .blue)
                            .cornerRadius(4)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("mainColor"), lineWidth: 1)
                            )
                            
                            Button(action: {
                                
                                selectedRegistrationMarketingChannelWorkArea = .anotherExample
                                
                            }) {
                                Text("기타")
                                    .frame(width: screenWidth/3.26, height: screenHeight/23.97)
                                    .font(.system(size: 16))
                                
                                
                            }
                            .background(selectedRegistrationMarketingChannelWorkArea == .anotherExample ? Color.blue : Color.clear)
                            .foregroundColor(selectedRegistrationMarketingChannelWorkArea == .anotherExample ? .white : .blue)
                            .cornerRadius(4)
                            .overlay(RoundedRectangle(cornerRadius: 4)
                                .stroke(Color("mainColor"), lineWidth: 1)
                            )
                            
                            
                        }//HStack
                        .overlay(RoundedRectangle(cornerRadius: 4)
                            .stroke(Color("mainColor"), lineWidth: 1)
                        )
                        
                        
                    } //VStack 업무 영역
                    .padding(.top, screenHeight / 54.133) //15
                    
                    
                    Spacer()
                    
                    
                    VStack(alignment: .leading){//계약서 등록
                        
                        HStack(spacing: 0){
                            Text("계약서 등록")
                                .font(.system(size: 14))
                            
                            Spacer()
                        }
                        .padding(.leading, screenWidth / 15.625)
                        
                        
                        
                        Button(action: {
                            showingImagePickerFor_MarketingChannelRegistration = true
                        }, label: {
                            
                            ZStack {
                                if let image = selectedContractImageFor_MarketingChannelRegistration {
                                    Image(uiImage: image)
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                } else {
                                    Rectangle()
                                        .frame(width: screenWidth/2.3006, height: screenHeight/5)
                                        .cornerRadius(12.0)
                                        .foregroundColor(.colorE5E5E5)
                                    
                                    Circle()
                                        .frame(width: screenWidth/10.416)
                                        .foregroundColor(.colorEFEFF4)
                                        .overlay{
                                            Image(systemName: "plus")
                                                .imageScale(.medium)
                                        }
                                }
                            }
                            
                        })
                        .padding(.leading, screenWidth / 18.75)
                        
                    }//VStack 계약서 등록 이미지
                    .padding(.top, screenHeight / 54.133) //20    54.133 = 15
                    .sheet(isPresented: $showingImagePickerFor_MarketingChannelRegistration, onDismiss: loadContractImageForMarketingChannelRegistation) {
                        ImagePicker(selectedImage: $selectedContractImageFor_MarketingChannelRegistration)
                    }
                    
                    HStack{
                        
                        Button(action: {
                            
                            if(channelNameFor_MarketingChannelRegistration == "" || channelOneLineExplanFor_MarketingChannelRegistration == "" || nameOfManagerFor_MarketingChannelRegistration == "" || phoneNumberOfManagerFor_MarketingChannelRegistration == "" || emailOfManagerFor_MarketingChannelRegistration == "" || selectedContractImageFor_MarketingChannelRegistration == nil){
                                
                                toastText = "칸을 비울 수 없습니다."
                                showToast = true
                                
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                    self.showToast = false
                                    toastText = ""
                                }
                                
                            } else {
                                self.presentationMode.wrappedValue.dismiss()
                            }
                            
                            
                            
                        }, label: {
                            
                                Text("마케팅 채널 등록")
                                    .foregroundColor(.white)
                                    .font(.system(size: 16))
                                    .bold()
                                    .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                            
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/67.9166) // 37
                        
                        
                        
                    }
                    
                    
                    
                    
                    Spacer()
                    
                    
                    
                    
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: Button(action: {
                    
                    self.presentationMode.wrappedValue.dismiss()
                    
                    
                }) {
                    
                    HStack(spacing: 0){
                        
                        Image(systemName: "chevron.backward")
                        
                    }
                    
                })
                .navigationBarTitle("마케팅 채널 등록", displayMode: .inline)
                
                
            }//ScrollView
            .onTapGesture { // 화면의 아무 곳이나 탭하면 키보드를 내립니다.
                UIApplication.shared.endEditing()
                
            }
            .toast(isShowing: $showToast, text: Text(toastText))
            
            
        }//ZStack
        
    }
    
    func loadContractImageForMarketingChannelRegistation() {
        guard let selectedContractImageFor_MarketingChannelRegistration = selectedContractImageFor_MarketingChannelRegistration else { return }
        // You can do something with the selected brand image here
    }
    
}

#Preview {
    RegistrationMarketingChannelView()
}
