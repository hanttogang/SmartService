//
//  DetailMarketingProjectListView.swift
//  SmartServiceDesign
//
//  Created by Teameverywhere on 12/18/23.
//

import SwiftUI

struct DetailMarketingProjectListView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    
//    init() {
//        let appearance = UINavigationBarAppearance()
//        //                appearance.configureWithOpaqueBackground()
//        //                appearance.backgroundColor = UIColor(.white) // 원하는 색상으로 변경하세요.
//        appearance.shadowColor = nil // 이 부분을 추가하면 네비게이션 바 하단 선이 사라집니다.
//        UINavigationBar.appearance().standardAppearance = appearance
//        UINavigationBar.appearance().compactAppearance = appearance
//        UINavigationBar.appearance().scrollEdgeAppearance = appearance
//        
//    }
//    
    
    @State private var navigate: Bool = false
    
    @State var isShowingMenu: Bool = false
    
    @State var searchText: String = ""
    @State var editText: Bool = false
    
    @State var initCustomerBrand: String = ""
    @State private var initIsSearchingCustomerBrand: Bool = false
    
    var titleProjectName: String
    
    var body: some View {
        
        ZStack{
                
                
                VStack{
                    
                    
                    HStack{
                        
                        HStack{
                            
                        }
                        .frame(width: screenWidth, height: screenHeight/14.5)
                        .overlay{
                            //검색창을 받을수있는 택스트필드
                            TextField("Search" , text : self.$searchText)
                                .autocapitalization(.none) // 첫 번째 글자 대문자 자동 변환 비활성화
                                .foregroundColor(Color("hintTextColor"))
                            //hint와 태두리에 간격을 띄우기위해 15정도의 간격을주고
                                .padding(10)
                            //양옆은 추가로 15를 더줌
                                .padding(.leading, screenWidth/12.5) //30
                            //배경색상은 자유롭게선택
                                .background(Color(.white))
                            //검색창이 너무각지면 딱딱해보이기때문에 모서리를 둥글게
                            //숫자는 취향것
                                .cornerRadius(15)
                            //내가만든 검색창 상단에
                            //돋보기를 넣어주기위해
                            //오버레이를 선언
                                .overlay(
                                    //HStack을 선언하여
                                    //가로로 view를 쌓을수있도록 하나 만들고
                                    HStack{
                                        //맨오른쪽으로 밀기위해 Spacer()로 밀어준다.
                                        
                                        //xcode에서 지원해주는 이미지
                                        //                                            if !self.editText {
                                        //magnifyingglass 를 사용
                                        //색상은 자유롭게 변경가능
                                        Image(systemName: "magnifyingglass")
                                            .foregroundColor(Color("hintTextColor"))
                                            .padding()
                                        //                                            }
                                        
                                        
                                        
                                        Spacer()
                                        
                                        if self.editText{
                                            //x버튼이미지를 클릭하게되면 입력되어있던값들을 취소하고
                                            //키입력 이벤트를 종료해야한다.
                                            Button(action : {
                                                self.editText = false
                                                self.searchText = ""
                                                //키보드에서 입력을 끝내게하는 코드
                                                UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
                                            }){
                                                Image(systemName: "multiply.circle.fill")
                                                    .foregroundColor(Color("hintTextColor"))
                                                    .padding()
                                            }
                                        }
                                        
                                    }
                                ).onTapGesture {
                                    self.editText = true
                                }
                                .padding(.horizontal, 21.945) //37
                            
                            
                            
                        }//overlay 
                        
                        
                        
                        
                        
                    }//HStack
                    .background(Color("colorF8F8F892"))
                    
                    
                    HStack{
                        
                        Button(action: {
                            
                            self.navigate = true
                            
                        }, label: {
                            
                            NavigationLink(destination: AddDetailScheduleView(titleProjectName: titleProjectName), isActive: $navigate) {
                                EmptyView()
                            }
                            
                            
                            
                                
                                //                                    AddNewProject(isSelectedCustomerBrand: "", isSearchingCustomerBrand: false)
                            
                                Text("+  세부 일정 추가")
                                    .foregroundColor(.white)
                                    .bold()
                                    .font(.system(size: 16))
                                    .frame(width: screenWidth/1.14, height: screenHeight/18.45)
                            
                        })
                        .background(Color("mainColor"))
                        .cornerRadius(4)
                        .padding(.top, screenHeight/50.75)
                        
                        
                        
                    }
                    
                    HStack{
                        Spacer()
                        
                        Text("캘린더 전환")
                        
                    }
                    .padding()
                    
                    
                    
                    ScrollView {
                        VStack(spacing: 0) {
                            ForEach(projectDummyData.filter { // filter 메서드를 사용하여 결과를 필터링합니다.
                                searchText.isEmpty || $0.companyName.contains(searchText) || $0.projectName.contains(searchText)
                                // 회사 이름 또는 프로젝트 이름을 기준으로 검색
                            })  { project in
                                
                                NavigationLink(destination: DetailSchedulDetailView(getSchedulNameFor_DetailSchedulDetail: project.companyName)){
                                    
                                    
                                    VStack {
                                        HStack {
                                            Image(systemName: project.icon)
                                                .resizable()
                                                .aspectRatio(contentMode: .fit)
                                                .frame(width: screenWidth/6.25)
                                            
                                            VStack(alignment: .leading){
                                                Text(project.companyName)
                                                    .font(.title2)
                                                    .bold()
                                                    .foregroundColor(.black)
                                                
                                                
                                                
                                                Text(project.projectName)
                                                    .foregroundColor(.black)
                                                
                                                Text("참여인원: \(project.participantCount)")
                                                    .foregroundColor(.gray)
                                            }
                                            
                                            Spacer()
                                            
                                            Image(systemName: "chevron.right")
                                                .padding()
                                                .foregroundColor(.gray)
                                        }
                                        
                                        .padding(.top)
                                        
                                        Divider()
                                    }//VStack
                                    .background(Color.white)
                                    .padding(.leading)
                                }//NavigationLink
                                
                            }
                        }
                    }//ScrollView


                    
                    
                    
                    
                    
                    
                    
                    
                    
                    Spacer()
                    
                    
                    
                    
                }//VStack
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: Button(action: {
                    
                    self.presentationMode.wrappedValue.dismiss()
                    
                    
                }) {
                    
                    HStack(spacing: 0){
                        
                        Image(systemName: "chevron.backward")
                        
                    }
                    
                    
                    
                })
                .navigationBarTitle("\(titleProjectName)", displayMode: .inline)
            //    .toast(isShowing: $showToast, text: Text(toastText))
//            .navigationBarHidden(true)
            
           
            
        }//ZStack
        
        
        
    }
    
}

#Preview {

    DetailMarketingProjectListView(titleProjectName: "")
}
