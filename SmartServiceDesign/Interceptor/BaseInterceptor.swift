//
//  BaseInterceptor.swift
//  ORCA_ERP
//
//  Created by Teameverywhere on 12/1/23.
//

import Foundation
import Alamofire

class BaseInterceptor: RequestInterceptor {
    
    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        
        var request = urlRequest
        
        //헤더부분 넣어주기
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        completion(.success(request))
        
        
    }
}

